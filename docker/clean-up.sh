#!/usr/bin/env bash

echo "Removing vs stack"
docker stack rm vs

echo "Waiting for stack to be removed"
sleep 20

echo "Removing vs volumes"
docker volume rm vs_redis vs_mysql
