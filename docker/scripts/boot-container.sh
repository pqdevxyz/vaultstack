#!/usr/bin/env bash

mkdir /run/php
/var/www/html/docker/scripts/setup-container.sh
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
