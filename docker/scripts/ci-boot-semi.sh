#!/usr/bin/env bash

#Setup PHP dir
mkdir /run/php

#Goto docker dir
cd docker

#Copy important files into required places in container
cp ./supervisor/pipeline.supervisord.conf /etc/supervisor/conf.d/supervisord.conf
cp ./php/php.ini /etc/php/8.1/cli/conf.d/99-overrides.ini
cp ./nginx/main.conf /etc/nginx/nginx.conf
mkdir /etc/nginx/sites-enabled
cp ./nginx/pipelines.conf /etc/nginx/sites-enabled/

cd ..

#Install & setup any app data
echo "127.0.0.1 vaultstack.test" >> /etc/hosts

#Install composer packages
composer install -n

#Install yarn packages
yarn install --non-interactive

#Setup logs
mkdir -p ./storage/logs
touch ./storage/logs/laravel.log

#Run the laravel commands
php artisan migrate
php artisan config:clear
php artisan cache:clear
php artisan view:clear

#Set file permissions
chmod -R 775 .
chmod -R 777 ./storage/
chmod -R 777 ./bootstrap/
chmod -R 777 ./docker/nginx/*.crt
chmod -R 777 ./docker/nginx/*.key

#Run supervisor to start php & nginx
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf&
sleep 10
