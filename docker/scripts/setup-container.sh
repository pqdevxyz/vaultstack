#!/usr/bin/env bash

cd /var/www/html

git config --global --add safe.directory /var/www/html

echo "127.0.0.1 vaultstack.test" >> /etc/hosts

#Install composer packages
composer install -n

#Install yarn packages
yarn install --non-interactive

#Setup logs
mkdir -p ./storage/logs
touch ./storage/logs/laravel.log

#Run the laravel commands
php artisan migrate
php artisan config:clear
php artisan cache:clear
php artisan view:clear

#Set file permissions
chmod -R 775 .
chmod -R 777 ./storage/
chmod -R 777 ./bootstrap/
chmod 664 ./bootstrap/app.php
chmod 664 ./public/index.php

chmod -R 777 ./docker/nginx/*.crt
chmod -R 777 ./docker/nginx/*.key

php artisan dusk:chrome-driver --detect
yarn cypress install
