#!/usr/bin/env bash

aws ecr get-login-password --region eu-west-2 | docker login --username AWS --password-stdin 339898008043.dkr.ecr.eu-west-2.amazonaws.com
cd ..
docker build -t vs-local:latest ./
cd docker
docker service update --image vs-local:latest --force vs_app
docker stack deploy -c docker-compose.yml vs --with-registry-auth --resolve-image always --prune
