<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alerts\AlertError;
use Tests\Browser\Pages\Auth\AuthLogin;
use Tests\DuskTestCase;
use App\Enum\NamedRoutesEnum as NR;

class AuthPageSet1Test extends DuskTestCase
{
    /**
     * Check to see you can login when providing valid admin details.
     *
     * @test checkLoginWorksWithCorrectDetails
     * @group auth.login
     * @return void
     * @throws \Throwable
     */
    public function checkLoginWorksWithCorrectDetails()
    {
        $this->browse(
            function (Browser $browser) {
                $page = new AuthLogin();
                $browser->visit($page);
                $page->assertVisuals($browser);
                $page->assertForms($browser);
                $browser->type('@email', $this->adminUser->email);
                $browser->type('@password', 'vaultstack');
                $browser->click('@login-button');
                $browser->screenshot('AuthPageSet1Test.checkLoginWorksWithCorrectDetails');
                $browser->assertRouteIs(NR::VAULT_OVERVIEW);
                $browser->assertAuthenticatedAs($this->adminUser);
            }
        );
    }

    /**
     * Check to see that an error is display when entering incorrect details on the login page.
     *
     * @test checkLoginErrorsWithIncorrectDetails
     * @group auth.login
     * @return void
     * @throws \Throwable
     */
    public function checkLoginErrorsWithIncorrectDetails()
    {
        $this->browse(
            function (Browser $browser) {
                $page = new AuthLogin();
                $browser->visit($page);
                $page->assertVisuals($browser);
                $page->assertForms($browser);
                $browser->type('@email', $this->adminUser->email);
                $browser->type('@password', 'password');
                $browser->click('@login-button');
                $browser->screenshot('AuthPageSet1Test.checkLoginErrorsWithIncorrectDetails');
                $browser->assertRouteIs(NR::AUTH_LOGIN);
                $browser->within(new AlertError(), function ($browser) {
                    $browser->assertSeeIn('@title', 'An Error Occurred');
                    $browser->assertSeeIn('@list', 'Sorry, your email address or password is incorrect');
                });
                $browser->assertGuest();
            }
        );
    }

    /**
     * Check to see that after clicking on the forgot password link on the login page you are redirect to the forgot password page.
     *
     * @test checkForgotPasswordLinkRedirectsToForgotPasswordPage
     * @group auth.login
     * @return void
     * @throws \Throwable
     */
    public function checkForgotPasswordLinkRedirectsToForgotPasswordPage()
    {
        $this->browse(
            function (Browser $browser) {
                $page = new AuthLogin();
                $browser->visit($page);
                $page->assertVisuals($browser);
                $page->assertForms($browser);
                $browser->click('@forgot-password-link');
                $browser->screenshot('AuthPageSet1Test.checkForgotPasswordLinkRedirectsToForgotPasswordPage');
                $browser->assertRouteIs(NR::AUTH_FORGOT_PASSWORD);
            }
        );
    }
}
