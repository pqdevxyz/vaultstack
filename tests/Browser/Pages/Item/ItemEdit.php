<?php

namespace Tests\Browser\Pages\Item;

use App\Enum\NamedRoutesEnum as NR;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Page;

class ItemEdit extends Page
{
    public int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return route(NR::ITEM_EDIT, ['id' => $this->id], false);
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
        $browser->assertRouteIs(NR::ITEM_EDIT, ['id' => $this->id]);
        $browser->assertTitle('Edit Item | Vault Stack');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@back-button' => '#back-button',
        ];
    }
}
