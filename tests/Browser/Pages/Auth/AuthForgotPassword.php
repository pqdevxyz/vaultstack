<?php

namespace Tests\Browser\Pages\Auth;

use App\Enum\NamedRoutesEnum as NR;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;
use Tests\HttpErrors;

class AuthForgotPassword extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return route(NR::AUTH_FORGOT_PASSWORD, [], false);
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertRouteIs(NR::AUTH_FORGOT_PASSWORD);
    }

    public function assertVisuals(Browser $browser)
    {
        $browser->assertSee('Forgot Password');
        $browser->assertTitle('Forgot Password | Vault Stack');
    }

    public function assertForms(Browser $browser)
    {
        $browser->assertVisible('@email');
        $browser->assertVisible('@send-email-button');
    }

    public function assertErrorPage(Browser $browser, $httpError)
    {
        $browser->assertSee($httpError['code']);
        $browser->assertSee($httpError['text']);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@email' => 'input[name=email]',
            '@send-email-button' => 'button[type=submit]',
        ];
    }
}
