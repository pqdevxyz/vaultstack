<?php

namespace Tests\Browser\Pages\Auth;

use App\Enum\NamedRoutesEnum as NR;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;
use Tests\HttpErrors;

class AuthLogin extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return route(NR::AUTH_LOGIN, [], false);
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertRouteIs(NR::AUTH_LOGIN);
    }

    public function assertVisuals(Browser $browser)
    {
        $browser->assertSee('Login');
        $browser->assertTitle('Login | Vault Stack');
    }

    public function assertForms(Browser $browser)
    {
        $browser->assertVisible('@email');
        $browser->assertVisible('@password');
        $browser->assertVisible('@forgot-password-link');
        $browser->assertVisible('@login-button');
    }

    public function assertErrorPage(Browser $browser, $httpError)
    {
        $browser->assertSee($httpError['code']);
        $browser->assertSee($httpError['text']);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@email' => 'input[name=email]',
            '@password' => 'input[name=password]',
            '@forgot-password-link' => '#forgot-password-link',
            '@login-button' => 'button[type=submit]',
        ];
    }
}
