<?php

namespace Tests\Browser\Pages\Auth;

use App\Enum\NamedRoutesEnum as NR;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;
use Tests\HttpErrors;

class AuthSetPassword extends Page
{
    protected ?string $token;

    public function __construct(string $token = null)
    {
        $this->token = $token;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        if (! is_null($this->token)) {
            return route(NR::AUTH_SET_PASSWORD, ['token' => $this->token], false);
        } else {
            return route(NR::AUTH_SET_PASSWORD, [], false);
        }
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertRouteIs(NR::AUTH_SET_PASSWORD);
    }

    public function assertVisuals(Browser $browser)
    {
        $browser->assertSee('Set Password');
        $browser->assertTitle('Set Password | Vault Stack');
    }

    public function assertForms(Browser $browser)
    {
        $browser->assertVisible('@password');
        $browser->assertVisible('@confirm-password');
        $browser->assertVisible('@save-password-button');
    }

    public function assertErrorPage(Browser $browser, $httpError)
    {
        $browser->assertSee($httpError['code']);
        $browser->assertSee($httpError['text']);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@password' => 'input[name=password]',
            '@confirm-password' => 'input[name=confirm-password]',
            '@save-password-button' => 'button[type=submit]',
        ];
    }
}
