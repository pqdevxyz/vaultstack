<?php

namespace Tests\Browser\Pages\Vault;

use App\Enum\NamedRoutesEnum as NR;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class VaultOverview extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return route(NR::VAULT_OVERVIEW, [], false);
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
        $browser->assertRouteIs(NR::VAULT_OVERVIEW);
        $browser->assertTitle('Vault | Vault Stack');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [];
    }
}
