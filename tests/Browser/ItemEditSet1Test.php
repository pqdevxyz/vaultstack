<?php

namespace Tests\Browser;

use App\Enum\NamedRoutesEnum as NR;
use App\Models\VaultGroup;
use App\Models\VaultItem;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alerts\AlertError;
use Tests\Browser\Components\Alerts\AlertSuccess;
use Tests\Browser\Components\Item\ItemDelete;
use Tests\Browser\Components\Item\ItemDetails;
use Tests\Browser\Pages\Item\ItemEdit;
use Tests\DuskTestCase;

class ItemEditSet1Test extends DuskTestCase
{
    /**
     * Check item edit page has all the correct elements.
     * @test checkItemEditPageHasAllTheCorrectElements
     * @group item.edit
     */
    public function checkItemCreatePageHasAllTheCorrectElements()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::where('user_id', $this->adminUser->id)->first();
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => $this->adminUser->id,
            'vault_groups_id' => $group->id,
        ]);

        $this->browse(function (Browser $browser) use ($item) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new ItemEdit($item->id));

            $browser->within(new ItemDetails(), function ($browser) use ($item) {
                $browser->assertValue('@name', $item->name);
                $browser->assertValue('@url', $item->url);
                $browser->assertValue('@description', $item->description);
            });

            $browser->screenshot('ItemEditSet1Test.checkItemCreatePageHasAllTheCorrectElements');
        });
    }

    /**
     * Check item edit page works correctly.
     * @test checkItemEditPageWorksCorrectly
     * @group item.edit
     */
    public function checkItemEditPageWorksCorrectly()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::where('user_id', $this->adminUser->id)->first();
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => $this->adminUser->id,
            'vault_groups_id' => $group->id,
        ]);
        /** @var VaultItem $updatedItem */
        $updatedItem = VaultItem::factory()->make();

        $this->browse(function (Browser $browser) use ($item, $updatedItem) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new ItemEdit($item->id));

            $browser->within(new ItemDetails(), function ($browser) use ($updatedItem) {
                $browser->type('@name', $updatedItem->name);
                $browser->type('@url', $updatedItem->url);
                $browser->type('@description', $updatedItem->description);
                $browser->click('@save-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Item updated');
            });

            $browser->screenshot('ItemEditSet1Test.checkItemEditPageWorksCorrectly');
        });
    }

    /**
     * Check item edit page returns error if missing data.
     * @test checkItemEditPageReturnsErrorIfMissingData
     * @group item.edit
     */
    public function checkItemEditPageReturnsErrorIfMissingData()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::where('user_id', $this->adminUser->id)->first();
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => $this->adminUser->id,
            'vault_groups_id' => $group->id,
        ]);

        $this->browse(function (Browser $browser) use ($item) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new ItemEdit($item->id));

            $browser->within(new ItemDetails(), function ($browser) {
                $browser->type('@name', '');
                $browser->type('@url', '');
                $browser->type('@description', '');
                $browser->click('@save-button');
            });

            $browser->within(new AlertError(), function ($browser) {
                $browser->assertSeeIn('@title', 'An Error Occurred');
                $browser->assertSeeIn('@list', 'The name field is required');
                $browser->assertSeeIn('@list', 'The url field is required');
                $browser->assertSeeIn('@list', 'The description field is required');
            });

            $browser->screenshot('ItemEditSet1Test.checkItemEditPageReturnsErrorIfMissingData');
        });
    }

    /**
     * Check item edit page back link works.
     * @test checkItemEditPageBackLinkWorks
     * @group item.edit
     */
    public function checkItemEditPageBackLinkWorks()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::where('user_id', $this->adminUser->id)->first();
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => $this->adminUser->id,
            'vault_groups_id' => $group->id,
        ]);

        $this->browse(function (Browser $browser) use ($item) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new ItemEdit($item->id));

            $browser->assertSeeIn('@back-button', 'Back');
            $browser->click('@back-button');

            $browser->assertRouteIs(NR::VAULT_OVERVIEW);
            $browser->screenshot('ItemEditSet1Test.checkItemEditPageBackLinkWorks');
        });
    }

    /**
     * Check item edit page delete item link works.
     * @test checkItemEditPageDeleteItemLinkWorks
     * @group item.edit
     */
    public function checkItemEditPageDeleteItemLinkWorks()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::where('user_id', $this->adminUser->id)->first();
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => $this->adminUser->id,
            'vault_groups_id' => $group->id,
        ]);

        $this->browse(function (Browser $browser) use ($item) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new ItemEdit($item->id));

            $browser->within(new ItemDelete(), function ($browser) {
                $browser->assertSeeIn('@delete-button', 'Delete Item');
                $browser->click('@delete-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Item deleted');
            });

            $browser->screenshot('ItemEditSet1Test.checkItemEditPageDeleteItemLinkWorks');
        });

        $this->assertSoftDeleted('vault_items', [
            'id' => $item->id,
        ]);
    }
}
