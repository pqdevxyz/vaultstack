<?php

namespace Tests\Browser;

use App\Enum\NamedRoutesEnum as NR;
use App\Models\VaultGroup;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alerts\AlertError;
use Tests\Browser\Components\Alerts\AlertSuccess;
use Tests\Browser\Components\Group\GroupDelete;
use Tests\Browser\Components\Group\GroupDetails;
use Tests\Browser\Pages\Group\GroupEdit;
use Tests\DuskTestCase;

class GroupEditSet1Test extends DuskTestCase
{
    /**
     * Check group edit page has all the correct elements.
     * @test checkGroupEditPageHasAllTheCorrectElements
     * @group group.edit
     */
    public function checkGroupEditPageHasAllTheCorrectElements()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);

        $this->browse(function (Browser $browser) use ($group) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new GroupEdit($group->id));

            $browser->within(new GroupDetails(), function ($browser) use ($group) {
                $browser->assertValue('@name', $group->name);
                $browser->assertValue('@color', $group->color);
            });

            $browser->screenshot('GroupEditSet1Test.checkGroupEditPageHasAllTheCorrectElements');
        });
    }

    /**
     * Check group edit page works correctly.
     * @test checkGroupEditPageWorksCorrectly
     * @group group.edit
     */
    public function checkGroupEditPageWorksCorrectly()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var VaultGroup $updatedGroup */
        $updatedGroup = VaultGroup::factory()->make();

        $this->browse(function (Browser $browser) use ($group, $updatedGroup) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new GroupEdit($group->id));

            $browser->within(new GroupDetails(), function ($browser) use ($updatedGroup) {
                $browser->type('@name', $updatedGroup->name);
                $browser->select('@color', $updatedGroup->color);
                $browser->click('@save-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Group updated');
            });

            $browser->screenshot('GroupEditSet1Test.checkGroupEditPageWorksCorrectly');
        });
    }

    /**
     * Check group edit page returns error if missing data.
     * @test checkGroupEditPageReturnsErrorIfMissingData
     * @group group.edit
     */
    public function checkGroupEditPageReturnsErrorIfMissingData()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);

        $this->browse(function (Browser $browser) use ($group) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new GroupEdit($group->id));

            $browser->within(new GroupDetails(), function ($browser) {
                $browser->type('@name', '');
                $browser->click('@save-button');
            });

            $browser->within(new AlertError(), function ($browser) {
                $browser->assertSeeIn('@title', 'An Error Occurred');
                $browser->assertSeeIn('@list', 'The name field is required');
            });

            $browser->screenshot('GroupEditSet1Test.checkGroupEditPageReturnsErrorIfMissingData');
        });
    }

    /**
     * Check group edit page back link works.
     * @test checkGroupEditPageBackLinkWorks
     * @group group.edit
     */
    public function checkGroupEditPageBackLinkWorks()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);

        $this->browse(function (Browser $browser) use ($group) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new GroupEdit($group->id));

            $browser->assertSeeIn('@back-button', 'Back');
            $browser->click('@back-button');

            $browser->assertRouteIs(NR::VAULT_OVERVIEW);
            $browser->screenshot('GroupEditSet1Test.checkGroupEditPageBackLinkWorks');
        });
    }

    /**
     * Check group edit page delete group link works.
     * @test checkGroupEditPageDeleteGroupLinkWorks
     * @group group.edit
     */
    public function checkGroupEditPageDeleteGroupLinkWorks()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);

        $this->browse(function (Browser $browser) use ($group) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new GroupEdit($group->id));

            $browser->within(new GroupDelete(), function ($browser) {
                $browser->assertSeeIn('@delete-button', 'Delete Group');
                $browser->click('@delete-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Group deleted');
            });

            $browser->screenshot('GroupEditSet1Test.checkGroupEditPageDeleteGroupLinkWorks');
        });

        $this->assertDatabaseMissing('vault_groups', [
            'id' => $group->id,
        ]);
    }
}
