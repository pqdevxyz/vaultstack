<?php

namespace Tests\Browser\Components\Item;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class ItemDetails extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#item-details';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
        $browser->assertVisible('@name');
        $browser->assertVisible('@url');
        $browser->assertVisible('@description');
        $browser->assertVisible('@group');
        $browser->assertVisible('@save-button');
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name' => 'input[name=name]',
            '@url' => 'input[name=url]',
            '@description' => 'textarea[name=description]',
            '@group' => 'select[name=group]',
            '@save-button' => 'button[type=submit]',
        ];
    }
}
