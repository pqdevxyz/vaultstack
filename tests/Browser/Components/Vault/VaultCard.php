<?php

namespace Tests\Browser\Components\Vault;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class VaultCard extends BaseComponent
{
    public int $number;

    public function __construct(int $number)
    {
        $this->number = $number;
    }

    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#vault-cards .vault-card:nth-of-type(' . $this->number . ')';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@title' => '.vault-card-title',
            '@url' => '.vault-card-url',
            '@description' => '.vault-card-description',
        ];
    }
}
