<?php

namespace Tests\Browser\Components\Alerts;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class AlertWarning extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#alert-warning';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@title' => 'div > h4',
            '@list' => '#alert-warning-message',
        ];
    }
}
