<?php

namespace Tests\Browser\Components\Group;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class GroupDetails extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#group-details';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
        $browser->assertVisible('@name');
        $browser->assertVisible('@color');
        $browser->assertVisible('@save-button');
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name' => 'input[name=name]',
            '@color' => 'select[name=color]',
            '@save-button' => 'button[type=submit]',
        ];
    }
}
