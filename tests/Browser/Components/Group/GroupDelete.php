<?php

namespace Tests\Browser\Components\Group;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class GroupDelete extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#group-delete';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
        $browser->assertVisible('@delete-button');
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@delete-button' => '#group-delete-button',
        ];
    }
}
