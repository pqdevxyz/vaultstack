<?php

namespace Tests\Browser;

use App\Models\VaultGroup;
use App\Models\VaultItem;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Item\ItemBanner;
use Tests\Browser\Pages\Item\ItemView;
use Tests\DuskTestCase;

class ItemViewSet1Test extends DuskTestCase
{
    /**
     * Check view item page has all the correct elements.
     * @test checkViewItemPageHasAllTheCorrectElements
     * @group item.view
     */
    public function checkViewItemPageHasAllTheCorrectElements()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::where('user_id', $this->adminUser->id)->first();
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => $this->adminUser->id,
            'vault_groups_id' => $group->id,
        ]);

        $this->browse(function (Browser $browser) use ($item) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new ItemView($item->id));

            $browser->within(new ItemBanner(), function ($browser) use ($item) {
                $browser->assertSeeIn('@item', $item->name);
                $browser->assertSeeIn('@item', $item->url);
                $browser->assertSeeIn('@group', $item->group->name);
            });

            $browser->assertSeeIn('@description', $item->description);

            $browser->screenshot('ItemViewSet1Test.checkViewItemPageHasAllTheCorrectElements');
        });
    }
}
