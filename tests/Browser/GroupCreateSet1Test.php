<?php

namespace Tests\Browser;

use App\Models\VaultGroup;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alerts\AlertError;
use Tests\Browser\Components\Alerts\AlertSuccess;
use Tests\Browser\Components\Group\GroupDetails;
use Tests\Browser\Pages\Group\GroupCreate;
use Tests\DuskTestCase;
use App\Enum\NamedRoutesEnum as NR;

class GroupCreateSet1Test extends DuskTestCase
{
    /**
     * Check group create page has all the correct elements.
     * @test checkGroupCreatePageHasAllTheCorrectElements
     * @group group.create
     */
    public function checkGroupCreatePageHasAllTheCorrectElements()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new GroupCreate());

            $browser->within(new GroupDetails(), function ($browser) {
                $browser->assertValue('@name', '');
                $browser->assertValue('@color', 'slate');
            });

            $browser->screenshot('GroupCreateSet1Test.checkGroupCreatePageHasAllTheCorrectElements');
        });
    }

    /**
     * Check group create page works correctly.
     * @test checkGroupCreatePageWorksCorrectly
     * @group group.create
     */
    public function checkGroupCreatePageWorksCorrectly()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->make();

        $this->browse(function (Browser $browser) use ($group) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new GroupCreate());

            $browser->within(new GroupDetails(), function ($browser) use ($group) {
                $browser->type('@name', $group->name);
                $browser->select('@color', $group->color);
                $browser->click('@save-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Group created');
            });

            $browser->screenshot('GroupCreateSet1Test.checkGroupCreatePageWorksCorrectly');
        });
    }

    /**
     * Check group create page returns error if missing data.
     * @test checkGroupCreatePageReturnsErrorIfMissingData
     * @group group.create
     */
    public function checkGroupCreatePageReturnsErrorIfMissingData()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new GroupCreate());

            $browser->within(new GroupDetails(), function ($browser) {
                $browser->type('@name', '');
                $browser->click('@save-button');
            });

            $browser->within(new AlertError(), function ($browser) {
                $browser->assertSeeIn('@title', 'An Error Occurred');
                $browser->assertSeeIn('@list', 'The name field is required');
            });

            $browser->screenshot('GroupCreateSet1Test.checkGroupCreatePageReturnsErrorIfMissingData');
        });
    }

    /**
     * Check group create page back link works.
     * @test checkGroupCreatePageBackLinkWorks
     * @group group.create
     */
    public function checkGroupCreatePageBackLinkWorks()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new GroupCreate());

            $browser->assertSeeIn('@back-button', 'Back');
            $browser->click('@back-button');

            $browser->assertRouteIs(NR::VAULT_OVERVIEW);
            $browser->screenshot('GroupCreateSet1Test.checkGroupCreatePageBackLinkWorks');
        });
    }
}
