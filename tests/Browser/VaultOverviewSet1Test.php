<?php

namespace Tests\Browser;

use App\Models\VaultItem;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Collection;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Navbar;
use Tests\Browser\Components\Vault\VaultCard;
use Tests\Browser\Pages\Vault\VaultOverview;
use Tests\DuskTestCase;

class VaultOverviewSet1Test extends DuskTestCase
{
    /**
     * Check vault overview page has all the correct elements.
     * @test checkVaultOverviewPageHasAllTheCorrectElements
     * @group vault.overview
     */
    public function checkVaultOverviewPageHasAllTheCorrectElements()
    {
        /** @var Collection $items */
        $items = VaultItem::where('user_id', $this->adminUser->id)->orderBy('created_at', 'desc')->get();

        $this->browse(function (Browser $browser) use ($items) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new VaultOverview());
            $browser->screenshot('VaultOverviewSet1Test.checkVaultOverviewPageHasAllTheCorrectElements');
            $browser->within(new Navbar(), function ($browser) {
                $browser->assertSeeIn('@other-links', 'Add Group');
                $browser->assertSeeIn('@other-links', 'Add Item');
            });

            foreach ($items as $itemKey => $item) {
                $browser->within(new VaultCard($itemKey + 1), function ($browser) use ($item) {
                    $browser->assertSeeIn('@title', $item->name);
                    $browser->assertSeeIn('@url', $item->url);
                });
            }
        });
    }
}
