<?php

namespace Tests\Browser;

use App\Models\SecureToken;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alerts\AlertError;
use Tests\Browser\Components\Alerts\AlertInfo;
use Tests\Browser\Components\Alerts\AlertSuccess;
use Tests\Browser\Pages\Auth\AuthForgotPassword;
use Tests\Browser\Pages\Auth\AuthSetPassword;
use Tests\DuskTestCase;
use Tests\HttpErrors;
use App\Enum\NamedRoutesEnum as NR;

class AuthPageSet2Test extends DuskTestCase
{
    /**
     * Check to see that message is displayed correct after entering email address.
     *
     * @test checkForgotPasswordDisplayInfoMessageAfterEmailIsEntered
     * @group auth.forgot-password
     * @return void
     * @throws \Throwable
     */
    public function checkForgotPasswordDisplayInfoMessageAfterEmailIsEntered()
    {
        $this->browse(
            function (Browser $browser) {
                $page = new AuthForgotPassword();
                $browser->visit($page);
                $page->assertVisuals($browser);
                $page->assertForms($browser);
                $browser->type('@email', $this->adminUser->email);
                $browser->click('@send-email-button');
                $browser->screenshot('AuthPageSet2Test.checkForgotPasswordDisplayInfoMessageAfterEmailIsEntered');
                $browser->assertRouteIs(NR::AUTH_FORGOT_PASSWORD);
                $browser->within(new AlertInfo(), function ($browser) {
                    $browser->assertSeeIn('@title', 'Info');
                    $browser->assertSeeIn('@list', 'We will send you an email with a link to reset your password if your account exists in our system');
                });
            }
        );
    }

    /**
     * Check to see that the success message is displayed correct after entering email address.
     *
     * @test checkSetPasswordDisplaySuccessMessageAfterPasswordsAreEntered
     * @group auth.set-password
     * @return void
     * @throws \Throwable
     */
    public function checkSetPasswordDisplaySuccessMessageAfterPasswordsAreEntered()
    {
        //Setup Data
        $passwordReset = (new SecureToken())->createToken($this->adminUser, 'password_reset');

        $this->browse(
            function (Browser $browser) use ($passwordReset) {
                $page = new AuthSetPassword($passwordReset->token);
                $browser->visit($page);
                $page->assertVisuals($browser);
                $page->assertForms($browser);
                $browser->type('@password', 'vaultstack');
                $browser->type('@confirm-password', 'vaultstack');
                $browser->click('@save-password-button');
                $browser->screenshot('AuthPageSet2Test.checkSetPasswordDisplaySuccessMessageAfterPasswordsAreEntered');
                $browser->assertRouteIs(NR::AUTH_LOGIN);
                $browser->within(new AlertSuccess(), function ($browser) {
                    $browser->assertSeeIn('@title', 'Success');
                    $browser->assertSeeIn('@list', 'Your password has been changed.');
                });
            }
        );
    }

    /**
     * Check to see that the error message is displayed after no token is provided.
     *
     * @test checkSetPasswordErrorsWhenNoTokenIsProvided
     * @group auth.set-password
     * @return void
     * @throws \Throwable
     */
    public function checkSetPasswordErrorsWhenNoTokenIsProvided()
    {
        $this->browse(
            function (Browser $browser) {
                $page = new AuthSetPassword();
                $browser->visit($page);
                $page->assertErrorPage($browser, HttpErrors::BAD_REQUEST);
                $browser->screenshot('AuthPageSet2Test.checkSetPasswordErrorsWhenNoTokenIsProvided');
                $browser->assertSee('The link you have clicked is not valid please check the link and try again');
            }
        );
    }

    /**
     * Check to see that the error message is displayed when the token is expired.
     *
     * @test checkSetPasswordErrorsWhenTokenIsExpired
     * @group auth.set-password
     * @return void
     * @throws \Throwable
     */
    public function checkSetPasswordErrorsWhenTokenIsExpired()
    {
        //Setup Data
        $passwordReset = (new SecureToken())->createToken($this->adminUser, 'password_reset');
        $passwordReset->created_at = Carbon::now()->subHours(2);
        $passwordReset->save();

        $this->browse(
            function (Browser $browser) use ($passwordReset) {
                $page = new AuthSetPassword($passwordReset->token);
                $browser->visit($page);
                $page->assertErrorPage($browser, HttpErrors::BAD_REQUEST);
                $browser->screenshot('AuthPageSet2Test.checkSetPasswordErrorsWhenTokenIsExpired');
                $browser->assertSee('The link has expired, please go back to the forgot password page and request a new email');
            }
        );
    }

    /**
     * Check to see that the error message is displayed when the token is expired after trying to save new password.
     *
     * @test checkSetPasswordErrorsWhenTokenIsExpiredWhenTryingToSaveNewPassword
     * @group auth.set-password
     * @return void
     * @throws \Throwable
     */
    public function checkSetPasswordErrorsWhenTokenIsExpiredWhenTryingToSaveNewPassword()
    {
        //Setup Data
        $passwordReset = (new SecureToken())->createToken($this->adminUser, 'password_reset');
        $page = new AuthSetPassword($passwordReset->token);
        $this->browse(
            function (Browser $browser) use ($page) {
                $browser->visit($page);
                $page->assertVisuals($browser);
                $page->assertForms($browser);
            }
        );

        $passwordReset->created_at = Carbon::now()->subHours(2);
        $passwordReset->save();

        $this->browse(
            function (Browser $browser) use ($page) {
                $browser->type('@password', 'vaultstack');
                $browser->type('@confirm-password', 'vaultstack');
                $browser->click('@save-password-button');
                $browser->screenshot('AuthPageSet2Test.checkSetPasswordErrorsWhenTokenIsExpiredWhenTryingToSaveNewPassword');
                $browser->assertRouteIs(NR::AUTH_SET_PASSWORD);
                $page->assertErrorPage($browser, HttpErrors::BAD_REQUEST);
                $browser->assertSee('The link has expired, please go back to the forgot password page and request a new email');
            }
        );
    }

    /**
     * Check to see that the error message is displayed when the passwords don't match.
     *
     * @test checkSetPasswordDisplayErrorMessageWhenPasswordsDontMatch
     * @group auth.set-password
     * @return void
     * @throws \Throwable
     */
    public function checkSetPasswordDisplayErrorMessageWhenPasswordsDontMatch()
    {
        //Setup Data
        $passwordReset = (new SecureToken())->createToken($this->adminUser, 'password_reset');

        $this->browse(
            function (Browser $browser) use ($passwordReset) {
                $page = new AuthSetPassword($passwordReset->token);
                $browser->visit($page);
                $page->assertVisuals($browser);
                $page->assertForms($browser);
                $browser->type('@password', 'vaultstack');
                $browser->type('@confirm-password', 'vaultstack2');
                $browser->click('@save-password-button');
                $browser->screenshot('AuthPageSet2Test.checkSetPasswordDisplayErrorMessageWhenPasswordsDontMatch');
                $browser->assertRouteIs(NR::AUTH_SET_PASSWORD);
                $browser->within(new AlertError(), function ($browser) {
                    $browser->assertSeeIn('@title', 'An Error Occurred');
                    $browser->assertSeeIn('@list', 'The passwords you have entered don\'t match, please try again.');
                });
            }
        );
    }
}
