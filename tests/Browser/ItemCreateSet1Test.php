<?php

namespace Tests\Browser;

use App\Models\VaultGroup;
use App\Models\VaultItem;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alerts\AlertError;
use Tests\Browser\Components\Alerts\AlertSuccess;
use Tests\Browser\Components\Item\ItemDetails;
use Tests\Browser\Pages\Item\ItemCreate;
use Tests\DuskTestCase;
use App\Enum\NamedRoutesEnum as NR;

class ItemCreateSet1Test extends DuskTestCase
{
    /**
     * Check item create page has all the correct elements.
     * @test checkItemCreatePageHasAllTheCorrectElements
     * @group item.create
     */
    public function checkItemCreatePageHasAllTheCorrectElements()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new ItemCreate());

            $browser->within(new ItemDetails(), function ($browser) {
                $browser->assertValue('@name', '');
                $browser->assertValue('@url', '');
                $browser->assertValue('@description', '');
                $browser->assertValue('@group', '');
            });

            $browser->screenshot('ItemCreateSet1Test.checkItemCreatePageHasAllTheCorrectElements');
        });
    }

    /**
     * Check item create page works correctly.
     * @test checkItemCreatePageWorksCorrectly
     * @group item.create
     */
    public function checkItemCreatePageWorksCorrectly()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::find(1);
        /** @var VaultItem $item */
        $item = VaultItem::factory()->make();

        $this->browse(function (Browser $browser) use ($item, $group) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new ItemCreate());

            $browser->within(new ItemDetails(), function ($browser) use ($item, $group) {
                $browser->type('@name', $item->name);
                $browser->type('@url', $item->url);
                $browser->type('@description', $item->description);
                $browser->select('@group', $group->id);
                $browser->click('@save-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Item created');
            });

            $browser->screenshot('ItemCreateSet1Test.checkItemCreatePageWorksCorrectly');
        });
    }

    /**
     * Check item create page returns error if missing data.
     * @test checkItemCreatePageReturnsErrorIfMissingData
     * @group item.create
     */
    public function checkItemCreatePageReturnsErrorIfMissingData()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new ItemCreate());

            $browser->within(new ItemDetails(), function ($browser) {
                $browser->type('@name', '');
                $browser->type('@url', '');
                $browser->type('@description', '');
                $browser->click('@save-button');
            });

            $browser->within(new AlertError(), function ($browser) {
                $browser->assertSeeIn('@title', 'An Error Occurred');
                $browser->assertSeeIn('@list', 'The name field is required');
                $browser->assertSeeIn('@list', 'The url field is required');
                $browser->assertSeeIn('@list', 'The description field is required');
            });

            $browser->screenshot('ItemCreateSet1Test.checkItemCreatePageReturnsErrorIfMissingData');
        });
    }

    /**
     * Check item create page back link works.
     * @test checkItemCreatePageBackLinkWorks
     * @group item.create
     */
    public function checkItemCreatePageBackLinkWorks()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new ItemCreate());

            $browser->assertSeeIn('@back-button', 'Back');
            $browser->click('@back-button');

            $browser->assertRouteIs(NR::VAULT_OVERVIEW);
            $browser->screenshot('ItemCreateSet1Test.checkItemCreatePageBackLinkWorks');
        });
    }
}
