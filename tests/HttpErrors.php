<?php

namespace Tests;

class HttpErrors
{
    public const BAD_REQUEST = ['code' => 400, 'text' => 'Bad Request'];
    public const UNAUTHORIZED = ['code' => 401, 'text' => 'Unauthorized'];
    public const FORBIDDEN = ['code' => 403, 'text' => 'Forbidden'];
    public const NOT_FOUND = ['code' => 404, 'text' => 'Not Found'];
    public const PAGE_EXPIRED = ['code' => 419, 'text' => 'Page Expired'];
    public const TOO_MANY_REQUESTS = ['code' => 429, 'text' => 'Too Many Requests'];
    public const SERVER_ERROR = ['code' => 500, 'text' => 'Server Error'];
    public const UNDER_MAINTENANCE = ['code' => 503, 'text' => 'Under Maintenance'];
}
