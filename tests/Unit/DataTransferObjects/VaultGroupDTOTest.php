<?php

namespace Tests\Unit\DataTransferObjects;

use App\DataTransferObjects\VaultGroupDTO;
use Illuminate\Http\Request;
use Tests\TestCase;
use App\Enum\VaultGroupColorsEnum;

class VaultGroupDTOTest extends TestCase
{
    /* =================================
    * createFromArray method
    * =================================*/

    /**
     * Check createFromArray method works correctly
     * @test checkCreateFromArrayMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultGroupDTO
     * @group app/DataTransferObjects/VaultGroupDTO:createFromArray
     */
    public function checkCreateFromArrayMethodWorksCorrectly()
    {
        $data = [
            'name' => 'name',
            'color' => VaultGroupColorsEnum::BLUE,
        ];

        $dto = VaultGroupDTO::createFromArray($data);

        $this->assertIsObject($dto);
        $this->assertInstanceOf(VaultGroupDTO::class, $dto);
        $this->assertSame($data['name'], $dto->name);
        $this->assertSame($data['color'], $dto->color->getValue());
    }

    /* =================================
    * createFromRequest method
    * =================================*/

    /**
     * Check createFromRequest method works correctly
     * @test checkCreateFromRequestMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultGroupDTO
     * @group app/DataTransferObjects/VaultGroupDTO:createFromRequest
     */
    public function checkCreateFromRequestMethodWorksCorrectly()
    {
        $data = [
            'name' => 'name',
            'color' => VaultGroupColorsEnum::BLUE,
        ];
        $request = new Request($data);

        $dto = VaultGroupDTO::createFromRequest($request);

        $this->assertIsObject($dto);
        $this->assertInstanceOf(VaultGroupDTO::class, $dto);
        $this->assertSame($data['name'], $dto->name);
        $this->assertSame($data['color'], $dto->color->getValue());
    }

    /* =================================
     * setUserId method
     * =================================*/

    /**
     * Check setUserId method works correctly
     * @test checkSetUserIdMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultGroupDTO
     * @group app/DataTransferObjects/VaultGroupDTO:setUserId
     */
    public function checkSetUserIdMethodWorksCorrectly()
    {
        $dto = VaultGroupDTO::createFromArray([]);

        $dto = $dto::setUserId($dto, $this->adminUser->id);

        $this->assertSame($this->adminUser->id, $dto->userId);
    }

    /* =================================
     * setCreatedBy method
     * =================================*/

    /**
     * Check setCreatedBy method works correctly
     * @test checkSetCreatedByMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultGroupDTO
     * @group app/DataTransferObjects/VaultGroupDTO:setCreatedBy
     */
    public function checkSetCreatedByMethodWorksCorrectly()
    {
        $dto = VaultGroupDTO::createFromArray([]);

        $dto = $dto::setCreatedBy($dto, $this->adminUser->id);

        $this->assertSame($this->adminUser->id, $dto->createdBy);
    }

    /* =================================
     * setUpdatedBy method
     * =================================*/

    /**
     * Check setUpdatedBy method works correctly
     * @test checkSetUpdatedByMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultGroupDTO
     * @group app/DataTransferObjects/VaultGroupDTO:setUpdatedBy
     */
    public function checkSetUpdatedByMethodWorksCorrectly()
    {
        $dto = VaultGroupDTO::createFromArray([]);

        $dto = $dto::setUpdatedBy($dto, $this->adminUser->id);

        $this->assertSame($this->adminUser->id, $dto->updatedBy);
    }

    /* =================================
     * setColor method
     * =================================*/

    /**
     * Check setColor method works correctly
     * @test checkSetColorMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultGroupDTO
     * @group app/DataTransferObjects/VaultGroupDTO:setColor
     */
    public function checkSetColorMethodWorksCorrectly()
    {
        $dto = VaultGroupDTO::createFromArray([]);

        $dto = $dto::setColor($dto, VaultGroupColorsEnum::GREEN);
        $this->assertSame(VaultGroupColorsEnum::GREEN, $dto->color->getValue());

        $dto = $dto::setColor($dto, 'red');
        $this->assertSame('red', $dto->color->getValue());
    }

    /* =================================
     * setName method
     * =================================*/

    /**
     * Check setName method works correctly
     * @test checkSetNameMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultGroupDTO
     * @group app/DataTransferObjects/VaultGroupDTO:setName
     */
    public function checkSetNameMethodWorksCorrectly()
    {
        $data = [
            'name' => 'name',
        ];
        $dto = VaultGroupDTO::createFromArray($data);

        $dto = $dto::setName($dto, 'PHPUnit');

        $this->assertSame('PHPUnit', $dto->name);
    }
}
