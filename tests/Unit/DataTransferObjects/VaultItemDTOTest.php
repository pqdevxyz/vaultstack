<?php

namespace Tests\Unit\DataTransferObjects;

use App\DataTransferObjects\VaultItemDTO;
use Illuminate\Http\Request;
use Tests\TestCase;

class VaultItemDTOTest extends TestCase
{
    /* =================================
    * createFromArray method
    * =================================*/

    /**
     * Check createFromArray method works correctly
     * @test checkCreateFromArrayMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultItemDTO
     * @group app/DataTransferObjects/VaultItemDTO:createFromArray
     */
    public function checkCreateFromArrayMethodWorksCorrectly()
    {
        $data = [
            'url' => 'url',
            'name' => 'name',
            'description' => 'description',
            'group' => 1,
        ];

        $dto = VaultItemDTO::createFromArray($data);

        $this->assertIsObject($dto);
        $this->assertInstanceOf(VaultItemDTO::class, $dto);
        $this->assertSame($data['url'], $dto->url);
        $this->assertSame($data['name'], $dto->name);
        $this->assertSame($data['description'], $dto->description);
        $this->assertSame($data['group'], $dto->group);
    }

    /* =================================
    * createFromRequest method
    * =================================*/

    /**
     * Check createFromRequest method works correctly
     * @test checkCreateFromRequestMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultItemDTO
     * @group app/DataTransferObjects/VaultItemDTO:createFromRequest
     */
    public function checkCreateFromRequestMethodWorksCorrectly()
    {
        $data = [
            'url' => 'url',
            'name' => 'name',
            'description' => 'description',
            'group' => 1,
        ];
        $request = new Request($data);

        $dto = VaultItemDTO::createFromRequest($request);

        $this->assertIsObject($dto);
        $this->assertInstanceOf(VaultItemDTO::class, $dto);
        $this->assertSame($data['url'], $dto->url);
        $this->assertSame($data['name'], $dto->name);
        $this->assertSame($data['description'], $dto->description);
        $this->assertSame($data['group'], $dto->group);
    }

    /* =================================
     * setName method
     * =================================*/

    /**
     * Check setName method works correctly
     * @test checkSetNameMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultItemDTO
     * @group app/DataTransferObjects/VaultItemDTO:setName
     */
    public function checkSetNameMethodWorksCorrectly()
    {
        $data = [
            'name' => 'name',
        ];
        $dto = VaultItemDTO::createFromArray($data);

        $dto = $dto::setName($dto, 'PHPUnit');

        $this->assertSame('PHPUnit', $dto->name);
    }

    /* =================================
     * setDescription method
     * =================================*/

    /**
     * Check setDescription method works correctly
     * @test checkSetDescriptionMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultItemDTO
     * @group app/DataTransferObjects/VaultItemDTO:setDescription
     */
    public function checkSetDescriptionMethodWorksCorrectly()
    {
        $data = [
            'description' => 'description',
        ];
        $dto = VaultItemDTO::createFromArray($data);

        $dto = $dto::setDescription($dto, 'PHPUnit');

        $this->assertSame('PHPUnit', $dto->description);
    }

    /* =================================
     * setURL method
     * =================================*/

    /**
     * Check setURL method works correctly
     * @test checkSetUrlMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultItemDTO
     * @group app/DataTransferObjects/VaultItemDTO:setURL
     */
    public function checkSetUrlMethodWorksCorrectly()
    {
        $data = [
            'url' => 'url',
        ];
        $dto = VaultItemDTO::createFromArray($data);

        $dto = $dto::setURL($dto, 'PHPUnit');

        $this->assertSame('PHPUnit', $dto->url);
    }

    /* =================================
     * setUserId method
     * =================================*/

    /**
     * Check setUserId method works correctly
     * @test checkSetUserIdMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultItemDTO
     * @group app/DataTransferObjects/VaultItemDTO:setUserId
     */
    public function checkSetUserIdMethodWorksCorrectly()
    {
        $dto = VaultItemDTO::createFromArray([]);

        $dto = $dto::setUserId($dto, $this->adminUser->id);

        $this->assertSame($this->adminUser->id, $dto->userId);
    }

    /* =================================
     * setCreatedBy method
     * =================================*/

    /**
     * Check setCreatedBy method works correctly
     * @test checkSetCreatedByMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultItemDTO
     * @group app/DataTransferObjects/VaultItemDTO:setCreatedBy
     */
    public function checkSetCreatedByMethodWorksCorrectly()
    {
        $dto = VaultItemDTO::createFromArray([]);

        $dto = $dto::setCreatedBy($dto, $this->adminUser->id);

        $this->assertSame($this->adminUser->id, $dto->createdBy);
    }

    /* =================================
     * setUpdatedBy method
     * =================================*/

    /**
     * Check setUpdatedBy method works correctly
     * @test checkSetUpdatedByMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultItemDTO
     * @group app/DataTransferObjects/VaultItemDTO:setUpdatedBy
     */
    public function checkSetUpdatedByMethodWorksCorrectly()
    {
        $dto = VaultItemDTO::createFromArray([]);

        $dto = $dto::setUpdatedBy($dto, $this->adminUser->id);

        $this->assertSame($this->adminUser->id, $dto->updatedBy);
    }

    /* =================================
     * setGroup method
     * =================================*/

    /**
     * Check setGroup method works correctly
     * @test checkSetGroupMethodWorksCorrectly
     * @group app/DataTransferObjects/VaultItemDTO
     * @group app/DataTransferObjects/VaultItemDTO:setGroup
     */
    public function checkSetGroupMethodWorksCorrectly()
    {
        $dto = VaultItemDTO::createFromArray([]);

        $dto = $dto::setGroup($dto, 99);

        $this->assertSame(99, $dto->group);
    }
}
