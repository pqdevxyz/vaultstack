<?php

namespace Tests\Unit\Mail;

use App\Enum\NamedRoutesEnum as NR;
use App\Mail\ResetYourPassword;
use Tests\TestCase;

class ResetYourPasswordMailTest extends TestCase
{
    /**
     * Check build method works correctly.
     *
     * @test checkBuildMethodWorksCorrectly
     * @group app/Mail/ResetYourPassword
     * @group app/Mail/ResetYourPassword:build
     */
    public function checkBuildMethodWorksCorrectly()
    {
        $mail = new ResetYourPassword('token');
        $mail = $mail->build();
        $this->assertTrue($mail->view == 'mail.auth.reset-your-password');
        $this->assertTrue($mail->token == route(NR::AUTH_SET_PASSWORD) . '?token=token');
    }
}
