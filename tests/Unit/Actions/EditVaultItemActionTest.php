<?php

namespace Tests\Unit\Actions;

use App\Actions\EditVaultItemAction;
use App\Exceptions\ModelNotFoundException;
use App\Models\VaultItem;
use Illuminate\Http\Request;
use Tests\TestCase;

class EditVaultItemActionTest extends TestCase
{
    public EditVaultItemAction $action;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = new EditVaultItemAction();
    }

    /* =================================
     * invoke method
     * =================================*/

    /**
     * Check invoke method works correctly
     * @test checkInvokeMethodWorksCorrectly
     * @group app/Actions/EditVaultItemAction
     * @group app/Actions/EditVaultItemAction:invoke
     */
    public function checkInvokeMethodWorksCorrectly()
    {
        $this->actingAs($this->adminUser);
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        $data = [
            'url' => 'url',
            'name' => 'name',
            'description' => 'description',
        ];
        $request = new Request($data);

        $updatedItem = ($this->action)($request, $item->id);

        $this->assertIsObject($updatedItem);
        $this->assertInstanceOf(VaultItem::class, $updatedItem);
        $this->assertSame($data['url'], $updatedItem->url);
        $this->assertSame($data['name'], $updatedItem->name);
        $this->assertSame($data['description'], $updatedItem->description);
    }

    /**
     * Check invoke method throws error when item not found
     * @test checkInvokeMethodThrowsErrorWhenItemNotFound
     * @group app/Actions/EditVaultItemAction
     * @group app/Actions/EditVaultItemAction:invoke
     */
    public function checkInvokeMethodThrowsErrorWhenItemNotFound()
    {
        $this->actingAs($this->adminUser);
        $data = [
            'url' => 'url',
            'name' => 'name',
            'description' => 'description',
        ];
        $request = new Request($data);
        $this->expectException(ModelNotFoundException::class);

        $updatedItem = ($this->action)($request, 0);
    }
}
