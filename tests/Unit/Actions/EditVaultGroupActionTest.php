<?php

namespace Tests\Unit\Actions;

use App\Actions\EditVaultGroupAction;
use App\Enum\VaultGroupColorsEnum;
use App\Exceptions\ModelNotFoundException;
use App\Models\VaultGroup;
use Illuminate\Http\Request;
use Tests\TestCase;

class EditVaultGroupActionTest extends TestCase
{
    public EditVaultGroupAction $action;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = new EditVaultGroupAction();
    }

    /* =================================
     * invoke method
     * =================================*/

    /**
     * Check invoke method works correctly
     * @test checkInvokeMethodWorksCorrectly
     * @group app/Actions/EditVaultGroupAction
     * @group app/Actions/EditVaultGroupAction:invoke
     */
    public function checkInvokeMethodWorksCorrectly()
    {
        $this->actingAs($this->adminUser);
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => $this->adminUser->id,
            'color' => VaultGroupColorsEnum::BLUE,
        ]);
        $data = [
            'name' => 'name',
            'color' => VaultGroupColorsEnum::GREEN,
        ];
        $request = new Request($data);

        $updatedGroup = ($this->action)($request, $group->id);

        $this->assertIsObject($updatedGroup);
        $this->assertInstanceOf(VaultGroup::class, $updatedGroup);
        $this->assertSame($data['name'], $updatedGroup->name);
        $this->assertSame($data['color'], $updatedGroup->color->getValue());
    }

    /**
     * Check invoke method throws error when group not found
     * @test checkInvokeMethodThrowsErrorWhenGroupNotFound
     * @group app/Actions/EditVaultGroupAction
     * @group app/Actions/EditVaultGroupAction:invoke
     */
    public function checkInvokeMethodThrowsErrorWhenGroupNotFound()
    {
        $this->actingAs($this->adminUser);
        $data = [
            'name' => 'name',
        ];
        $request = new Request($data);
        $this->expectException(ModelNotFoundException::class);

        $updatedGroup = ($this->action)($request, 0);
    }
}
