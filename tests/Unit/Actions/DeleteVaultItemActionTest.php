<?php

namespace Tests\Unit\Actions;

use App\Actions\DeleteVaultItemAction;
use App\Exceptions\ModelNotFoundException;
use App\Models\VaultItem;
use Tests\TestCase;

class DeleteVaultItemActionTest extends TestCase
{
    public DeleteVaultItemAction $action;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = new DeleteVaultItemAction();
    }

    /* =================================
     * invoke method
     * =================================*/

    /**
     * Check invoke method works correctly
     * @test checkInvokeMethodWorksCorrectly
     * @group app/Actions/DeleteVaultItemAction
     * @group app/Actions/DeleteVaultItemAction:invoke
     */
    public function checkInvokeMethodWorksCorrectly()
    {
        $this->actingAs($this->adminUser);
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);

        $deletedItem = ($this->action)($item->id);

        $this->assertTrue($deletedItem);
        $this->assertSoftDeleted('vault_items', [
            'id' => $item->id,
        ]);
    }

    /**
     * Check invoke method throws error when item not found
     * @test checkInvokeMethodThrowsErrorWhenItemNotFound
     * @group app/Actions/DeleteVaultItemAction
     * @group app/Actions/DeleteVaultItemAction:invoke
     */
    public function checkInvokeMethodThrowsErrorWhenItemNotFound()
    {
        $this->actingAs($this->adminUser);
        $this->expectException(ModelNotFoundException::class);

        $deletedItem = ($this->action)(0);
    }
}
