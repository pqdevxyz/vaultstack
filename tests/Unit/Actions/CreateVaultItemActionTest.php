<?php

namespace Tests\Unit\Actions;

use App\Actions\CreateVaultItemAction;
use App\Models\VaultItem;
use Illuminate\Http\Request;
use Tests\TestCase;

class CreateVaultItemActionTest extends TestCase
{
    public CreateVaultItemAction $action;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = new CreateVaultItemAction();
    }

    /* =================================
     * invoke method
     * =================================*/

    /**
     * Check invoke method works correctly
     * @test checkInvokeMethodWorksCorrectly
     * @group app/Actions/CreateVaultItemAction
     * @group app/Actions/CreateVaultItemAction:invoke
     */
    public function checkInvokeMethodWorksCorrectly()
    {
        $this->actingAs($this->adminUser);
        $data = [
            'url' => 'url',
            'name' => 'name',
            'description' => 'description',
        ];
        $request = new Request($data);

        $item = ($this->action)($request);

        $this->assertIsObject($item);
        $this->assertInstanceOf(VaultItem::class, $item);
    }
}
