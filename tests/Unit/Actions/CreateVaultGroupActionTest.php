<?php

namespace Tests\Unit\Actions;

use App\Actions\CreateVaultGroupAction;
use App\Enum\VaultGroupColorsEnum;
use App\Models\VaultGroup;
use Illuminate\Http\Request;
use Tests\TestCase;

class CreateVaultGroupActionTest extends TestCase
{
    public CreateVaultGroupAction $action;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = new CreateVaultGroupAction();
    }

    /* =================================
     * invoke method
     * =================================*/

    /**
     * Check invoke method works correctly
     * @test checkInvokeMethodWorksCorrectly
     * @group app/Actions/CreateVaultGroupAction
     * @group app/Actions/CreateVaultGroupAction:invoke
     */
    public function checkInvokeMethodWorksCorrectly()
    {
        $this->actingAs($this->adminUser);
        $data = [
            'name' => 'name',
            'color' => VaultGroupColorsEnum::BLUE,
        ];
        $request = new Request($data);

        $group = ($this->action)($request);

        $this->assertIsObject($group);
        $this->assertInstanceOf(VaultGroup::class, $group);
    }
}
