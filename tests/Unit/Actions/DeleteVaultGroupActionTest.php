<?php

namespace Tests\Unit\Actions;

use App\Actions\DeleteVaultGroupAction;
use App\Exceptions\ModelNotFoundException;
use App\Models\VaultGroup;
use Tests\TestCase;

class DeleteVaultGroupActionTest extends TestCase
{
    public DeleteVaultGroupAction $action;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = new DeleteVaultGroupAction();
    }

    /* =================================
     * invoke method
     * =================================*/

    /**
     * Check invoke method works correctly
     * @test checkInvokeMethodWorksCorrectly
     * @group app/Actions/DeleteVaultGroupAction
     * @group app/Actions/DeleteVaultGroupAction:invoke
     */
    public function checkInvokeMethodWorksCorrectly()
    {
        $this->actingAs($this->adminUser);
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);

        $deletedGroup = ($this->action)($group->id);

        $this->assertTrue($deletedGroup);
        $this->assertDatabaseMissing('vault_groups', [
            'id' => $group->id,
        ]);
    }

    /**
     * Check invoke method throws error when group not found
     * @test checkInvokeMethodThrowsErrorWhenGroupNotFound
     * @group app/Actions/DeleteVaultGroupAction
     * @group app/Actions/DeleteVaultGroupAction:invoke
     */
    public function checkInvokeMethodThrowsErrorWhenGroupNotFound()
    {
        $this->actingAs($this->adminUser);
        $this->expectException(ModelNotFoundException::class);

        $deletedGroup = ($this->action)(0);
    }
}
