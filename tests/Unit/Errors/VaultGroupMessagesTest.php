<?php

namespace Tests\Unit\Errors;

use App\Errors\VaultGroupMessages;
use Tests\TestCase;

class VaultGroupMessagesTest extends TestCase
{
    protected VaultGroupMessages $class;

    protected function setUp(): void
    {
        parent::setUp();
        $this->class = new VaultGroupMessages();
    }

    /* =================================
     * error array
     * =================================*/

    /**
     * Check error array is not blank
     * @test checkErrorArrayIsNotBlank
     * @group app/Errors/VaultGroupMessages
     * @group app/Errors/VaultGroupMessages:error
     */
    public function checkErrorArrayIsNotBlank()
    {
        $this->assertNotNull($this->class->errorMessages);
        $this->assertIsArray($this->class->errorMessages);
        $this->assertFalse([] == $this->class->errorMessages);
    }

    /* =================================
     * warning array
     * =================================*/

    /**
     * Check warning array is not blank
     * @test checkWarningArrayIsNotBlank
     * @group app/Errors/VaultGroupMessages
     * @group app/Errors/VaultGroupMessages:warning
     */
    public function checkWarningArrayIsNotBlank()
    {
        $this->assertNotNull($this->class->warningMessages);
        $this->assertIsArray($this->class->warningMessages);
        $this->assertFalse([] == $this->class->warningMessages);
    }

    /* =================================
     * success array
     * =================================*/

    /**
     * Check success array is not blank
     * @test checkSuccessArrayIsNotBlank
     * @group app/Errors/VaultGroupMessages
     * @group app/Errors/VaultGroupMessages:success
     */
    public function checkSuccessArrayIsNotBlank()
    {
        $this->assertNotNull($this->class->successMessages);
        $this->assertIsArray($this->class->successMessages);
        $this->assertFalse([] == $this->class->successMessages);
    }

    /* =================================
     * info array
     * =================================*/

    /**
     * Check info array is not blank
     * @test checkInfoArrayIsNotBlank
     * @group app/Errors/VaultGroupMessages
     * @group app/Errors/VaultGroupMessages:info
     */
    public function checkInfoArrayIsNotBlank()
    {
        $this->assertNotNull($this->class->infoMessages);
        $this->assertIsArray($this->class->infoMessages);
        $this->assertFalse([] == $this->class->infoMessages);
    }
}
