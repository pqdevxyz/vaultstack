<?php

namespace Tests\Unit\Exceptions;

use Tests\Doubles\Exceptions\MockException;
use Tests\TestCase;

class PQExceptionTest extends TestCase
{
    public MockException $exception;

    protected function setUp(): void
    {
        parent::setUp();
        $this->exception = new MockException();
    }

    /**
     * Check exception is setup correctly
     * @test checkExceptionIsSetupCorrectly
     * @group app/Exceptions/PQException
     */
    public function checkExceptionIsSetupCorrectly()
    {
        $this->assertSame('PHPUnit Message', $this->exception->pqMessage);
        $this->assertSame('phpunit-slug', $this->exception->pqSlug);
        $this->assertSame('phpunit-type', $this->exception->pqType);
    }

    /* =================================
     * setData method
     * =================================*/

    /**
     * Check setData method works correctly
     * @test checkSetDataMethodWorksCorrectly
     * @group app/Exceptions/PQException
     * @group app/Exceptions/PQException:setData
     */
    public function checkSetDataMethodWorksCorrectly()
    {
        $e = new $this->exception();
        $data = [
            'message' => 'Updated Message',
            'slug' => 'updated-slug',
            'type' => 'updated-type',
            'class' => 'UpdatedClass'
        ];

        /** @var MockException $e */
        $e = $e->setData($data['message'], $data['slug'], $data['type'], $data['class']);

        $this->assertSame($data['message'], $e->pqMessage);
        $this->assertSame($data['slug'], $e->pqSlug);
        $this->assertSame($data['type'], $e->pqType);
        $this->assertSame($data['class'], $e->pqClass);
    }

    /* =================================
     * setMessage method
     * =================================*/

    /**
     * Check setMessage method works correctly
     * @test checkSetMessageMethodWorksCorrectly
     * @group app/Exceptions/PQException
     * @group app/Exceptions/PQException:setMessage
     */
    public function checkSetMessageMethodWorksCorrectly()
    {
        $e = new $this->exception();
        $data = [
            'message' => 'Set Message',
        ];

        /** @var MockException $e */
        $e->setMessage($data['message']);

        $this->assertSame($data['message'], $e->pqMessage);
    }

    /* =================================
     * setSlug method
     * =================================*/

    /**
     * Check setSlug method works correctly
     * @test checkSetSlugMethodWorksCorrectly
     * @group app/Exceptions/PQException
     * @group app/Exceptions/PQException:setSlug
     */
    public function checkSetSlugMethodWorksCorrectly()
    {
        $e = new $this->exception();
        $data = [
            'slug' => 'set-slug',
        ];

        /** @var MockException $e */
        $e->setSlug($data['slug']);

        $this->assertSame($data['slug'], $e->pqSlug);
    }

    /* =================================
     * setType method
     * =================================*/

    /**
     * Check setType method works correctly
     * @test checkSetTypeMethodWorksCorrectly
     * @group app/Exceptions/PQException
     * @group app/Exceptions/PQException:setType
     */
    public function checkSetTypeMethodWorksCorrectly()
    {
        $e = new $this->exception();
        $data = [
            'type' => 'set-type',
        ];

        /** @var MockException $e */
        $e->setType($data['type']);

        $this->assertSame($data['type'], $e->pqType);
    }

    /* =================================
     * setClass method
     * =================================*/

    /**
     * Check setClass method works correctly
     * @test checkSetClassMethodWorksCorrectly
     * @group app/Exceptions/PQException
     * @group app/Exceptions/PQException:setClass
     */
    public function checkSetClassMethodWorksCorrectly()
    {
        $e = new $this->exception();
        $data = [
            'class' => 'SetClass',
        ];

        /** @var MockException $e */
        $e->setClass($data['class']);

        $this->assertSame($data['class'], $e->pqClass);
    }
}
