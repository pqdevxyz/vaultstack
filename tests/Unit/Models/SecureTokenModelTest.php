<?php

namespace Tests\Unit\Models;

use App\Models\SecureToken;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Tests\TestCase;

class SecureTokenModelTest extends TestCase
{
    /* =================================
     * user relationship
     * =================================*/

    /**
     * Check relationship returns linked User model correctly
     * @test checkUserRelationshipReturnsUserModelCorrectly
     * @group app/SecureToken
     * @group app/SecureToken:user
     */
    public function checkUserRelationshipReturnsUserModelCorrectly()
    {
        /** @var SecureToken $token*/
        $token = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'test',
        ]);
        $this->assertNotNull($token->user->email);
    }

    /**
     * Check createToken method works correctly
     * @test checkCreateTokenMethodWorksCorrectly
     * @group app/SecureToken
     * @group app/SecureToken:createToken
     */
    public function checkCreateTokenMethodWorksCorrectly()
    {
        $token = (new SecureToken())->createToken($this->adminUser, 'test');
        $this->assertDatabaseHas('secure_tokens', [
            'email' => $this->adminUser->email,
            'token' => $token->token,
            'type' => 'test'
        ]);
    }

    /**
     * Check checkIfTokenExists method works correctly
     * @test checkCheckIfTokenExistsMethodWorksCorrectly
     * @group app/SecureToken
     * @group app/SecureToken:checkIfTokenExists
     */
    public function checkCheckIfTokenExistsMethodWorksCorrectly()
    {
        $token = Str::uuid();
        $this->assertDatabaseMissing('secure_tokens', ['token' => $token]);
        $this->assertFalse(SecureToken::checkIfTokenExists($token, 'test'));
    }

    /**
     * Check checkIfTokenIsValid method works correctly
     * @test checkCheckIfTokenIsValidMethodWorksCorrectly
     * @group app/SecureToken
     * @group app/SecureToken:checkIfTokenIsValid
     */
    public function checkCheckIfTokenIsValidMethodWorksCorrectly()
    {
        $token = (new SecureToken())->createToken($this->adminUser, 'test');
        $this->assertTrue(SecureToken::checkIfTokenIsValid($token->token, 'test', 60));
        $token->created_at = Carbon::now()->subMinutes(120);
        $token->save();
        $this->assertFalse(SecureToken::checkIfTokenIsValid($token->token, 'test', 60));
    }

    /**
     * Check getToken method works correctly
     * @test checkGetTokenMethodWorksCorrectly
     * @group app/SecureToken
     * @group app/SecureToken:getToken
     */
    public function checkGetTokenMethodWorksCorrectly()
    {
        /** @var SecureToken $token*/
        $token = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'test',
        ]);
        $this->assertTrue($token->id == SecureToken::getToken($token->token, 'test')->id);
    }

    /**
     * Check deleteToken method works correctly
     * @test checkDeleteTokenMethodWorksCorrectly
     * @group app/SecureToken
     * @group app/SecureToken:deleteToken
     */
    public function checkDeleteTokenMethodWorksCorrectly()
    {
        /** @var SecureToken $token*/
        $token = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'test',
        ]);
        $this->assertDatabaseHas('secure_tokens', [
            'token' => $token->token,
            'type' => 'test'
        ]);
        SecureToken::deleteToken($token->token, 'test');
        $this->assertDatabaseMissing('secure_tokens', [
            'token' => $token->token,
            'type' => 'test'
        ]);
    }
}
