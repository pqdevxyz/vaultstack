<?php

namespace Tests\Unit\Models\VaultItem;

use App\Models\VaultItem;
use Tests\TestCase;

class VaultItemModelMessagesTest extends TestCase
{
    /* =================================
     * getMessage method
     * =================================*/

    /**
     * Check getMessage returns error message
     * @test checkGetMessageReturnsErrorMessage
     * @group app/Models/VaultItem
     * @group app/Models/VaultItem:getMessage
     */
    public function checkGetMessageReturnsErrorMessage()
    {
        $message = VaultItem::getMessage('error', 'not-found');
        $this->assertNotNull($message);
        $this->assertTrue($message !== 'Unknown Error');
        $message = VaultItem::getMessage('error', 'test');
        $this->assertNotNull($message);
        $this->assertTrue($message == 'Unknown Error');
    }

    /**
     * Check getMessage returns warning message
     * @test checkGetMessageReturnsWarningMessage
     * @group app/Models/VaultItem
     * @group app/Models/VaultItem:getMessage
     */
    public function checkGetMessageReturnsWarningMessage()
    {
        $message = VaultItem::getMessage('warning', 'cannot-update-vault-item');
        $this->assertNotNull($message);
        $this->assertTrue($message !== 'Unknown Warning');
        $message = VaultItem::getMessage('warning', 'test');
        $this->assertNotNull($message);
        $this->assertTrue($message == 'Unknown Warning');
    }

    /**
     * Check getMessage returns success message
     * @test checkGetMessageReturnsSuccessMessage
     * @group app/Models/VaultItem
     * @group app/Models/VaultItem:getMessage
     */
    public function checkGetMessageReturnsSuccessMessage()
    {
        $message = VaultItem::getMessage('success', 'created');
        $this->assertNotNull($message);
        $this->assertTrue($message !== 'Unknown Success');
        $message = VaultItem::getMessage('success', 'test');
        $this->assertNotNull($message);
        $this->assertTrue($message == 'Unknown Success');
    }

    /**
     * Check getMessage returns info message
     * @test checkGetMessageReturnsInfoMessage
     * @group app/Models/VaultItem
     * @group app/Models/VaultItem:getMessage
     */
    public function checkGetMessageReturnsInfoMessage()
    {
        $message = VaultItem::getMessage('info', 'test');
        $this->assertNotNull($message);
        $this->assertTrue($message == 'Unknown Info');
    }
}
