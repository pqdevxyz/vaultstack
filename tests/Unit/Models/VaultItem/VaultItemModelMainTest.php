<?php

namespace Tests\Unit\Models\VaultItem;

use App\Models\VaultItem;
use Tests\TestCase;

class VaultItemModelMainTest extends TestCase
{
    /* =================================
     * Created By Field
     * =================================*/

    /**
     * Check method returns null
     * @test checkGetCreatedByAttributeReturnsNull
     * @group app/VaultItem
     * @group app/VaultItem:getCreatedByAttribute
     */
    public function checkGetCreatedByAttributeReturnsNull()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->make([
            'additional_data' => null
        ]);
        $this->assertNull($item->created_by);
    }

    /**
     * Check method returns User model
     * @test checkGetCreatedByAttributeReturnsUserModel
     * @group app/VaultItem
     * @group app/VaultItem:getCreatedByAttribute
     */
    public function checkGetCreatedByAttributeReturnsUserModel()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->make([
            'created_by' => $this->adminUser->id
        ]);
        $this->assertNotNull($item->created_by);
        $this->assertTrue($item->created_by == $this->adminUser->id);
    }

    /**
     * Check method sets field
     * @test checkCreatedByAttributeSetsField
     * @group app/VaultItem
     * @group app/VaultItem:setCreatedByAttribute
     */
    public function checkCreatedByAttributeSetsField()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->make([
            'additional_data' => null
        ]);
        $item->created_by = $this->adminUser->id;
        $this->assertNotNull($item->created_by);
        $this->assertTrue($item->created_by == $this->adminUser->id);
    }

    /**
     * Check method updates field
     * @test checkCreatedByAttributeUpdatesField
     * @group app/VaultItem
     * @group app/VaultItem:setCreatedByAttribute
     */
    public function checkCreatedByAttributeUpdatesField()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->make([
            'created_by' => 0
        ]);
        $this->assertNull($item->created_by);
        $item->created_by = $this->adminUser->id;
        $this->assertNotNull($item->created_by);
        $this->assertTrue($item->created_by == $this->adminUser->id);
    }

    /* =================================
     * Updated By Field
     * =================================*/

    /**
     * Check method returns null
     * @test checkGetUpdatedByAttributeReturnsNull
     * @group app/VaultItem
     * @group app/VaultItem:getUpdatedByAttribute
     */
    public function checkGetUpdatedByAttributeReturnsNull()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->make([
            'additional_data' => null
        ]);
        $this->assertNull($item->updated_by);
    }

    /**
     * Check method returns User model
     * @test checkGetUpdatedByAttributeReturnsUserModel
     * @group app/VaultItem
     * @group app/VaultItem:getUpdatedByAttribute
     */
    public function checkGetUpdatedByAttributeReturnsUserModel()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->make([
            'updated_by' => $this->adminUser->id
        ]);
        $this->assertNotNull($item->updated_by);
        $this->assertTrue($item->updated_by == $this->adminUser->id);
    }

    /**
     * Check method sets field
     * @test checkUpdatedByAttributeSetsField
     * @group app/VaultItem
     * @group app/VaultItem:setUpdatedByAttribute
     */
    public function checkUpdatedByAttributeSetsField()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->make([
            'additional_data' => null
        ]);
        $item->updated_by = $this->adminUser->id;
        $this->assertNotNull($item->updated_by);
        $this->assertTrue($item->updated_by == $this->adminUser->id);
    }

    /**
     * Check method updates field
     * @test checkUpdatedByAttributeUpdatesField
     * @group app/VaultItem
     * @group app/VaultItem:setUpdatedByAttribute
     */
    public function checkUpdatedByAttributeUpdatesField()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->make([
            'updated_by' => 0
        ]);
        $this->assertNull($item->updated_by);
        $item->updated_by = $this->adminUser->id;
        $this->assertNotNull($item->updated_by);
        $this->assertTrue($item->updated_by == $this->adminUser->id);
    }
}
