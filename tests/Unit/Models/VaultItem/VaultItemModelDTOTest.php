<?php

namespace Tests\Unit\Models\VaultItem;

use App\DataTransferObjects\VaultItemDTO;
use App\Models\VaultItem;
use Tests\TestCase;

class VaultItemModelDTOTest extends TestCase
{
    /* =================================
     * createFromDTO method
     * =================================*/

    /**
     * Check createFromDTO returns correctly
     * @test checkCreateFromDtoReturnsCorrectly
     * @group app/Models/VaultItem
     * @group app/Models/VaultItem:createFromDTO
     */
    public function checkCreateFromDtoReturnsCorrectly()
    {
        $data = [
            'name' => 'name',
            'description' => 'description',
            'url' => 'url',
            'group' => 1,
        ];
        $dto = VaultItemDTO::createFromArray($data);

        $item = VaultItem::createFromDTO($dto);

        $this->assertInstanceOf(VaultItem::class, $item);
        $this->assertSame($data['url'], $item->url);
        $this->assertSame($data['name'], $item->name);
        $this->assertSame($data['description'], $item->description);
        $this->assertSame($data['group'], $item->vault_groups_id);
    }

    /* =================================
     * updateFromDTO method
     * =================================*/

    /**
     * Check updateFromDTO returns correctly
     * @test checkUpdateFromDtoReturnsCorrectly
     * @group app/Models/VaultItem
     * @group app/Models/VaultItem:updateFromDTO
     */
    public function checkUpdateFromDtoReturnsCorrectly()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        $data = [
            'name' => 'updateName',
            'description' => 'updateDescription',
            'url' => 'updateUrl',
            'group' => 1,
        ];
        $dto = VaultItemDTO::createFromArray($data);

        $updatedItem = VaultItem::updateFromDTO($item, $dto);

        $this->assertInstanceOf(VaultItem::class, $updatedItem);
        $this->assertSame($data['url'], $updatedItem->url);
        $this->assertSame($data['name'], $updatedItem->name);
        $this->assertSame($data['description'], $updatedItem->description);
        $this->assertSame($data['group'], $updatedItem->vault_groups_id);
    }

    /**
     * Check updateFromDTO returns correctly when not dirty
     * @test checkUpdateFromDtoReturnsCorrectlyWhenNotDirty
     * @group app/Models/VaultItem
     * @group app/Models/VaultItem:updateFromDTO
     */
    public function checkUpdateFromDtoReturnsCorrectlyWhenNotDirty()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => $this->adminUser->id,
            'vault_groups_id' => 1,
        ]);
        $dto = VaultItemDTO::createFromArray([
            'name' => $item->name,
            'description' => $item->description,
            'url' => $item->url,
            'group' => 1,
        ]);

        $updatedItem = VaultItem::updateFromDTO($item, $dto);

        $this->assertInstanceOf(VaultItem::class, $updatedItem);
        $this->assertSame($item->url, $updatedItem->url);
        $this->assertSame($item->name, $updatedItem->name);
        $this->assertSame($item->description, $updatedItem->description);
        $this->assertSame($item->vault_groups_id, $updatedItem->vault_groups_id);
    }
}
