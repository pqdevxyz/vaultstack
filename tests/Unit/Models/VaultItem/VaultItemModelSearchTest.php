<?php

namespace Tests\Unit\Models\VaultItem;

use App\Models\VaultItem;
use Illuminate\Support\Str;
use Tests\TestCase;

class VaultItemModelSearchTest extends TestCase
{
    /* =================================
     * searchableAs Method
     * =================================*/

    /**
     * Check searchableAs returns correctly
     * @test checkSearchableAsReturnsCorrectly
     * @group app/Models/VaultItem
     * @group app/Models/VaultItem:searchableAs
     */
    public function checkSearchableAsReturnsCorrectly()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->make();

        $this->assertStringContainsString('vaultstack', $item->searchableAs());
        $this->assertStringContainsString('vault-item', $item->searchableAs());
    }
}
