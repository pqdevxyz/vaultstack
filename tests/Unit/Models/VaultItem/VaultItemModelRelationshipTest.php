<?php

namespace Tests\Unit\Models\VaultItem;

use App\Models\VaultGroup;
use App\Models\VaultItem;
use Tests\TestCase;

class VaultItemModelRelationshipTest extends TestCase
{
    /* =================================
     * user relationship
     * =================================*/

    /**
     * Check relationship returns linked User model correctly
     * @test checkUserRelationshipReturnsUserModelCorrectly
     * @group app/VaultItem
     * @group app/VaultItem:user
     */
    public function checkUserRelationshipReturnsUserModelCorrectly()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => $this->adminUser->id
        ]);

        $this->assertNotNull($item->user->email);
    }

    /* =================================
     * group relationship
     * =================================*/

    /**
     * Check relationship returns linked VaultGroup model correctly
     * @test checkRelationshipReturnsLinkedVaultGroupModelCorrectly
     * @group app/VaultItem
     * @group app/VaultItem:group
     */
    public function checkRelationshipReturnsLinkedVaultGroupModelCorrectly()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create();
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'vault_groups_id' => $group->id,
        ]);

        $this->assertNotNull($item->group->name);
    }
}
