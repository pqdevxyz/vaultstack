<?php

namespace Tests\Unit\Models;

use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class UserModelTest extends TestCase
{
    /* =================================
    * Created By Field
    * =================================*/

    /**
     * Check method returns null
     * @test checkGetCreatedByAttributeReturnsNull
     * @group app/User
     * @group app/User:getCreatedByAttribute
     */
    public function checkGetCreatedByAttributeReturnsNull()
    {
        /** @var User $user*/
        $user = User::factory()->make([
            'additional_data' => null
        ]);
        $this->assertNull($user->created_by);
    }

    /**
     * Check method returns User model
     * @test checkGetCreatedByAttributeReturnsUserModel
     * @group app/User
     * @group app/User:getCreatedByAttribute
     */
    public function checkGetCreatedByAttributeReturnsUserModel()
    {
        /** @var User $user*/
        $user = User::factory()->make([
            'created_by' => $this->adminUser->id
        ]);
        $this->assertNotNull($user->created_by);
        $this->assertTrue($user->created_by == $this->adminUser->id);
    }

    /**
     * Check method sets field
     * @test checkCreatedByAttributeSetsField
     * @group app/User
     * @group app/User:setCreatedByAttribute
     */
    public function checkCreatedByAttributeSetsField()
    {
        /** @var User $user*/
        $user = User::factory()->make([
            'additional_data' => null
        ]);
        $user->created_by = $this->adminUser->id;
        $this->assertNotNull($user->created_by);
        $this->assertTrue($user->created_by == $this->adminUser->id);
    }

    /**
     * Check method updates field
     * @test checkCreatedByAttributeUpdatesField
     * @group app/User
     * @group app/User:setCreatedByAttribute
     */
    public function checkCreatedByAttributeUpdatesField()
    {
        /** @var User $user*/
        $user = User::factory()->make([
            'created_by' => 0
        ]);
        $this->assertNull($user->created_by);
        $user->created_by = $this->adminUser->id;
        $this->assertNotNull($user->created_by);
        $this->assertTrue($user->created_by == $this->adminUser->id);
    }

    /* =================================
     * Updated By Field
     * =================================*/

    /**
     * Check method returns null
     * @test checkGetUpdatedByAttributeReturnsNull
     * @group app/User
     * @group app/User:getUpdatedByAttribute
     */
    public function checkGetUpdatedByAttributeReturnsNull()
    {
        /** @var User $user*/
        $user = User::factory()->make([
            'additional_data' => null
        ]);
        $this->assertNull($user->updated_by);
    }

    /**
     * Check method returns User model
     * @test checkGetUpdatedByAttributeReturnsUserModel
     * @group app/User
     * @group app/User:getUpdatedByAttribute
     */
    public function checkGetUpdatedByAttributeReturnsUserModel()
    {
        /** @var User $user*/
        $user = User::factory()->make([
            'updated_by' => $this->adminUser->id
        ]);
        $this->assertNotNull($user->updated_by);
        $this->assertTrue($user->updated_by == $this->adminUser->id);
    }

    /**
     * Check method sets field
     * @test checkUpdatedByAttributeSetsField
     * @group app/User
     * @group app/User:setUpdatedByAttribute
     */
    public function checkUpdatedByAttributeSetsField()
    {
        /** @var User $user*/
        $user = User::factory()->make([
            'additional_data' => null
        ]);
        $user->updated_by = $this->adminUser->id;
        $this->assertNotNull($user->updated_by);
        $this->assertTrue($user->updated_by == $this->adminUser->id);
    }

    /**
     * Check method updates field
     * @test checkUpdatedByAttributeUpdatesField
     * @group app/User
     * @group app/User:setUpdatedByAttribute
     */
    public function checkUpdatedByAttributeUpdatesField()
    {
        /** @var User $user*/
        $user = User::factory()->make([
            'updated_by' => 0
        ]);
        $this->assertNull($user->updated_by);
        $user->updated_by = $this->adminUser->id;
        $this->assertNotNull($user->updated_by);
        $this->assertTrue($user->updated_by == $this->adminUser->id);
    }

    /* =================================
     * Full Name Field
     * =================================*/

    /**
     * Check method returns correctly.
     *
     * @test checkGetFullNameAttributeReturnsCorrectly
     * @group app/Models/User
     * @group app/Models/User:getFullNameAttribute
     */
    public function checkGetFullNameAttributeReturnsCorrectly()
    {
        /** @var User $user */
        $user = User::factory()->make();
        $this->assertNotNull($user->full_name);
        $this->assertTrue($user->full_name == $user->first_name . ' ' . $user->last_name);
    }

    /* =================================
     * Password Last Changed Field
     * =================================*/

    /**
     * Check method returns null.
     *
     * @test checkGetPasswordLastChangedAttributeReturnsNull
     * @group app/Models/User
     * @group app/Models/User:getPasswordLastChangedAttribute
     */
    public function checkGetPasswordLastChangedAttributeReturnsNull()
    {
        /** @var User $user */
        $user = User::factory()->make([
            'password_last_changed' => null,
        ]);
        $this->assertNull($user->password_last_changed);
    }

    /**
     * Check method returns User model.
     *
     * @test checkGetPasswordLastChangedAttributeReturnsUserModel
     * @group app/Models/User
     * @group app/Models/User:getPasswordLastChangedAttribute
     */
    public function checkGetPasswordLastChangedAttributeReturnsUserModel()
    {
        $now = Carbon::now();
        /** @var User $user */
        $user = User::factory()->make([
            'password_last_changed' => $now,
        ]);
        $this->assertNotNull($user->password_last_changed);
        $this->assertTrue($user->password_last_changed == $now);
    }

    /**
     * Check method sets field.
     *
     * @test checkPasswordLastChangedAttributeSetsField
     * @group app/Models/User
     * @group app/Models/User:setPasswordLastChangedAttribute
     */
    public function checkPasswordLastChangedAttributeSetsField()
    {
        $now = Carbon::now();
        /** @var User $user */
        $user = User::factory()->make([
            'additional_data' => null,
        ]);
        $user->password_last_changed = $now;
        $this->assertNotNull($user->password_last_changed);
        $this->assertTrue($user->password_last_changed == $now);
    }

    /**
     * Check method updates field.
     *
     * @test checkPasswordLastChangedAttributeUpdatesField
     * @group app/Models/User
     * @group app/Models/User:setPasswordLastChangedAttribute
     */
    public function checkPasswordLastChangedAttributeUpdatesField()
    {
        $now = Carbon::now();
        /** @var User $user */
        $user = User::factory()->make([
            'password_last_changed' => Carbon::parse('0'),
        ]);

        $this->assertEqualsWithDelta(Carbon::parse('0')->format('U'), $user->password_last_changed->format('U'), 1);

        $user->password_last_changed = $now;
        $this->assertNotNull($user->password_last_changed);
        $this->assertEqualsWithDelta($now->format('U'), $user->password_last_changed->format('U'), 2);
    }
}
