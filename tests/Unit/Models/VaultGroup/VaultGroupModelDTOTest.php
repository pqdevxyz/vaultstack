<?php

namespace Tests\Unit\Models\VaultGroup;

use App\DataTransferObjects\VaultGroupDTO;
use App\Enum\VaultGroupColorsEnum;
use App\Models\VaultGroup;
use Tests\TestCase;

class VaultGroupModelDTOTest extends TestCase
{
    /* =================================
     * createFromDTO method
     * =================================*/

    /**
     * Check createFromDTO returns correctly
     * @test checkCreateFromDtoReturnsCorrectly
     * @group app/Models/VaultGroup
     * @group app/Models/VaultGroup:createFromDTO
     */
    public function checkCreateFromDtoReturnsCorrectly()
    {
        $data = [
            'name' => 'name',
            'color' => VaultGroupColorsEnum::BLUE,
        ];
        $dto = VaultGroupDTO::createFromArray($data);

        $group = VaultGroup::createFromDTO($dto);

        $this->assertInstanceOf(VaultGroup::class, $group);
        $this->assertSame($data['name'], $group->name);
        $this->assertSame($data['color'], $group->color->getValue());
    }

    /* =================================
     * updateFromDTO method
     * =================================*/

    /**
     * Check updateFromDTO returns correctly
     * @test checkUpdateFromDtoReturnsCorrectly
     * @group app/Models/VaultGroup
     * @group app/Models/VaultGroup:updateFromDTO
     */
    public function checkUpdateFromDtoReturnsCorrectly()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => $this->adminUser->id,
            'color' => VaultGroupColorsEnum::BLUE,
        ]);
        $data = [
            'name' => 'updateName',
            'color' => VaultGroupColorsEnum::GREEN,
        ];
        $dto = VaultGroupDTO::createFromArray($data);

        $updatedGroup = VaultGroup::updateFromDTO($group, $dto);

        $this->assertInstanceOf(VaultGroup::class, $updatedGroup);
        $this->assertSame($data['name'], $updatedGroup->name);
        $this->assertSame($data['color'], $updatedGroup->color->getValue());
    }

    /**
     * Check updateFromDTO returns correctly when not dirty
     * @test checkUpdateFromDtoReturnsCorrectlyWhenNotDirty
     * @group app/Models/VaultGroup
     * @group app/Models/VaultGroup:updateFromDTO
     */
    public function checkUpdateFromDtoReturnsCorrectlyWhenNotDirty()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => $this->adminUser->id,
            'color' => VaultGroupColorsEnum::BLUE,
        ]);
        $dto = VaultGroupDTO::createFromArray([
            'name' => $group->name,
        ]);

        $updatedGroup = VaultGroup::updateFromDTO($group, $dto);

        $this->assertInstanceOf(VaultGroup::class, $updatedGroup);
        $this->assertSame($group->name, $updatedGroup->name);
        $this->assertSame($group->color, $updatedGroup->color);
    }
}
