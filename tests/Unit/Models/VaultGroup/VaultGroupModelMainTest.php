<?php

namespace Tests\Unit\Models\VaultGroup;

use App\Models\VaultGroup;
use Tests\TestCase;

class VaultGroupModelMainTest extends TestCase
{
    /* =================================
     * Created By Field
     * =================================*/

    /**
     * Check method returns null
     * @test checkGetCreatedByAttributeReturnsNull
     * @group app/VaultGroup
     * @group app/VaultGroup:getCreatedByAttribute
     */
    public function checkGetCreatedByAttributeReturnsNull()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->make([
            'additional_data' => null
        ]);
        $this->assertNull($group->created_by);
    }

    /**
     * Check method returns User model
     * @test checkGetCreatedByAttributeReturnsUserModel
     * @group app/VaultGroup
     * @group app/VaultGroup:getCreatedByAttribute
     */
    public function checkGetCreatedByAttributeReturnsUserModel()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->make([
            'created_by' => $this->adminUser->id
        ]);
        $this->assertNotNull($group->created_by);
        $this->assertTrue($group->created_by == $this->adminUser->id);
    }

    /**
     * Check method sets field
     * @test checkCreatedByAttributeSetsField
     * @group app/VaultGroup
     * @group app/VaultGroup:setCreatedByAttribute
     */
    public function checkCreatedByAttributeSetsField()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->make([
            'additional_data' => null
        ]);
        $group->created_by = $this->adminUser->id;
        $this->assertNotNull($group->created_by);
        $this->assertTrue($group->created_by == $this->adminUser->id);
    }

    /**
     * Check method updates field
     * @test checkCreatedByAttributeUpdatesField
     * @group app/VaultGroup
     * @group app/VaultGroup:setCreatedByAttribute
     */
    public function checkCreatedByAttributeUpdatesField()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->make([
            'created_by' => 0
        ]);
        $this->assertNull($group->created_by);
        $group->created_by = $this->adminUser->id;
        $this->assertNotNull($group->created_by);
        $this->assertTrue($group->created_by == $this->adminUser->id);
    }

    /* =================================
     * Updated By Field
     * =================================*/

    /**
     * Check method returns null
     * @test checkGetUpdatedByAttributeReturnsNull
     * @group app/VaultGroup
     * @group app/VaultGroup:getUpdatedByAttribute
     */
    public function checkGetUpdatedByAttributeReturnsNull()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->make([
            'additional_data' => null
        ]);
        $this->assertNull($group->updated_by);
    }

    /**
     * Check method returns User model
     * @test checkGetUpdatedByAttributeReturnsUserModel
     * @group app/VaultGroup
     * @group app/VaultGroup:getUpdatedByAttribute
     */
    public function checkGetUpdatedByAttributeReturnsUserModel()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->make([
            'updated_by' => $this->adminUser->id
        ]);
        $this->assertNotNull($group->updated_by);
        $this->assertTrue($group->updated_by == $this->adminUser->id);
    }

    /**
     * Check method sets field
     * @test checkUpdatedByAttributeSetsField
     * @group app/VaultGroup
     * @group app/VaultGroup:setUpdatedByAttribute
     */
    public function checkUpdatedByAttributeSetsField()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->make([
            'additional_data' => null
        ]);
        $group->updated_by = $this->adminUser->id;
        $this->assertNotNull($group->updated_by);
        $this->assertTrue($group->updated_by == $this->adminUser->id);
    }

    /**
     * Check method updates field
     * @test checkUpdatedByAttributeUpdatesField
     * @group app/VaultGroup
     * @group app/VaultGroup:setUpdatedByAttribute
     */
    public function checkUpdatedByAttributeUpdatesField()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->make([
            'updated_by' => 0
        ]);
        $this->assertNull($group->updated_by);
        $group->updated_by = $this->adminUser->id;
        $this->assertNotNull($group->updated_by);
        $this->assertTrue($group->updated_by == $this->adminUser->id);
    }
}
