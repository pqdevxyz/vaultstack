<?php

namespace Tests\Unit\Models\VaultGroup;

use App\Models\VaultGroup;
use App\Models\VaultItem;
use Tests\TestCase;

class VaultGroupModelRelationshipTest extends TestCase
{
    /* =================================
     * user relationship
     * =================================*/

    /**
     * Check relationship returns linked User model correctly
     * @test checkUserRelationshipReturnsUserModelCorrectly
     * @group app/VaultGroup
     * @group app/VaultGroup:user
     */
    public function checkUserRelationshipReturnsUserModelCorrectly()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => $this->adminUser->id
        ]);

        $this->assertNotNull($group->user->email);
    }

    /* =================================
     * user relationship
     * =================================*/

    /**
     * Check relationship returns linked VaultGroup model correctly
     * @test checkRelationshipReturnsLinkedVaultGroupModelCorrectly
     * @group app/VaultGroup
     * @group app/VaultGroup:items
     */
    public function checkRelationshipReturnsLinkedVaultGroupModelCorrectly()
    {
        $count = 3;
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create();
        /** @var VaultItem $items */
        $items = VaultItem::factory()->count($count)->create([
            'vault_groups_id' => $group->id,
        ]);

        $this->assertNotNull($group->items);
        $this->assertCount($count, $group->items);
    }
}
