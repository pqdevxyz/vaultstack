<?php

namespace Tests\Unit\Models\VaultGroup;

use App\Models\VaultGroup;
use Tests\TestCase;

class VaultGroupModelMessagesTest extends TestCase
{
    /* =================================
     * getMessage method
     * =================================*/

    /**
     * Check getMessage returns error message
     * @test checkGetMessageReturnsErrorMessage
     * @group app/Models/VaultGroup
     * @group app/Models/VaultGroup:getMessage
     */
    public function checkGetMessageReturnsErrorMessage()
    {
        $message = VaultGroup::getMessage('error', 'not-found');
        $this->assertNotNull($message);
        $this->assertTrue($message !== 'Unknown Error');
        $message = VaultGroup::getMessage('error', 'test');
        $this->assertNotNull($message);
        $this->assertTrue($message == 'Unknown Error');
    }

    /**
     * Check getMessage returns warning message
     * @test checkGetMessageReturnsWarningMessage
     * @group app/Models/VaultGroup
     * @group app/Models/VaultGroup:getMessage
     */
    public function checkGetMessageReturnsWarningMessage()
    {
        $message = VaultGroup::getMessage('warning', 'test');
        $this->assertNotNull($message);
        $this->assertTrue($message == 'Unknown Warning');
    }

    /**
     * Check getMessage returns success message
     * @test checkGetMessageReturnsSuccessMessage
     * @group app/Models/VaultGroup
     * @group app/Models/VaultGroup:getMessage
     */
    public function checkGetMessageReturnsSuccessMessage()
    {
        $message = VaultGroup::getMessage('success', 'created');
        $this->assertNotNull($message);
        $this->assertTrue($message !== 'Unknown Success');
        $message = VaultGroup::getMessage('success', 'test');
        $this->assertNotNull($message);
        $this->assertTrue($message == 'Unknown Success');
    }

    /**
     * Check getMessage returns info message
     * @test checkGetMessageReturnsInfoMessage
     * @group app/Models/VaultGroup
     * @group app/Models/VaultGroup:getMessage
     */
    public function checkGetMessageReturnsInfoMessage()
    {
        $message = VaultGroup::getMessage('info', 'test');
        $this->assertNotNull($message);
        $this->assertTrue($message == 'Unknown Info');
    }
}
