<?php

namespace Tests\Feature;

use App\Enum\NamedRoutesEnum as NR;
use App\Enum\VaultGroupColorsEnum;
use App\Models\VaultGroup;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\HandleDBTransactions;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class GroupControllerSet1Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs($this->adminUser);
    }

    /**
     * Check create group page loads.
     * @test checkCreateGroupPageLoads
     * @group app/Http/Controllers/GroupController
     * @group app/Http/Controllers/GroupController:getCreateGroup
     */
    public function checkCreateGroupPageLoads()
    {
        $response = $this->getJson(route(NR::GROUP_CREATE));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.group.create');
    }

    /**
     * Check create group page saves correctly.
     * @test checkCreateGroupPageSavesCorrectly
     * @group app/Http/Controllers/GroupController
     * @group app/Http/Controllers/GroupController:createGroup
     */
    public function checkCreateGroupPageSavesCorrectly()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->make();
        $body = [
            'name' => $group->name,
            'color' => VaultGroupColorsEnum::BLUE
        ];

        $response = $this->from(route(NR::VAULT_OVERVIEW))->postJson(route(NR::GROUP_CREATE), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::VAULT_OVERVIEW));
        $this->assertDatabaseHas('vault_groups', [
            'name' => $group->name,
        ]);
    }

    /**
     * Check edit group page loads.
     * @test checkEditGroupPageLoads
     * @group app/Http/Controllers/GroupController
     * @group app/Http/Controllers/GroupController:getEditGroup
     */
    public function checkEditGroupPageLoads()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => Auth::id(),
        ]);
        $response = $this->getJson(route(NR::GROUP_EDIT, ['id' => $group->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.group.edit');
    }

    /**
     * Check edit group page returns not found error.
     * @test checkEditGroupPageReturnsNotFoundError
     * @group app/Http/Controllers/GroupController
     * @group app/Http/Controllers/GroupController:getEditGroup
     */
    public function checkEditGroupPageReturnsNotFoundError()
    {
        $response = $this->getJson(route(NR::GROUP_EDIT, ['id' => 0]));
        $response->assertStatus(404);
    }

    /**
     * Check edit group page saves correctly.
     * @test checkEditGroupPageSavesCorrectly
     * @group app/Http/Controllers/GroupController
     * @group app/Http/Controllers/GroupController:editGroup
     */
    public function checkEditGroupPageSavesCorrectly()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => Auth::id(),
        ]);
        /** @var VaultGroup $updatedGroup */
        $updatedGroup = VaultGroup::factory()->make();
        $body = [
            'name' => $updatedGroup->name,
            'color' => $updatedGroup->color,
        ];

        $response = $this->actingAs($this->adminUser)->from(route(NR::VAULT_OVERVIEW))->postJson(route(NR::GROUP_EDIT, ['id' => $group->id]), $body);

        $response->assertStatus(302);
        $response->assertRedirect(route(NR::VAULT_OVERVIEW));
        $this->assertDatabaseHas('vault_groups', [
            'name' => $updatedGroup->name,
            'color' => $updatedGroup->color,
        ]);
    }

    /**
     * Check group remove works correctly.
     * @test checkGroupRemoveWorksCorrectly
     * @group app/Http/Controllers/GroupController
     * @group app/Http/Controllers/GroupController:removeGroup
     */
    public function checkGroupRemoveWorksCorrectly()
    {
        /** @var VaultGroup $group */
        $group = VaultGroup::factory()->create([
            'user_id' => Auth::id(),
        ]);

        $response = $this->actingAs($this->adminUser)->from(route(NR::VAULT_OVERVIEW))->getJson(route(NR::GROUP_REMOVE, ['id' => $group->id]));

        $response->assertStatus(302);
        $response->assertRedirect(route(NR::VAULT_OVERVIEW));
        $this->assertDatabaseMissing('vault_groups', [
            'id' => $group->id,
        ]);
    }

    /**
     * Check group remove returns not found error.
     * @test checkGroupRemoveReturnsNotFoundError
     * @group app/Http/Controllers/GroupController
     * @group app/Http/Controllers/GroupController:removeGroup
     */
    public function checkGroupRemoveReturnsNotFoundError()
    {
        $response = $this->actingAs($this->adminUser)->from(route(NR::VAULT_OVERVIEW))->getJson(route(NR::GROUP_REMOVE, ['id' => 0]));

        $response->assertStatus(302);
        $response->assertRedirect(route(NR::VAULT_OVERVIEW));
    }
}
