<?php

namespace Tests\Feature;

use App\Enum\NamedRoutesEnum as NR;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\HandleDBTransactions;
use Tests\TestCase;

class AuthControllerSet1Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;

    /**
     * Check login page loads.
     *
     * @test checkLoginPageLoads
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewLoginPage
     */
    public function checkLoginPageLoads()
    {
        $response = $this->getJson(route(NR::AUTH_LOGIN));
        $response->assertStatus(200);
        $response->assertViewIs('auth.login.login');
    }

    /**
     * Check login page redirects when already logged in.
     *
     * @test checkLoginPageRedirectsWhenAlreadyLoggedIn
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewLoginPage
     */
    public function checkLoginPageRedirectsWhenAlreadyLoggedIn()
    {
        $response = $this->actingAs($this->adminUser)->getJson(route(NR::AUTH_LOGIN));
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::HOME));
    }

    /**
     * Check login page works correctly.
     *
     * @test checkLoginPageWorksCorrectly
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:login
     */
    public function checkLoginPageWorksCorrectly()
    {
        $body = [
            'email' => $this->adminUser->email,
            'password' => 'vaultstack',
        ];
        $response = $this->from(route(NR::AUTH_LOGIN))->postJson(route(NR::AUTH_LOGIN), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::VAULT_OVERVIEW));
        $this->assertAuthenticatedAs($this->adminUser);
    }

    /**
     * Check login page displays error when password is incorrect.
     *
     * @test checkLoginPageDisplaysErrorWhenPasswordIsIncorrect
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:login
     */
    public function checkLoginPageDisplaysErrorWhenPasswordIsIncorrect()
    {
        $body = [
            'email' => $this->adminUser->email,
            'password' => 'password',
        ];
        $response = $this->from(route(NR::AUTH_LOGIN))->postJson(route(NR::AUTH_LOGIN), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::AUTH_LOGIN));
        $this->assertFalse($this->isAuthenticated());
    }
}
