<?php

namespace Tests\Feature;

use App\Enum\NamedRoutesEnum as NR;
use App\Models\SecureToken;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\HandleDBTransactions;
use Tests\TestCase;

class AuthControllerSet2Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;

    /**
     * Check forgot password page loads.
     *
     * @test checkForgotPasswordPageLoads
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewForgotPasswordPage
     */
    public function checkForgotPasswordPageLoads()
    {
        $response = $this->getJson(route(NR::AUTH_FORGOT_PASSWORD));
        $response->assertStatus(200);
        $response->assertViewIs('auth.password.forgot-password');
    }

    /**
     * Check forgot password page redirects when already logged in.
     *
     * @test checkForgotPasswordPageRedirectsWhenAlreadyLoggedIn
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewForgotPasswordPage
     */
    public function checkForgotPasswordPageRedirectsWhenAlreadyLoggedIn()
    {
        $response = $this->actingAs($this->adminUser)->getJson(route(NR::AUTH_FORGOT_PASSWORD));
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::HOME));
    }

    /**
     * Check forgot password page works correctly.
     *
     * @test checkForgotPasswordPageWorksCorrectly
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:sendPasswordReset
     */
    public function checkForgotPasswordPageWorksCorrectly()
    {
        $body = [
            'email' => $this->adminUser->email,
        ];
        $response = $this->from(route(NR::AUTH_FORGOT_PASSWORD))->postJson(route(NR::AUTH_FORGOT_PASSWORD), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::AUTH_FORGOT_PASSWORD));
        $this->assertDatabaseHas('secure_tokens', [
            'email' => $this->adminUser->email,
        ]);
    }

    /**
     * Check forgot password page redirects if user doesn't exist.
     *
     * @test checkForgotPasswordPageRedirectsIfUserDoesntExist
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:sendPasswordReset
     */
    public function checkForgotPasswordPageRedirectsIfUserDoesntExist()
    {
        $email = 'test@example.com';
        $body = [
            'email' => $email,
        ];
        $response = $this->from(route(NR::AUTH_FORGOT_PASSWORD))->postJson(route(NR::AUTH_FORGOT_PASSWORD), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::AUTH_FORGOT_PASSWORD));
        $this->assertDatabaseMissing('secure_tokens', [
            'email' => $email,
        ]);
    }
}
