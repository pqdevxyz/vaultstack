<?php

namespace Tests\Feature;

use App\Enum\NamedRoutesEnum as NR;
use App\Models\VaultItem;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\HandleDBTransactions;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class VaultControllerSet1Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs($this->adminUser);
    }

    /**
     * Check vault overview page loads.
     * @test checkVaultOverviewPageLoads
     * @group app/Http/Controllers/VaultController
     * @group app/Http/Controllers/VaultController:viewVault
     */
    public function checkVaultOverviewPageLoads()
    {
        $response = $this->getJson(route(NR::VAULT_OVERVIEW));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.vault.overview');
    }

    /**
     * Check vault overview page search works.
     * @test checkVaultOverviewPageSearchWorks
     * @group app/Http/Controllers/VaultController
     * @group app/Http/Controllers/VaultController:viewVault
     */
    public function checkVaultOverviewPageSearchWorks()
    {
        $response = $this->postJson(route(NR::VAULT_OVERVIEW), ['query' => 'example']);
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.vault.overview');
    }
}
