<?php

namespace Tests\Feature;

use App\Enum\NamedRoutesEnum as NR;
use App\Models\VaultItem;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\HandleDBTransactions;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class ItemControllerSet1Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs($this->adminUser);
    }

    /**
     * Check create item page loads.
     * @test checkCreateItemPageLoads
     * @group app/Http/Controllers/ItemController
     * @group app/Http/Controllers/ItemController:getCreateItem
     */
    public function checkCreateItemPageLoads()
    {
        $response = $this->getJson(route(NR::ITEM_CREATE));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.item.create');
    }

    /**
     * Check create item page saves correctly.
     * @test checkCreateItemPageSavesCorrectly
     * @group app/Http/Controllers/ItemController
     * @group app/Http/Controllers/ItemController:createItem
     */
    public function checkCreateItemPageSavesCorrectly()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->make();
        $body = [
            'name' => $item->name,
            'url' => $item->url,
            'description' => $item->description,
        ];

        $response = $this->from(route(NR::VAULT_OVERVIEW))->postJson(route(NR::ITEM_CREATE), $body);

        $response->assertStatus(302);
        $response->assertRedirect(route(NR::VAULT_OVERVIEW));
        $this->assertDatabaseHas('vault_items', [
            'name' => $item->name,
            'url' => $item->url,
        ]);
    }

    /**
     * Check view item page loads.
     * @test checkViewItemPageLoads
     * @group app/Http/Controllers/ItemController
     * @group app/Http/Controllers/ItemController:viewItem
     */
    public function checkViewItemPageLoads()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => Auth::id(),
        ]);

        $response = $this->getJson(route(NR::ITEM_VIEW, ['id' => $item->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.item.view');
    }

    /**
     * Check view item page returns not found.
     * @test checkViewItemPageReturnsNotFound
     * @group app/Http/Controllers/ItemController
     * @group app/Http/Controllers/ItemController:viewItem
     */
    public function checkViewItemPageReturnsNotFound()
    {
        $response = $this->getJson(route(NR::ITEM_VIEW, ['id' => 0]));
        $response->assertStatus(404);
    }

    /**
     * Check edit item page loads.
     * @test checkEditItemPageLoads
     * @group app/Http/Controllers/ItemController
     * @group app/Http/Controllers/ItemController:getEditItem
     */
    public function checkEditItemPageLoads()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => Auth::id(),
        ]);
        $response = $this->getJson(route(NR::ITEM_EDIT, ['id' => $item->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.item.edit');
    }

    /**
     * Check edit item page returns not found error.
     * @test checkEditItemPageReturnsNotFoundError
     * @group app/Http/Controllers/ItemController
     * @group app/Http/Controllers/ItemController:getEditItem
     */
    public function checkEditItemPageReturnsNotFoundError()
    {
        $response = $this->getJson(route(NR::ITEM_EDIT, ['id' => 0]));
        $response->assertStatus(404);
    }

    /**
     * Check edit item page saves correctly.
     * @test checkEditItemPageSavesCorrectly
     * @group app/Http/Controllers/ItemController
     * @group app/Http/Controllers/ItemController:editItem
     */
    public function checkEditItemPageSavesCorrectly()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => Auth::id(),
        ]);
        /** @var VaultItem $updatedItem */
        $updatedItem = VaultItem::factory()->make();
        $body = [
            'name' => $updatedItem->name,
            'url' => $updatedItem->url,
            'description' => $updatedItem->description,
        ];

        $response = $this->actingAs($this->adminUser)->from(route(NR::VAULT_OVERVIEW))->postJson(route(NR::ITEM_EDIT, ['id' => $item->id]), $body);

        $response->assertStatus(302);
        $response->assertRedirect(route(NR::VAULT_OVERVIEW));
        $this->assertDatabaseHas('vault_items', [
            'name' => $updatedItem->name,
            'url' => $updatedItem->url,
        ]);
    }

    /**
     * Check item remove works correctly.
     * @test checkItemRemoveWorksCorrectly
     * @group app/Http/Controllers/ItemController
     * @group app/Http/Controllers/ItemController:removeItem
     */
    public function checkItemRemoveWorksCorrectly()
    {
        /** @var VaultItem $item */
        $item = VaultItem::factory()->create([
            'user_id' => Auth::id(),
        ]);

        $response = $this->actingAs($this->adminUser)->from(route(NR::VAULT_OVERVIEW))->getJson(route(NR::ITEM_REMOVE, ['id' => $item->id]));

        $response->assertStatus(302);
        $response->assertRedirect(route(NR::VAULT_OVERVIEW));
        $this->assertSoftDeleted('vault_items', [
            'id' => $item->id,
        ]);
    }

    /**
     * Check item remove returns not found error.
     * @test checkItemRemoveReturnsNotFoundError
     * @group app/Http/Controllers/ItemController
     * @group app/Http/Controllers/ItemController:removeItem
     */
    public function checkItemRemoveReturnsNotFoundError()
    {
        $response = $this->actingAs($this->adminUser)->from(route(NR::VAULT_OVERVIEW))->getJson(route(NR::ITEM_REMOVE, ['id' => 0]));

        $response->assertStatus(302);
        $response->assertRedirect(route(NR::VAULT_OVERVIEW));
    }
}
