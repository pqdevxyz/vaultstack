<?php

namespace Tests\Feature;

trait DataProviderHelper
{
    public function twoArgsDataProvider(): array
    {
        return [
            [false, false],
            [false, true],

            [true, false],
        ];
    }

    public function threeArgsDataProvider(): array
    {
        return [
            [false, false, false],
            [false, false, true],
            [false, true, false],
            [false, true, true],

            [true, false, false],
            [true, false, true],
            [true, true, false],
        ];
    }

    public function sixArgsDataProvider(): array
    {
        return [
            [false, false, false, false, false, false],
            [true, true, true, true, true, false],
            [true, true, true, true, false, true],
            [true, true, true, false, true, true],
            [true, true, false, true, true, true],
            [true, false, true, true, true, true],
            [false, true, true, true, true, true],
        ];
    }
}
