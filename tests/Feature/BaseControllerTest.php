<?php

namespace Tests\Feature;

use App\Enum\NamedRoutesEnum as NR;
use App\Exceptions\PQException;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\HandleDBTransactions;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;

class BaseControllerTest extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;

    /* =================================
     * viewHome method
     * =================================*/

    /**
     * Check view home method returns correct redirect if not logged in.
     *
     * @test checkViewHomeMethodReturnsCorrectRedirectIfNotLoggedIn
     * @group app/Http/Controllers/Controller
     * @group app/Http/Controllers/Controller:viewHome
     */
    public function checkViewHomeMethodReturnsCorrectRedirectIfNotLoggedIn()
    {
        $response = $this->getJson(route(NR::HOME));
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::AUTH_LOGIN));
    }

    /**
     * Check view home method returns correct redirect if logged in.
     *
     * @test checkViewHomeMethodReturnsCorrectRedirectIfLoggedIn
     * @group app/Http/Controllers/Controller
     * @group app/Http/Controllers/Controller:viewHome
     */
    public function checkViewHomeMethodReturnsCorrectRedirectIfLoggedIn()
    {
        $this->actingAs($this->adminUser);
        $response = $this->getJson(route(NR::HOME));
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::VAULT_OVERVIEW));
    }

    /* =================================
     * handleError method
     * =================================*/

    /**
     * Check handleError method returns correct response when error code is not 500
     * @test checkHandleErrorMethodReturnsCorrectResponseWhenErrorCodeIsNot500
     * @group app/Http/Controllers/Controller
     * @group app/Http/Controllers/Controller:handleError
     */
    public function checkHandleErrorMethodReturnsCorrectResponseWhenErrorCodeIsNot500()
    {
        $exception = new PQException('Test Exception', 400);
        $error = 'Test Error';
        $message = 'Test Message';
        $class = 'PHPUnit';
        $method = 'Test Method';

        $response = self::runRestrictedMethod(new Controller(), 'handleError', [$exception, $error, $message, $class, $method]);

        /** @var JsonResponse $response */
        $data = $response->getData();
        $this->assertSame($error, $data->error);
        $this->assertSame($message, $data->message);
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * Check handleError method returns correct response when error code is 500
     * @test checkHandleErrorMethodReturnsCorrectResponseWhenErrorCodeIs500
     * @group app/Http/Controllers/Controller
     * @group app/Http/Controllers/Controller:handleError
     */
    public function checkHandleErrorMethodReturnsCorrectResponseWhenErrorCodeIs500()
    {
        $exception = new PQException('Test Exception', 500);
        $class = 'PHPUnit';
        $method = 'Test Method';

        $response = self::runRestrictedMethod(new Controller(), 'handleError', [$exception, '', '', $class, $method]);

        /** @var JsonResponse $response */
        $data = $response->getData();
        $this->assertSame('An error occurred', $data->error);
        $this->assertStringContainsString('Something went wrong, please try again later. Error Code:', $data->message);
        $this->assertEquals(500, $response->getStatusCode());
    }
}
