<?php

namespace Tests\Feature;

use App\Enum\NamedRoutesEnum as NR;
use App\Models\SecureToken;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\HandleDBTransactions;
use Tests\TestCase;

class AuthControllerSet3Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;

    /**
     * Check set password page loads.
     *
     * @test checkSetPasswordPageLoads
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewSetPasswordPage
     */
    public function checkSetPasswordPageLoads()
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'password_reset',
        ]);
        $response = $this->getJson(route(NR::AUTH_SET_PASSWORD) . '?token=' . $passwordReset->token);
        $response->assertStatus(200);
        $response->assertViewIs('auth.password.set-password');
    }

    /**
     * Check set password page errors with no token.
     *
     * @test checkSetPasswordPageErrorsWithNoToken
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewSetPasswordPage
     */
    public function checkSetPasswordPageErrorsWithNoToken()
    {
        $response = $this->getJson(route(NR::AUTH_SET_PASSWORD));
        $response->assertStatus(400);
    }

    /**
     * Check set password page errors with expired token.
     *
     * @test checkSetPasswordPageErrorsWithExpiredToken
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewSetPasswordPage
     */
    public function checkSetPasswordPageErrorsWithExpiredToken()
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'password_reset',
            'created_at' => Carbon::now()->subMinutes(120),
        ]);
        $response = $this->getJson(route(NR::AUTH_SET_PASSWORD) . '?token=' . $passwordReset->token);
        $response->assertStatus(400);
    }

    /**
     * Check set password page redirects when already logged in.
     *
     * @test checkSetPasswordPageRedirectsWhenAlreadyLoggedIn
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewSetPasswordPage
     */
    public function checkSetPasswordPageRedirectsWhenAlreadyLoggedIn()
    {
        $response = $this->actingAs($this->adminUser)->getJson(route(NR::AUTH_SET_PASSWORD));
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::HOME));
    }

    /**
     * Check set password page works correctly.
     *
     * @test checkSetPasswordPageWorksCorrectly
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:setPassword
     */
    public function checkSetPasswordPageWorksCorrectly()
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'password_reset',
        ]);
        $body = [
            'token' => $passwordReset->token,
            'password' => 'vaultstack',
            'confirm-password' => 'vaultstack',
        ];
        $response = $this->from(route(NR::AUTH_SET_PASSWORD))->postJson(route(NR::AUTH_SET_PASSWORD), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::AUTH_LOGIN));
    }

    /**
     * Check set password page errors when token has expired.
     *
     * @test checkSetPasswordPageErrorsWhenTokenHasExpired
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:setPassword
     */
    public function checkSetPasswordPageErrorsWhenTokenHasExpired()
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'password_reset',
            'created_at' => Carbon::now()->subMinutes(120),
        ]);
        $body = [
            'token' => $passwordReset->token,
            'password' => 'vaultstack',
            'confirm-password' => 'vaultstack',
        ];
        $response = $this->from(route(NR::AUTH_SET_PASSWORD))->postJson(route(NR::AUTH_SET_PASSWORD), $body);
        $response->assertStatus(400);
    }

    /**
     * Check set password page errors when passwords does not match.
     *
     * @test checkSetPasswordPageErrorsWhenPasswordsDoesNotMatch
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:setPassword
     */
    public function checkSetPasswordPageErrorsWhenPasswordsDontMatch()
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'password_reset',
        ]);
        $body = [
            'token' => $passwordReset->token,
            'password' => 'vaultstack',
            'confirm-password' => 'vaultstack2',
        ];
        $response = $this->from(route(NR::AUTH_SET_PASSWORD))->postJson(route(NR::AUTH_SET_PASSWORD), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::AUTH_SET_PASSWORD));
    }

    /**
     * Check set password page errors when user does not exist.
     *
     * @test checkSetPasswordPageErrorsWhenUserDoesNotExist
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:setPassword
     */
    public function checkSetPasswordPageErrorsWhenUserDoesNotExist()
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->faker->email,
            'user_id' => 0,
            'type' => 'password_reset',
        ]);
        $body = [
            'token' => $passwordReset->token,
            'password' => 'vaultstack',
            'confirm-password' => 'vaultstack',
        ];
        $response = $this->from(route(NR::AUTH_SET_PASSWORD))->postJson(route(NR::AUTH_SET_PASSWORD), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::AUTH_SET_PASSWORD));
    }

    /**
     * Check logout redirect after logging out the user.
     *
     * @test checkLogoutRedirectAfterLoggingOutTheUser
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:logout
     */
    public function checkLogoutRedirectAfterLoggingOutTheUser()
    {
        $this->actingAs($this->adminUser);
        $response = $this->getJson(route(NR::AUTH_LOGOUT));
        $response->assertStatus(302);
        $response->assertRedirect(route(NR::AUTH_LOGIN));
        $this->assertGuest();
    }
}
