<?php

namespace Tests\Doubles\Helpers;

use App\Helpers\PurpleModel\PurpleModel as PurpleModelTrait;

/**
 * @property array|null $additional_data
 */
class PurpleModel
{
    use PurpleModelTrait;

    public array|null $additional_data;
}
