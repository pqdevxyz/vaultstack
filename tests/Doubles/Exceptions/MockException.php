<?php

namespace Tests\Doubles\Exceptions;

use App\Exceptions\PQException;

class MockException extends PQException
{
    /** @var string */
    public string $pqMessage = 'PHPUnit Message';

    /** @var string */
    public string $pqSlug = 'phpunit-slug';

    /** @var string */
    public string $pqType = 'phpunit-type';
}
