<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Queue;
use ReflectionClass;
use ReflectionException;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithFaker;

    public $adminUser;

    protected static bool $init = false;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware(ThrottleRequests::class);

        if (is_null($this->adminUser)) {
            $adminUser = User::where('email', 'paul@quine.co.uk')->first();
            if (! is_null($adminUser)) {
                $this->adminUser = $adminUser;
            } else {
                $this->adminUser = User::factory()->create([
                    'first_name' => 'Paul',
                    'last_name' => 'Q',
                    'email' => 'paul@quine.co.uk',
                    'access_level' => 10,
                ]);
            }
        }

        Event::fake();
        Notification::fake();
        Mail::fake();
        Queue::fake();
    }

    /**
     * @param  object  $object
     * @param  string  $method
     * @param  array  $params
     * @return mixed
     */
    public static function runRestrictedMethod(object $object, string $method, array $params = []): mixed
    {
        try {
            $class = new ReflectionClass($object);
            $method = $class->getMethod($method);
            $method->setAccessible(true);

            return $method->invokeArgs($object, $params);
        } catch (ReflectionException $exception) {
            self::fail('ReflectionException: ' . $exception->getMessage());
        }
    }
}
