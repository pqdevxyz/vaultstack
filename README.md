# VaultStack
Manage your collection of links

## Authors
- [Paul Quine](https://bitbucket.org/pqdevxyz/)

## License
The project is licensed under [MIT](https://bitbucket.org/pqdevxyz/vaultstack/src/master/LICENSE.md)

## Acknowledgements
- [Laravel](https://laravel.com/)
- [Tailwind](http://tailwindcss.com/)
- [MJML](https://mjml.io/)
