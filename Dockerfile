FROM 339898008043.dkr.ecr.eu-west-2.amazonaws.com/vs-fullfat:latest

COPY ./ /var/www/html

COPY ./docker/scripts/boot-container.sh /usr/local/bin/boot-container
COPY ./docker/supervisor/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY ./docker/php/php.ini /etc/php/8.1/cli/conf.d/99-overrides.ini
COPY ./docker/nginx/main.conf /etc/nginx/nginx.conf
COPY ./docker/nginx/vs.conf /etc/nginx/sites-enabled/
COPY ./docker/alias /root/.bash_aliases

RUN chmod +x /usr/local/bin/boot-container
RUN chmod +x ./docker/scripts/setup-container.sh

EXPOSE 80

ENTRYPOINT ["boot-container"]

