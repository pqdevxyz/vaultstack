<div class="h-full">
    <a href="{{ $link }}">
        <div class="bg-white dark:bg-neutral-700 shadow-lg rounded-lg w-auto h-full">
            <div class="p-4 grid grid-cols-3 h-full">
                <div class="col-start-1 col-end-3">
                    <p class="text-xl font-medium mb-1 text-neutral-600 dark:text-neutral-300 vault-card-title">{{ $title }}</p>
                </div>
                <div class="col-start-3 col-end-4 text-right">
                    <p class="inline-block p-2 rounded-full bg-groups-{{ $color }}" data-belongs-to="{{ $belongsTo }}"></p>
                </div>
                <div class="col-span-3 mt-2">
                    <p class="text-md font-light italic mb-1 text-neutral-500 dark:text-neutral-400 vault-card-url">{{ $url }}</p>
                </div>
                <div class="col-span-3 mt-2">
                    <p class="text-md text-neutral-600 dark:text-neutral-300 truncate vault-card-description">{{ $description }}</p>
                </div>
            </div>
        </div>
    </a>
</div>
