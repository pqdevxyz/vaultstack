<nav class="bg-white dark:bg-neutral-900">
    <div class="container mx-auto">
        <div class="relative flex items-center justify-between h-16">
            <div class="flex-1 flex items-center justify-center">
                <div class="flex-grow-0 flex items-center mx-4">
                    <a href="{{ route($NR::HOME) }}">
                        <img class="h-8" src="/Logo.svg" alt="Logo">
                    </a>
                </div>
                <div class="flex-grow" id="nav-main-links">
                    <div class="flex justify-start space-x-4">
                        <a href="{{ route($NR::VAULT_OVERVIEW) }}" class="px-3 py-2 hidden md:block rounded-md text-sm font-medium text-neutral-700 hover:bg-neutral-300 dark:hover:bg-neutral-600 dark:text-neutral-200">Home</a>
                    </div>
                </div>
                <div class="flex-grow-0" id="nav-other-links">
                    <div class="flex space-x-4 mr-4 md:mr-0">
                        @stack('header-cta')
                        <p id="nav-auth" class="px-3 py-2 text-sm hidden md:block dark:text-neutral-200">Welcome {{ Auth::user()->full_name }}</p>
                        <a href="{{ route($NR::AUTH_LOGOUT) }}" class="px-3 py-2 rounded-md text-sm font-medium text-neutral-700 hover:bg-neutral-300 dark:hover:bg-neutral-600 dark:text-neutral-200">Logout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
