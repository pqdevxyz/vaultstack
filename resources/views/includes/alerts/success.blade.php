@if(session('success'))
    <div class="container mx-auto mt-6 max-w-4xl" id="alert-success">
        <div class="bg-success-100 dark:bg-success-900 shadow-lg rounded-lg mx-4">
            <div class="flex">
                <div class="flex flex-grow-0 items-center justify-center bg-success-300 dark:bg-success-700 rounded-l-lg">
                    <p class="text-2xl w-16 text-center"><em class="fas fa-check-circle text-success-700 dark:text-success-300"></em></p>
                </div>
                <div class="flex-grow mx-4">
                    <h4 class="font-medium text-success-900 dark:text-success-100 pt-4">Success</h4>
                    <div class="pb-4" id="alert-success-message">
                        @if(is_array(session('success')))
                            @foreach(session('success') as $message)
                                <p class="text-sm text-success-700 dark:text-success-300">
                                    {{ $message }}
                                </p>
                            @endforeach
                        @else
                            <p class="text-sm text-success-700 dark:text-success-300">
                                {{ session('success') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
