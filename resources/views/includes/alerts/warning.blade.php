@if(session('warning'))
    <div class="container mx-auto mt-6 max-w-4xl" id="alert-warning">
        <div class="bg-warning-100 dark:bg-warning-900 shadow-lg rounded-lg mx-4">
            <div class="flex">
                <div class="flex flex-grow-0 items-center justify-center bg-warning-300 dark:bg-warning-700 rounded-l-lg">
                    <p class="text-2xl w-16 text-center"><em class="fas fa-exclamation-triangle text-warning-700 dark:text-warning-300"></em></p>
                </div>
                <div class="flex-grow mx-4">
                    <h4 class="font-medium text-warning-900 dark:text-warning-100 pt-4">Warning</h4>
                    <div class="pb-4" id="alert-warning-message">
                        @if(is_array(session('warning')))
                            @foreach(session('warning') as $message)
                                <p class="text-sm text-warning-700 dark:text-warning-300">
                                    {{ $message }}
                                </p>
                            @endforeach
                        @else
                            <p class="text-sm text-warning-700 dark:text-warning-300">
                                {{ session('warning') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
