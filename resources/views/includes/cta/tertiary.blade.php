<a href="{{ $link }}" class="px-3 py-2 rounded-md hidden md:block text-sm font-medium text-neutral-500 hover:bg-neutral-100 dark:hover:bg-neutral-600 dark:text-neutral-300">{{ $name }}</a>
