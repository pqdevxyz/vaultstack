@extends('errors.base')
@section('site-title', 'Error - 419 | Vault Stack')

@section('content')
    @php
        $message = "Your request has expired, please try again.";
        if (isset($exception) && $exception->getMessage() != "") {
            $message = $exception->getMessage();
        }
    @endphp

    @include('errors.main', ['code' => 419, 'title' => 'Page Expired', 'message' => $message])
@endsection
