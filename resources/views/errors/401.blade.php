@extends('errors.base')
@section('site-title', 'Error - 401 | Vault Stack')

@section('content')
    @php
        $message = "Your request is unauthorized, please check the request and try again.";
        if (isset($exception) && $exception->getMessage() != "") {
            $message = $exception->getMessage();
        }
    @endphp

    @include('errors.main', ['code' => 401, 'title' => 'Unauthorized', 'message' => $message])
@endsection
