@extends('errors.base')
@section('site-title', 'Error - 500 | Vault Stack')

@section('content')
    <div class="min-h-screen flex items-center justify-center bg-primary-100 py-12 px-4 sm:px-6 lg:px-8">
        <div>
            <img class="mx-auto w-auto max-w-md mb-8" src="/vaultStack_500px.png" alt="Vault Stack">
            <div class="bg-white shadow-lg rounded-lg px-8">
                <div class="max-w-md w-full space-y-8 py-8">
                    <h2 class="text-center text-4xl font-medium text-primary-800 font-quicksand mb-4">
                        500
                    </h2>
                    <h4 class="text-center text-xl text-primary-600 font-quicksand mb-4">
                        Server Error
                    </h4>
                    <p class="text-center font-quicksand">Sorry something when wrong at our end</p>
                    @if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))
                        <p class="text-center test-sm font-quicksand">Error Id: {{ Sentry::getLastEventID() }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
