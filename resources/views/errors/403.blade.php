@extends('errors.base')
@section('site-title', 'Error - 403 | Vault Stack')

@section('content')
    @php
        $message = "Your request is forbidden, please check the request.";
        if (isset($exception) && $exception->getMessage() != "") {
            $message = $exception->getMessage();
        }
    @endphp

    @include('errors.main', ['code' => 403, 'title' => 'Forbidden', 'message' => $message])
@endsection
