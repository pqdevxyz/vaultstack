@extends('errors.base')
@section('site-title', 'Error - 400 | Vault Stack')

@section('content')
    @php
        $message = "Your request is invalid, please check the request and try again.";
        if (isset($exception) && $exception->getMessage() != "") {
            $message = $exception->getMessage();
        }
    @endphp

    @include('errors.main', ['code' => 400, 'title' => 'Bad Request', 'message' => $message])
@endsection
