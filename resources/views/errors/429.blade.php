@extends('errors.base')
@section('site-title', 'Error - 429 | Vault Stack')

@section('content')
    @php
        $message = "Please try again in a few minutes.";
        if (isset($exception) && $exception->getMessage() != "") {
            $message = $exception->getMessage();
        }
    @endphp

    @include('errors.main', ['code' => 429, 'title' => 'Too Many Requests', 'message' => $message])
@endsection
