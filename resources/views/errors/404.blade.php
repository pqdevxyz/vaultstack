@extends('errors.base')
@section('site-title', 'Error - 404 | Vault Stack')

@section('content')
    @php
        $message = "We couldn't find the page you are looking for, please check the link and try again.";
        if (isset($exception) && $exception->getMessage() != "") {
            $message = $exception->getMessage();
        }
    @endphp

    @include('errors.main', ['code' => 404, 'title' => 'Not Found', 'message' => $message])
@endsection
