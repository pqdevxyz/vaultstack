@extends('auth.base')
@section('site-title', 'Login | Vault Stack')

@section('content')
    <div class="min-h-screen flex items-center justify-center bg-primary-100 dark:bg-primary-800 py-12 px-4 sm:px-6 lg:px-8">
        <div>
            <a href="{{ route($NR::HOME) }}">
                <img class="mx-auto w-auto mb-8" src="/vaultStack_500px.png" alt="Vault Stack">
            </a>
            <div class="bg-white dark:bg-neutral-800 shadow-lg rounded-lg px-8">
                <div class="max-w-md w-full space-y-8">
                    <div class="pt-8">
                        <h4 class="text-center text-xl font-medium text-primary-700 dark:text-primary-300 font-quicksand mb-4">Login</h4>
                    </div>
                    <div>
                        @include('includes.alerts')
                    </div>
                    <form class="mt-8 space-y-8 mx-4" action="{{ route($NR::AUTH_LOGIN) }}" method="post">
                        @csrf
                        <div class="rounded-md shadow-sm space-y-4">
                            <div>
                                <label for="email-address" class="sr-only">Email address</label>
                                <input id="email-address" name="email" type="email" required class="appearance-none relative block w-full px-3 py-2 border border-primary-300 placeholder-neutral-500 text-neutral-900 rounded focus:outline-none focus:ring-primary-500 focus:border-primary-500 focus:z-10 dark:bg-neutral-800 dark:text-neutral-300 dark:placeholder-neutral-300" placeholder="Email address">
                            </div>
                            <div>
                                <label for="password" class="sr-only">Password</label>
                                <input id="password" name="password" type="password" required class="appearance-none relative block w-full px-3 py-2 border border-primary-300 placeholder-neutral-500 text-neutral-900 rounded focus:outline-none focus:ring-primary-500 focus:border-primary-500 focus:z-10 dark:bg-neutral-800 dark:text-neutral-300 dark:placeholder-neutral-300" placeholder="Password">
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="group relative w-full flex justify-center py-2 px-4 text-sm font-medium rounded-md text-white bg-primary-500 hover:bg-primary-400 dark:bg-primary-700 dark:text-primary-400 dark:hover:bg-primary-800">
                                Sign in
                            </button>
                        </div>
                        <div class="flex items-center justify-center pb-8">
                            <div class="text-sm">
                                <a href="{{ route($NR::AUTH_FORGOT_PASSWORD) }}" class="font-medium text-primary-700 hover:text-primary-400 dark:text-primary-500 dark:hover:text-primary-300" id="forgot-password-link">
                                    Forgot your password?
                                </a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
