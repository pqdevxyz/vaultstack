@extends('dashboard.base')
@section('site-title', 'Add Item | Vault Stack')

@section('content')
    <div class="max-w-screen-lg mx-auto mt-8 md:mt-16">
        <div class="mx-4 flex justify-between">
            <h1 class="text-2xl text-neutral-600 dark:text-neutral-300 font-light">Create Item</h1>
            <a href="{{ route($NR::VAULT_OVERVIEW) }}" id="back-button" class="px-3 py-2 rounded-md text-sm font-medium text-neutral-500 hover:bg-neutral-200 dark:hover:bg-neutral-600 dark:text-neutral-300">Back</a>
        </div>
        <hr class="mx-4 mt-8 border border-neutral-200 dark:border-neutral-700">
    </div>

    {{--Item Details--}}
    <div class="max-w-screen-lg mx-auto mt-8" id="item-details">
        @include('dashboard.item.components.item-details', ['action' => route($NR::ITEM_CREATE)])
    </div>

    <div class="mb-24"></div>
@endsection
