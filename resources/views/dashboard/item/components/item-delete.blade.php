<div class="mx-4 grid md:grid-cols-3 gap-6">
    <div class="md:col-start-1 md:col-end-2">
        <p class="text-lg text-neutral-800 dark:text-neutral-300">Delete Item</p>
        <p class="text-sm text-neutral-500 dark:text-neutral-400 mt-2">If you no longer need this item you can delete it here.</p>
    </div>
    <div class="md:col-start-2 md:col-end-4">
        <div class="flex justify-end items-end h-full">
            <a href="{{ route($NR::ITEM_REMOVE, ['id' => $item->id]) }}" id="item-delete-button" class="px-3 py-2 rounded-md text-sm font-medium text-center text-error-500 hover:bg-error-100 dark:hover:bg-error-700 dark:hover:text-error-300 w-full md:w-auto">Delete Item</a>
        </div>
    </div>
</div>
<hr class="mx-4 mt-8 border border-neutral-200 dark:border-neutral-700">
