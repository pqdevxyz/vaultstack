@section('banner')
    <div id="banner">
        <div class="bg-neutral-200 dark:bg-neutral-700" id="banner-item">
            <div class="container mx-auto">
                <div class="grid md:grid-cols-2">
                    <div class="md:col-span-2 my-8 mx-16 text-center">
                        <p class="font-semibold text-md uppercase tracking-wide text-neutral-500">Name</p>
                        <p class="font-light text-4xl text-neutral-900 dark:text-neutral-200">{{ $item->name }}</p>
                    </div>
                    <div class="my-8 mx-16 text-center">
                        <p class="font-semibold text-md uppercase tracking-wide text-neutral-500">URL</p>
                        <a href="{{ $item->url }}" class="text-3xl text-neutral-500 hover:text-neutral-600 dark:hover:text-neutral-400">{{ $item->url }}</a>
                    </div>
                    <div class="my-8 mx-16 text-center">
                        <p class="font-semibold text-md uppercase tracking-wide text-neutral-500">Created</p>
                        <p class="font-light text-3xl text-neutral-900 dark:text-neutral-200">{{ $item->created_at->format('dS M Y') }}</p>
                    </div>
                </div>
            </div>
        </div>
        @if (isset($item->group))
            <div class="bg-groups-{{ $item->group->color }}" id="banner-group">
                <div class="container mx-auto">
                    <div class="grid md:grid-cols-3 md:auto-cols-min">
                        <div class="my-8">
                            <p class="font-semibold text-sm uppercase tracking-wide text-neutral-500 text-center">Group Name</p>
                            <p class="font-light text-2xl text-neutral-800 dark:text-neutral-300 text-center">{{ $item->group->name }}</p>
                        </div>
                        <div class="my-8">
                            <p class="font-semibold text-sm uppercase tracking-wide text-neutral-500 text-center">Group Created</p>
                            <p class="font-light text-2xl text-neutral-800 dark:text-neutral-300 text-center">{{ $item->group->created_at->format('dS M Y') }}</p>
                        </div>
                        <div class="my-8 flex justify-center items-center">
                            <a href="{{ route($NR::GROUP_EDIT, ['id' => $item->group->id]) }}" id="group-edit-button" class="px-3 py-2 rounded-md text-sm font-medium text-neutral-800 bg-neutral-200 hover:bg-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:hover:bg-neutral-700">Edit Group</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
