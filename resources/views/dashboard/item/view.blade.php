@extends('dashboard.base')
@section('site-title', 'View Item | Vault Stack')

@include('dashboard.item.includes.banner', ['item' => $item])

@section('content')
    <div class="container mx-auto mt-16">
        <div class="mx-4">
            <p class="font-semibold uppercase tracking-wide text-sm mb-2 text-neutral-800 dark:text-neutral-300">Description</p>
            <article class="prose prose-2xl prose-stone dark:prose-invert mb-12">
                {{ $item->description }}
            </article>

            <div class="text-center">
                <a href="{{ route($NR::ITEM_EDIT, ['id' => $item->id]) }}" id="item-edit-button" class="px-3 py-2 rounded-md text-sm font-medium text-neutral-800 bg-neutral-200 hover:bg-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:hover:bg-neutral-700">Edit</a>
            </div>
        </div>
    </div>

    <div class="mb-24"></div>
@endsection
