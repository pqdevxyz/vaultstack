<div class="mx-4 grid md:grid-cols-3 gap-6">
    <div class="md:col-start-1 md:col-end-2">
        <p class="text-lg text-neutral-800 dark:text-neutral-300">Group Details</p>
        <p class="text-sm text-neutral-500 dark:text-neutral-400 mt-2">This is text.</p>
    </div>
    <div class="md:col-start-2 md:col-end-4">
        <form action="{{ $action }}" method="post">
            @csrf
            <div class="space-y-4 mx-auto">
                <div>
                    <label for="name" class="block text-sm font-medium text-neutral-700 dark:text-neutral-400">Name</label>
                    <div class="mt-1 flex rounded-md">
                        <input type="text" name="name" id="name" class="focus:ring-primary-500 focus:border-primary-500 flex-1 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" value="{{ $group->name ?? '' }}">
                    </div>
                </div>
                <div>
                    <label for="color" class="block text-sm font-medium text-neutral-700 dark:text-neutral-400">Color</label>
                    <div class="mt-1 flex rounded-md">
                        <select name="color" id="color" class="focus:ring-primary-500 focus:border-primary-500 flex-1 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600">
                            @foreach($colors as $color)
                                <option @if(isset($group->color) && $group->color == $color->getValue()) selected @endif value="{{ $color->getValue() }}">{{ ucwords($color->getValue()) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <div class="text-right pt-2">
                        <button type="submit" class="px-3 py-2 rounded-md text-sm font-medium text-primary-800 bg-primary-200 hover:bg-primary-300 dark:bg-primary-800 dark:text-primary-500 dark:hover:bg-primary-900 w-full md:w-auto">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<hr class="mx-4 mt-8 border border-neutral-200 dark:border-neutral-700">
