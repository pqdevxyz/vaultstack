<div class="mx-4 grid md:grid-cols-3 gap-6">
    <div class="md:col-start-1 md:col-end-2">
        <p class="text-lg text-neutral-800 dark:text-neutral-300">Delete Group</p>
        <p class="text-sm text-neutral-500 dark:text-neutral-400 mt-2">If you no longer need this group you can delete it here.</p>
    </div>
    <div class="md:col-start-2 md:col-end-4">
        <div class="flex justify-end items-end h-full">
            <a href="{{ route($NR::GROUP_REMOVE, ['id' => $group->id]) }}" id="group-delete-button" class="px-3 py-2 rounded-md text-sm font-medium text-center text-error-500 hover:bg-error-100 dark:hover:bg-error-700 dark:hover:text-error-300 w-full md:w-auto">Delete Group</a>
        </div>
    </div>
</div>
<hr class="mx-4 mt-8 border border-neutral-200 dark:border-neutral-700">
