<div class="container mx-auto mt-16">
    <form action="{{ route($NR::VAULT_OVERVIEW) }}" method="post">
        @csrf
        <div class="flex items-center justify-between mx-4">
            <div class="flex-1 flex items-center justify-between shadow-lg rounded-lg">
                <div class="w-full">
                    <label for="query" class="sr-only">Search</label>
                    <input id="query" name="query" type="text" class="appearance-none block w-full h-16 text-xl text-primary-600 placeholder:text-neutral-500 placeholder:italic rounded-lg form-clear" placeholder="Search" value="{{ request('query') }}">
                </div>
            </div>
        </div>
    </form>
</div>
