@section('banner')
    <div class="bg-primary-100 dark:bg-primary-900">
        <div class="container mx-auto">
            <div class="grid grid-cols-2 md:grid-cols-4 md:auto-cols-min">
                <div class="md:col-start-2 my-8">
                    <p class="font-semibold text-md uppercase tracking-wide text-center text-primary-500 dark:text-primary-600">Total Items</p>
                    <p class="font-light text-4xl text-center text-primary-900 dark:text-primary-300">{{ $totalItems }}</p>
                </div>
                <div class="my-8">
                    <p class="font-semibold text-md uppercase tracking-wide text-center text-primary-500 dark:text-primary-600">Total Groups</p>
                    <p class="font-light text-4xl text-center text-primary-900 dark:text-primary-300">{{ $totalGroups }}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
