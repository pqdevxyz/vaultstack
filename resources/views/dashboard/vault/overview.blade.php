@extends('dashboard.base')
@section('site-title', 'Vault | Vault Stack')

@push('header-cta')
    @include('includes.cta.secondary', ['name' => 'Add Group', 'link' => route($NR::GROUP_CREATE)])
    @include('includes.cta.primary', ['name' => 'Add Item', 'link' => route($NR::ITEM_CREATE)])
@endpush

@include('dashboard.vault.includes.banner', ['totalItems' => $totalItems ?? 0, 'totalGroups' => $totalGroups ?? 0])

@section('content')
    @include('dashboard.vault.includes.searchbar')
    <div class="container mx-auto mt-16" id="vault-cards">
        <div class="mx-4 grid md:grid-cols-3 gap-4 md:gap-8">
            @foreach($items as $item)
                <div class="vault-card">
                    @include('includes.cards.small', [
                           'title' => $item->name,
                           'description' => $item->description,
                           'url' => $item->url,
                           'link' => route($NR::ITEM_VIEW, ['id' => $item->id]),
                           'belongsTo' => $item->group->name ?? 'none',
                           'color' => $item->group->color ?? 'slate',
                       ])
                </div>
            @endforeach
        </div>
    </div>

    <div class="mb-24"></div>
@endsection
