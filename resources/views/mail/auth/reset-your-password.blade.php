
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <title>

    </title>
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        #outlook a { padding:0; }
        body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
        table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
        img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
        p { display:block;margin:13px 0; }
    </style>
    <!--[if mso]>
    <noscript>
        <xml>
            <o:OfficeDocumentSettings>
                <o:AllowPNG/>
                <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
    </noscript>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type="text/css">
        .mj-outlook-group-fix { width:100% !important; }
    </style>
    <![endif]-->

    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet" type="text/css">
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
        @import url(https://fonts.googleapis.com/css?family=Quicksand);
    </style>
    <!--<![endif]-->



    <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-100 { width:100% !important; max-width: 100%; }
        }
    </style>
    <style media="screen and (min-width:480px)">
        .moz-text-html .mj-column-per-100 { width:100% !important; max-width: 100%; }
    </style>
    <style type="text/css">
        [owa] .mj-column-per-100 { width:100% !important; max-width: 100%; }
    </style>

    <style type="text/css">



        @media only screen and (max-width:480px) {
            table.mj-full-width-mobile { width: 100% !important; }
            td.mj-full-width-mobile { width: auto !important; }
        }

    </style>
    <style type="text/css">

    </style>

</head>
<body style="word-spacing:normal;background-color:#e0f2fe;">


<div
    style="background-color:#e0f2fe;"
>


    <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->


    <div  style="margin:0px auto;max-width:600px;">

        <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
        >
            <tbody>
            <tr>
                <td
                    style="border:0px solid #ffffff;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;padding-left:0px;padding-right:0px;padding-top:0px;text-align:center;"
                >
                    <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->

                    <div
                        class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                    >

                        <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                        >
                            <tbody>

                            <tr>
                                <td
                                    style="font-size:0px;word-break:break-word;"
                                >

                                    <div
                                        style="height:25px;line-height:25px;"
                                    >&#8202;</div>

                                </td>
                            </tr>

                            </tbody>
                        </table>

                    </div>

                    <!--[if mso | IE]></td></tr></table><![endif]-->
                </td>
            </tr>
            </tbody>
        </table>

    </div>


    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->


    <div  style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">

        <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
        >
            <tbody>
            <tr>
                <td
                    style="border:0px solid #ffffff;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;padding-left:0px;padding-right:0px;padding-top:0px;text-align:center;"
                >
                    <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->

                    <div
                        class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                    >

                        <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                        >
                            <tbody>

                            <tr>
                                <td
                                    align="null" style="font-size:0px;padding:0px;padding-top:0px;padding-bottom:0px;word-break:break-word;"
                                >

                                    <div
                                        style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:null;color:#000000;"
                                    ><div style="border-top: 2px solid #38BDF8"></div></div>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >

                                    <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                    >
                                        <tbody>
                                        <tr>
                                            <td  style="width:500px;">

                                                <a
                                                    href="{{ route($NR::HOME) }}" target="_blank"
                                                >

                                                    <img
                                                        alt="Vault Stack Logo" height="auto" src="https://xqkgq.mjt.lu/tplimg/xqkgq/b/05glp/zlw.png" style="border:none;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" title="" width="500"
                                                    />

                                                </a>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    align="left" vertical-align="top" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;"
                                >

                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:32px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;"
                                    ><h1 class="text-build-content" style="text-align:center;; margin-top: 10px; margin-bottom: 10px" data-testid="MJtZJ8hG03_Wi"><span style="font-family:Quicksand, Helvetica, Arial, sans-serif;font-size:32px;">Reset your password</span></h1></div>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    style="font-size:0px;word-break:break-word;"
                                >

                                    <div
                                        style="height:25px;line-height:25px;"
                                    >&#8202;</div>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;"
                                >

                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:24px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;"
                                    ><p class="text-build-content" style="text-align: center; margin: 10px 0;; margin-top: 10px; margin-bottom: 10px" data-testid="Hyw_wetiq"><span style="font-family:Quicksand;font-size:18px;">If you requested a password change for you Vault Stack account, click on the button below to reset you password</span></p></div>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    style="font-size:0px;word-break:break-word;"
                                >

                                    <div
                                        style="height:25px;line-height:25px;"
                                    >&#8202;</div>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    align="center" vertical-align="middle" style="font-size:0px;padding:10px 25px 10px 25px;padding-right:25px;padding-left:25px;word-break:break-word;"
                                >

                                    <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;"
                                    >
                                        <tbody>
                                        <tr>
                                            <td
                                                align="center" bgcolor="#0284c7" role="presentation" style="border:0px solid #ffffff;border-radius:3px;cursor:auto;mso-padding-alt:20px 25px 20px 25px;background:#0284c7;" valign="middle"
                                            >
                                                <a
                                                    href="{{ $token }}" style="display:inline-block;background:#0284c7;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:20px 25px 20px 25px;mso-padding-alt:0px;border-radius:3px;" target="_blank"
                                                >
                                                    <span style="font-family:Quicksand;font-size:16px;">Reset my password</span>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    style="font-size:0px;word-break:break-word;"
                                >

                                    <div
                                        style="height:25px;line-height:25px;"
                                    >&#8202;</div>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;"
                                >

                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:14px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;"
                                    ><p class="text-build-content" data-testid="-elgKytKk" style="margin: 10px 0;; margin-top: 10px"><span style="font-family:Quicksand;font-size:14px;">Please note that the reset link will expire within 1 hour from when this email was sent.</span></p><p class="text-build-content" data-testid="-elgKytKk" style="margin: 10px 0;; margin-bottom: 10px"><span style="font-family:Quicksand;font-size:14px;">If you didn't request this email, then please ignore it.</span></p></div>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    style="font-size:0px;word-break:break-word;"
                                >

                                    <div
                                        style="height:25px;line-height:25px;"
                                    >&#8202;</div>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;"
                                >

                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:16px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;"
                                    ><p class="text-build-content" data-testid="eAiP87DYo" style="margin: 10px 0;; margin-top: 10px; margin-bottom: 10px"><span style="font-family:Quicksand;font-size:16px;">If the above button doesn't work, copy this URL into a browser:</span></p></div>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:0px 25px 20px 25px;padding-top:0px;padding-right:25px;padding-bottom:20px;padding-left:25px;word-break:break-word;"
                                >

                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:14px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;"
                                    ><p class="text-build-content" data-testid="XVgk5gvF8" style="margin: 10px 0;; margin-top: 10px; margin-bottom: 10px"><span style="font-family:Quicksand;font-size:14px;">{{ $token }}</span></p></div>

                                </td>
                            </tr>

                            </tbody>
                        </table>

                    </div>

                    <!--[if mso | IE]></td></tr></table><![endif]-->
                </td>
            </tr>
            </tbody>
        </table>

    </div>


    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->


    <div  style="margin:0px auto;max-width:600px;">

        <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
        >
            <tbody>
            <tr>
                <td
                    style="border:0px solid #ffffff;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;padding-left:0px;padding-right:0px;padding-top:0px;text-align:center;"
                >
                    <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->

                    <div
                        class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                    >

                        <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                        >
                            <tbody>

                            <tr>
                                <td
                                    align="null" style="font-size:0px;padding:0px;padding-top:0px;padding-bottom:0px;word-break:break-word;"
                                >

                                    <div
                                        style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:null;color:#000000;"
                                    ><div style="border-top:2px solid #38BDF8"></div></div>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    style="font-size:0px;word-break:break-word;"
                                >

                                    <div
                                        style="height:25px;line-height:25px;"
                                    >&#8202;</div>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;"
                                >

                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:14px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;"
                                    ><p class="text-build-content" style="text-align: center; margin: 10px 0;; margin-top: 10px" data-testid="8jwq9zzvG"><span style="color:#737373;font-family:Quicksand;font-size:14px;">Powered by pqdev.xyz</span></p><p class="text-build-content" style="text-align: center; margin: 10px 0;" data-testid="8jwq9zzvG"><span style="color:#737373;font-family:Quicksand;font-size:14px;">This email was sent to you by pqdev.xyz because you asked us to send you a password reset.</span></p><p class="text-build-content" style="text-align: center; margin: 10px 0;; margin-bottom: 10px" data-testid="8jwq9zzvG"><span style="color:#737373;font-family:Quicksand;font-size:14px;">Not working? Click here to view in browser.</span></p></div>

                                </td>
                            </tr>

                            <tr>
                                <td
                                    align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >

                                    <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                    >
                                        <tbody>
                                        <tr>
                                            <td  style="width:200px;">

                                                <a
                                                    href="https://pqdev.xyz/" target="_blank"
                                                >

                                                    <img
                                                        alt="pqdev.xyz Logo" height="auto" src="https://xqkgq.mjt.lu/tplimg/xqkgq/b/05glp/zll.png" style="border:none;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" title="" width="200"
                                                    />

                                                </a>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </td>
                            </tr>

                            </tbody>
                        </table>

                    </div>

                    <!--[if mso | IE]></td></tr></table><![endif]-->
                </td>
            </tr>
            </tbody>
        </table>

    </div>


    <!--[if mso | IE]></td></tr></table><![endif]-->


</div>

</body>
</html>
