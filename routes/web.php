<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\VaultController;
use Illuminate\Support\Facades\Route;
use App\Enum\NamedRoutesEnum as NR;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['headers'])->group(function () {
    Route::get('/', [Controller::class, 'viewHome'])->name(NR::HOME);

    Route::prefix('auth')->group(function () {
        Route::prefix('forgot-password')->group(function () {
            Route::post('', [AuthController::class, 'sendPasswordReset']);
            Route::get('', [AuthController::class, 'viewForgotPasswordPage'])->name(NR::AUTH_FORGOT_PASSWORD);
        });
        Route::prefix('set-password')->group(function () {
            Route::post('', [AuthController::class, 'setPassword']);
            Route::get('', [AuthController::class, 'viewSetPasswordPage'])->name(NR::AUTH_SET_PASSWORD);
        });
        Route::prefix('login')->group(function () {
            Route::post('', [AuthController::class, 'login']);
            Route::get('', [AuthController::class, 'viewLoginPage'])->name(NR::AUTH_LOGIN);
        });
        Route::prefix('logout')->name(NR::AUTH_LOGOUT)->group(function () {
            Route::get('', [AuthController::class, 'logout']);
        });
    });

    Route::middleware(['auth'])->group(function () {
        Route::prefix('vault')->group(function () {
            Route::post('', [VaultController::class, 'viewVault']);
            Route::get('', [VaultController::class, 'viewVault'])->name(NR::VAULT_OVERVIEW);
        });
        Route::prefix('item')->group(function () {
            Route::prefix('remove')->name(NR::ITEM_REMOVE)->group(function () {
                Route::get('{id}', [ItemController::class, 'removeItem']);
            });
            Route::prefix('view')->group(function () {
                Route::get('{id}', [ItemController::class, 'viewItem'])->name(NR::ITEM_VIEW);
            });
            Route::prefix('edit')->group(function () {
                Route::post('{id}', [ItemController::class, 'editItem']);
                Route::get('{id}', [ItemController::class, 'getEditItem'])->name(NR::ITEM_EDIT);
            });
            Route::prefix('create')->group(function () {
                Route::post('', [ItemController::class, 'createItem']);
                Route::get('', [ItemController::class, 'getCreateItem'])->name(NR::ITEM_CREATE);
            });
        });
        Route::prefix('group')->group(function () {
            Route::prefix('remove')->name(NR::GROUP_REMOVE)->group(function () {
                Route::get('{id}', [GroupController::class, 'removeGroup']);
            });
            Route::prefix('create')->group(function () {
                Route::post('', [GroupController::class, 'createGroup']);
                Route::get('', [GroupController::class, 'getCreateGroup'])->name(NR::GROUP_CREATE);
            });
            Route::prefix('edit')->group(function () {
                Route::post('{id}', [GroupController::class, 'editGroup']);
                Route::get('{id}', [GroupController::class, 'getEditGroup'])->name(NR::GROUP_EDIT);
            });
        });
    });
});
