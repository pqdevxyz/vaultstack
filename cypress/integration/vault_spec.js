describe('Vault', () => {

    let user = {
        email:'paul@quine.co.uk',
        fullName: 'Paul Q',
    }

    before(() => {
        cy.artisan('vs:init -U');
    });

    describe('New Item', () => {

        it('It works', () => {
            cy.login({
                email: user.email
            });

            //Visit Vault Overview page
            cy.visit('vault');

            /*
            * Create a new group called Group A
            * Check that it creates correctly and success alert is displayed
            */
            cy.get('#nav-other-links').contains('Add Group').click();

            cy.get('#name').clear().type('Group A');
            cy.get('#color').select('sky');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Group created');

            /*
            * Create a new group called Group B
            * Check that it creates correctly and success alert is displayed
            */
            cy.get('#nav-other-links').contains('Add Group').click();

            cy.get('#name').clear().type('Group B');
            cy.get('#color').select('rose');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Group created');

            /*
            * Create a new item called Test 1
            * Check that it creates correctly and success alert is displayed
            */
            cy.get('#nav-other-links').contains('Add Item').click();

            cy.get('#name').clear().type('Test 1');
            cy.get('#url').clear().type('https://pqdev.xyz/test-1');
            cy.get('#description').clear().type('Test Description 1 Group A');
            cy.get('#group').select('Group A');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Item created');

            /*
            * Create a new item called Test 1
            * Check that it creates correctly and success alert is displayed
            */
            cy.get('#nav-other-links').contains('Add Item').click();

            cy.get('#name').clear().type('Test 2');
            cy.get('#url').clear().type('https://pqdev.xyz/test-2');
            cy.get('#description').clear().type('Test Description 2 Group B');
            cy.get('#group').select('Group B');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Item created');

            /*
            * Search for Test 1
            * Assert that just Test 1 is visible
            */
            cy.get('#query').clear().type('Test 1').type('{enter}');

            cy.get('#vault-cards').contains('Test 1')
                .should('contain.text', 'https://pqdev.xyz/test-1')
                .and('contain.text', 'Test Description 1 Group A')
            cy.get('#vault-cards').contains('Test 2').should('not.exist')

            //Go to Test 1 View Item page
            cy.get('#vault-cards').contains('Test 1').click();

            cy.get('#banner-item').should('contain.text', 'Test 1')
                .and('contain.text', 'https://pqdev.xyz/test-1')
            cy.get('#banner-group').should('contain.text', 'Group A')

            /*
            * Edit Test 1
            * Update values
            */
            cy.get('#item-edit-button').click();

            cy.get('#name').clear().type('Test 1A');
            cy.get('#url').clear().type('https://pqdev.xyz/test-1a');
            cy.get('#description').clear().type('Test Description 1a Group A');
            cy.get('#group').select('Group A');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Item updated');

            /*
            * Check values have been updated
            * Return to the overview page
            */
            cy.get('#query').clear().type('Test 1A').type('{enter}');

            cy.get('#vault-cards').contains('Test 1A')
                .should('contain.text', 'https://pqdev.xyz/test-1a')
                .and('contain.text', 'Test Description 1a Group A')

            cy.get('#vault-cards').contains('Test 1A').click();

            cy.get('#banner-item').should('contain.text', 'Test 1A')
                .and('contain.text', 'https://pqdev.xyz/test-1a')
            cy.get('#banner-group').should('contain.text', 'Group A')

            cy.get('#nav-main-links').contains('Home').click();

            /*
            * Search for Test 2
            * Assert that just Test 2 is visible
            */
            cy.get('#query').clear().type('Test 2').type('{enter}');

            cy.get('#vault-cards').contains('Test 2')
                .should('contain.text', 'https://pqdev.xyz/test-2')
                .and('contain.text', 'Test Description 2 Group B')
            cy.get('#vault-cards').contains('Test 1').should('not.exist')

            //Go to Test 2 View Item page
            cy.get('#vault-cards').contains('Test 2').click();

            cy.get('#banner-item').should('contain.text', 'Test 2')
                .and('contain.text', 'https://pqdev.xyz/test-2')
            cy.get('#banner-group').should('contain.text', 'Group B')
        });

    });

});
