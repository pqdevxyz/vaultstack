const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

mix.copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/fonts/fontawesome');

mix.sass('resources/sass/app.scss', 'public/css').options({
    processCssUrls: false, postCss: [tailwindcss('./tailwind.config.js')],
});

mix.sass('resources/sass/font.scss', 'public/css', {
    sassOptions: {outputStyle: 'compressed'}
});

mix.js('resources/js/app.js', 'public/js');

mix.version();
