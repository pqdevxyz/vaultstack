<?php

namespace Database\Data;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Collection;

class VaultGroupSeedData
{
    use WithFaker;

    private array $groups = ['Backend', 'Frontend', 'Tech'];
    private array $colors = ['red', 'orange', 'lime'];

    public function __construct()
    {
        $this->setUpFaker();
    }

    public function __invoke(): Collection
    {
        return (collect($this->groups))->map(function ($group, $key) {
            return [
                'name' => $group,
                'color' => $this->colors[$key],
            ];
        });
    }
}
