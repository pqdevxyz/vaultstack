<?php

namespace Database\Data;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class VaultItemSeedData
{
    use WithFaker;

    private array $data = [
        'Backend' => [
            ['name' => 'Example 1a','description' => 'Example Description 1a', 'url' => 'https://pqdev.xyz/example-1a'],
            ['name' => 'Example 1b','description' => 'Example Description 1b', 'url' => 'https://pqdev.xyz/example-1b'],
            ['name' => 'Example 1c','description' => 'Example Description 1c', 'url' => 'https://pqdev.xyz/example-1c'],
            ['name' => 'Example 1d','description' => 'Example Description 1d', 'url' => 'https://pqdev.xyz/example-1d'],
            ['name' => 'Example 1e','description' => 'Example Description 1e', 'url' => 'https://pqdev.xyz/example-1e'],
            ['name' => 'Example 1f','description' => 'Example Description 1f', 'url' => 'https://pqdev.xyz/example-1f'],
        ],
        'Frontend' => [
            ['name' => 'Example 2a','description' => 'Example Description 2a', 'url' => 'https://pqdev.xyz/example-2a'],
            ['name' => 'Example 2b','description' => 'Example Description 2b', 'url' => 'https://pqdev.xyz/example-2b'],
            ['name' => 'Example 2c','description' => 'Example Description 2c', 'url' => 'https://pqdev.xyz/example-2c'],
            ['name' => 'Example 2d','description' => 'Example Description 2d', 'url' => 'https://pqdev.xyz/example-2d'],
            ['name' => 'Example 2e','description' => 'Example Description 2e', 'url' => 'https://pqdev.xyz/example-2e'],
            ['name' => 'Example 2f','description' => 'Example Description 2f', 'url' => 'https://pqdev.xyz/example-2f'],
            ['name' => 'Example 2g','description' => 'Example Description 2g', 'url' => 'https://pqdev.xyz/example-2g'],
            ['name' => 'Example 2h','description' => 'Example Description 2h', 'url' => 'https://pqdev.xyz/example-2h'],
            ['name' => 'Example 2i','description' => 'Example Description 2i', 'url' => 'https://pqdev.xyz/example-2i'],
            ['name' => 'Example 2j','description' => 'Example Description 2j', 'url' => 'https://pqdev.xyz/example-2j'],
        ],
        'Tech' => [
            ['name' => 'Example 3a','description' => 'Example Description 3a', 'url' => 'https://pqdev.xyz/example-3a'],
            ['name' => 'Example 3b','description' => 'Example Description 3b', 'url' => 'https://pqdev.xyz/example-3b'],
            ['name' => 'Example 3c','description' => 'Example Description 3c', 'url' => 'https://pqdev.xyz/example-3c'],
            ['name' => 'Example 3d','description' => 'Example Description 3d', 'url' => 'https://pqdev.xyz/example-3d'],
            ['name' => 'Example 4a','description' => 'Example Description 4a', 'url' => 'https://pqdev.xyz/example-4a'],
            ['name' => 'Example 4b','description' => 'Example Description 4b', 'url' => 'https://pqdev.xyz/example-4b'],
            ['name' => 'Example 4c','description' => 'Example Description 4c', 'url' => 'https://pqdev.xyz/example-4c'],
            ['name' => 'Example 4d','description' => 'Example Description 4d', 'url' => 'https://pqdev.xyz/example-4d'],
            ['name' => 'Example 4e','description' => 'Example Description 4e', 'url' => 'https://pqdev.xyz/example-4e'],
        ],
    ];

    public function __construct()
    {
        $this->setUpFaker();
    }

    public function __invoke(string $group): Collection|null
    {
        foreach (array_keys($this->data) as $key) {
            if (Str::contains($group, $key)) {
                return collect($this->data[$key]);
            }
        }

        return null;
    }
}
