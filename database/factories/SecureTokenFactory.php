<?php

namespace Database\Factories;

use App\Models\SecureToken;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/** @extends Factory<SecureToken> */
class SecureTokenFactory extends Factory
{
    /**
     * @var class-string<SecureToken>
     */
    protected $model = SecureToken::class;

    /**
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->safeEmail,
            'user_id' => $this->faker->randomNumber(1),
            'token' => Str::uuid(),
            'type' => 'password_reset',
            'created_at' => Carbon::now(),
        ];
    }
}
