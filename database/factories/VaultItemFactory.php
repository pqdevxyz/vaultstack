<?php

namespace Database\Factories;

use App\Models\VaultItem;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/** @extends Factory<VaultItem> */
class VaultItemFactory extends Factory
{
    /**
     * @var class-string<VaultItem>
     */
    protected $model = VaultItem::class;

    /**
     * @return array
     */
    public function definition()
    {
        $date = Carbon::now()->subDays(rand(1, 365));
        return [
            'name' => $this->faker->sentence,
            'description' => $this->faker->text(180),
            'url' => 'https://pqdev.xyz/' . $this->faker->slug,
            'vault_groups_id' => 1,
            'user_id' => $this->faker->randomNumber(1),
            'additional_data' => ['created_by' => 0, 'updated_by' => 0],
            'created_at' => $date,
            'updated_at' => $date,
        ];
    }
}
