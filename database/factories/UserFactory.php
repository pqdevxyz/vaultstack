<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/** @extends Factory<User> */
class UserFactory extends Factory
{
    /**
     * @var class-string<User>
     */
    protected $model = User::class;

    /**
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->safeEmail,
            'email_verified_at' => now(),
            'password' => Hash::make('vaultstack'),
            'remember_token' => Str::random(10),
            'access_level' => 0,
            'api_token' => null,
            'additional_data' => ['created_by' => 0, 'updated_by' => 0],
        ];
    }
}
