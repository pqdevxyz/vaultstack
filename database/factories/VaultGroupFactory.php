<?php

namespace Database\Factories;

use App\Models\VaultGroup;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/** @extends Factory<VaultGroup> */
class VaultGroupFactory extends Factory
{
    /**
     * @var class-string<VaultGroup>
     */
    protected $model = VaultGroup::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence,
            'color' => $this->faker->randomElement(['red', 'yellow', 'green', 'blue']),
            'user_id' => $this->faker->randomNumber(1),
            'additional_data' => ['created_by' => 0, 'updated_by' => 0],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
