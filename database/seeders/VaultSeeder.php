<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\VaultGroup;
use App\Models\VaultItem;
use Database\Data\VaultGroupSeedData;
use Database\Data\VaultItemSeedData;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VaultSeeder extends Seeder
{
    private User|null $user = null;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->user = User::where('email', 'paul@quine.co.uk')->first();
        if (is_null($this->user)) {
            $this->user = User::factory()->create([
                'first_name' => 'Paul',
                'last_name' => 'Q',
                'email' => 'paul@quine.co.uk',
            ]);
        }

        DB::table('vault_groups')->truncate();
        DB::table('vault_items')->truncate();

        /** @var VaultGroup $groupsData */
        $groupsData = (new VaultGroupSeedData())();

        $groupsData->each(function ($groupData) {
            /** @var VaultGroup $group */
            $group = VaultGroup::factory()->create([
                'name' => $groupData['name'],
                'color' => $groupData['color'],
                'user_id' => $this->user->id,
            ]);

            $itemsData = (new VaultItemSeedData())($groupData['name']);

            $itemsData->each(function ($itemData) use ($group) {
                VaultItem::factory()->create([
                    'name' => $itemData['name'],
                    'description' => $itemData['description'],
                    'url' => $itemData['url'],
                    'vault_groups_id' => $group->id,
                    'user_id' => $this->user->id,
                ]);
            });
        });
    }
}
