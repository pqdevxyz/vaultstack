<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
            'first_name' => 'Paul',
            'last_name' => 'Q',
            'email' => 'paul@quine.co.uk',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('vaultstack'),
            'api_token' => null,
            'additional_data' => json_encode(['created_by' => 0, 'updated_by' => 0]),
            'access_level' => 10,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
