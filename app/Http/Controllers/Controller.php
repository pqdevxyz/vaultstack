<?php

namespace App\Http\Controllers;

use App\Exceptions\PQException;
use App\Helpers\PurpleLog\Outputs\MonoLogger;
use App\Helpers\PurpleLog\Outputs\Opsgenie;
use App\Helpers\PurpleLog\Outputs\Sentry;
use App\Helpers\PurpleLog\PurpleLog;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Enum\NamedRoutesEnum as NR;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    /**
     * @param PQException $exception
     * @param string $pqError
     * @param string $pqMessage
     * @param string $class
     * @param string $method
     * @return JsonResponse
     */
    protected function handleError(PQException $exception, string $pqError, string $pqMessage, string $class, string $method): JsonResponse
    {
        if ($exception->getCode() != 500) {
            return response()->json(['error' => $pqError, 'message' => $pqMessage], $exception->getCode());
        }

        return response()->json(['error' => 'An error occurred', 'message' => 'Something went wrong, please try again later. Error Code:' . app('sentry')->getLastEventId()], 500);
    }

    /**
     * @param  Request  $request
     * @return RedirectResponse
     */
    protected function viewHome(Request $request): RedirectResponse
    {
        if (Auth::check()) {
            return redirect()->route(NR::VAULT_OVERVIEW);
        } else {
            return redirect()->route(NR::AUTH_LOGIN);
        }
    }
}
