<?php

namespace App\Http\Controllers;

use App\Actions\CreateVaultItemAction;
use App\Actions\DeleteVaultItemAction;
use App\Actions\EditVaultItemAction;
use App\Exceptions\PQException;
use App\Models\VaultGroup;
use App\Models\VaultItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Enum\NamedRoutesEnum as NR;

class ItemController extends Controller
{
    protected function viewItem(Request $request, int $id)
    {
        $item = VaultItem::with(['group'])->where('user_id', Auth::id())->find($id);
        if (is_null($item)) {
            abort(404);
        }

        return view('dashboard.item.view', ['item' => $item]);
    }

    protected function getCreateItem(Request $request)
    {
        $groups = VaultGroup::where('user_id', Auth::id())->get();
        return view('dashboard.item.create', ['item' => new VaultItem(), 'groups' => $groups]);
    }

    protected function createItem(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'url' => 'required',
            'description' => 'required',
        ]);

        try {
            (new CreateVaultItemAction())($request);
            return redirect()->route(NR::VAULT_OVERVIEW)->with('success', 'Item created');
        } catch (PQException $exception) {
            return redirect()->back()->with('error', 'Could not save item');
        }
    }

    protected function getEditItem(Request $request, int $id)
    {
        $groups = VaultGroup::where('user_id', Auth::id())->get();
        $item = VaultItem::with(['group'])->where('user_id', Auth::id())->find($id);
        if (is_null($item)) {
            abort(404);
        }

        return view('dashboard.item.edit', ['item' => $item, 'groups' => $groups]);
    }

    protected function editItem(Request $request, int $id)
    {
        $request->validate([
            'name' => 'required',
            'url' => 'required',
            'description' => 'required',
        ]);

        try {
            (new EditVaultItemAction())($request, $id);
            return redirect()->route(NR::VAULT_OVERVIEW)->with('success', 'Item updated');
        } catch (PQException $exception) {
            return redirect()->back()->with('error', 'Could not save item');
        }
    }

    protected function removeItem(Request $request, int $id)
    {
        try {
            (new DeleteVaultItemAction())($id);
            return redirect()->route(NR::VAULT_OVERVIEW)->with('success', 'Item deleted');
        } catch (PQException $exception) {
            return redirect()->back()->with('error', 'Could not delete item');
        }
    }
}
