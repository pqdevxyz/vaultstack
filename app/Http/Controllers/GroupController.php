<?php

namespace App\Http\Controllers;

use App\Actions\CreateVaultGroupAction;
use App\Actions\DeleteVaultGroupAction;
use App\Actions\EditVaultGroupAction;
use App\Enum\VaultGroupColorsEnum;
use App\Exceptions\PQException;
use App\Models\VaultGroup;
use Illuminate\Http\Request;
use App\Enum\NamedRoutesEnum as NR;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    protected function getCreateGroup(Request $request)
    {
        $colors = VaultGroupColorsEnum::values();
        return view('dashboard.group.create', ['group' => new VaultGroup(), 'colors' => $colors]);
    }

    protected function createGroup(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'color' => 'required',
        ]);

        $item = (new CreateVaultGroupAction())($request);

        if ($item === false) {
            return redirect()->back()->with('error', 'Could not save group');
        }
        return redirect()->route(NR::VAULT_OVERVIEW)->with('success', 'Group created');
    }

    protected function getEditGroup(Request $request, int $id)
    {
        $colors = VaultGroupColorsEnum::values();
        $group = VaultGroup::where('user_id', Auth::id())->find($id);
        if (is_null($group)) {
            abort(404);
        }

        return view('dashboard.group.edit', ['group' => $group, 'colors' => $colors]);
    }

    protected function editGroup(Request $request, int $id)
    {
        $request->validate([
            'name' => 'required',
            'color' => 'required',
        ]);

        try {
            (new EditVaultGroupAction())($request, $id);
            return redirect()->route(NR::VAULT_OVERVIEW)->with('success', 'Group updated');
        } catch (PQException $exception) {
            return redirect()->back()->with('error', 'Could not save group');
        }
    }

    protected function removeGroup(Request $request, int $id)
    {
        try {
            (new DeleteVaultGroupAction())($id);
            return redirect()->route(NR::VAULT_OVERVIEW)->with('success', 'Group deleted');
        } catch (PQException $exception) {
            return redirect()->back()->with('error', 'Could not delete group');
        }
    }
}
