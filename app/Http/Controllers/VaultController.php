<?php

namespace App\Http\Controllers;

use App\Models\VaultGroup;
use App\Models\VaultItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VaultController extends Controller
{
    protected function viewVault(Request $request)
    {
        if ($request->has('query')) {
            $items = VaultItem::search($request->input('query'))->where('user_id', Auth::id())->orderBy('created_at', 'desc')->get()->load(['group']);
            $totalItems = VaultItem::where('user_id', Auth::id())->count();
        } else {
            $items = VaultItem::with(['group'])->where('user_id', Auth::id())->orderBy('created_at', 'desc')->get();
            $totalItems = count($items);
        }

        $totalGroups = VaultGroup::where('user_id', Auth::id())->count();

        return view('dashboard.vault.overview', ['items' => $items, 'totalGroups' => $totalGroups, 'totalItems' => $totalItems]);
    }
}
