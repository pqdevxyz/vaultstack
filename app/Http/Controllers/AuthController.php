<?php

namespace App\Http\Controllers;

use App\Mail\ResetYourPassword;
use App\Models\SecureToken;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Enum\NamedRoutesEnum as NR;

class AuthController extends Controller
{
    /**
     * @param  Request  $request
     * @return bool|\Illuminate\Auth\Access\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|RedirectResponse|mixed
     */
    protected function viewLoginPage(Request $request)
    {
        if (Auth::check()) {
            return redirect()->route(NR::HOME);
        }

        return view('auth.login.login');
    }

    /**
     * @param  Request  $request
     * @return RedirectResponse
     */
    protected function login(Request $request): RedirectResponse
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        //Only look for users who are at least an admin
        $user = User::where('email', $request->input('email'))->first();
        if (! is_null($user) && Hash::check($request->input('password'), $user->password)) {
            Auth::login($user);
            return redirect()->route(NR::VAULT_OVERVIEW);
        } else {
            return redirect()->back()->with('error', 'Sorry, your email address or password is incorrect');
        }
    }

    /**
     * @param  Request  $request
     * @return bool|\Illuminate\Auth\Access\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|RedirectResponse|mixed
     */
    protected function viewForgotPasswordPage(Request $request)
    {
        if (Auth::check()) {
            return redirect()->route(NR::HOME);
        }

        return view('auth.password.forgot-password');
    }

    /**
     * @param  Request  $request
     * @return RedirectResponse
     */
    protected function sendPasswordReset(Request $request)
    {
        $request->validate([
            'email' => 'required',
        ]);

        $user = User::where('email', $request->input('email'))->first();
        if (! is_null($user)) {
            $token = (new SecureToken())->createToken($user, 'password_reset');
            Mail::to($user->email)->queue((new ResetYourPassword($token->token))->onQueue('long'));
        }

        return redirect()->back()->with('info', 'We will send you an email with a link to reset your password if your account exists in our system');
    }

    /**
     * @param  Request  $request
     * @return bool|\Illuminate\Auth\Access\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|RedirectResponse|mixed
     */
    protected function viewSetPasswordPage(Request $request)
    {
        if (Auth::check()) {
            return redirect()->route(NR::HOME);
        }

        //Check to see if the token is present
        $validator = Validator::make($request->all(), [
            'token' => 'required',
        ]);

        if ($validator->fails() || ! SecureToken::checkIfTokenExists($request->input('token'), 'password_reset')) {
            abort(400, 'The link you have clicked is not valid please check the link and try again.');
        }

        if (! SecureToken::checkIfTokenIsValid($request->input('token'), 'password_reset', 60)) {
            abort(400, 'The link has expired, please go back to the forgot password page and request a new email.');
        }

        return view('auth.password.set-password');
    }

    /**
     * @param  Request  $request
     * @return RedirectResponse
     */
    protected function setPassword(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'password' => 'required',
            'confirm-password' => 'required',
        ]);

        if (! SecureToken::checkIfTokenIsValid($request->input('token'), 'password_reset')) {
            abort(400, 'The link has expired, please go back to the forgot password page and request a new email.');
        }

        //Check to make sure that both passwords are the same
        if ($request->input('password') != $request->input('confirm-password')) {
            return redirect()->back()->with('error', 'The passwords you have entered don\'t match, please try again.');
        }

        $token = SecureToken::getToken($request->input('token'), 'password_reset');

        //Get the user based on the data stored with the reset token
        /** @var User|null $user */
        $user = User::find($token->user_id);
        if (is_null($user)) {
            return redirect()->back()->with('error', 'Unexpected error');
        }

        $user->password = Hash::make($request->input('password'));
        $user->password_last_changed = Carbon::now();
        $user->save();
        SecureToken::deleteToken($request->input('token'), 'password_reset');

        return redirect()->route(NR::AUTH_LOGIN)->with('success', 'Your password has been changed.');
    }

    /**
     * @param  Request  $request
     * @return RedirectResponse
     */
    protected function logout(Request $request): RedirectResponse
    {
        try {
            //Log the user out
            Auth::logout();
            session()->invalidate();
            session()->regenerateToken();
        } catch (Exception $exception) {
            Log::info($exception->getMessage(), ['action' => 'Logout', 'message' => 'Exception when trying to logout']);
        } finally {
            return redirect()->route(NR::AUTH_LOGIN);
        }
    }
}
