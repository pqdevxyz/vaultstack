<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

/**
 * @property int $id
 * @property string $token
 */
class SecureToken extends Model
{
    use HasFactory;

    public const UPDATED_AT = null;
    private const PASSWORD_RESET_VALID_FOR_MINS = 60;

    /**
     * Get the user the SecureToken belongs to
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Create a secure token (uuid) and stores it in the DB
     * @param User $user
     * @param string $type
     * @return self
     */
    public function createToken(User $user, string $type): self
    {
        $token = new $this();
        $token->email = $user->email;
        $token->user_id = $user->id;
        $token->token = Str::uuid();
        $token->type = $type;
        $token->save();
        return $token;
    }

    /**
     * Check to see if a token exists with that type
     * @param string $token
     * @param string $type
     * @return bool
     */
    public static function checkIfTokenExists(string $token, string $type): bool
    {
        return self::where('token', $token)->where('type', $type)->count() > 0;
    }

    /**
     * Check to see if the token is still valid
     * @param string $token
     * @param string $type
     * @param int $timeOffset
     * @return bool
     */
    public static function checkIfTokenIsValid(string $token, string $type, int $timeOffset = self::PASSWORD_RESET_VALID_FOR_MINS): bool
    {
        $token = self::where('token', $token)->where('type', $type)->first();
        return Carbon::parse($token->created_at)->greaterThanOrEqualTo(Carbon::now()->subMinutes($timeOffset));
    }

    /**
     * Get the token
     * @param string $token
     * @param string $type
     * @return mixed
     */
    public static function getToken(string $token, string $type)
    {
        return self::where('token', $token)->where('type', $type)->first();
    }

    /**
     * Delete the token
     * @param string $token
     * @param string $type
     */
    public static function deleteToken(string $token, string $type)
    {
        self::where('token', $token)->where('type', $type)->delete();
    }
}
