<?php

namespace App\Models;

use App\Helpers\PurpleModel\PurpleModel;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $api_token
 * @property array|null $additional_data
 * @property string|null $remember_token
 * @property int $access_level
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 */
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasFactory;
    use PurpleModel;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'api_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'additional_data' => 'array',
    ];

    /**
     * @var array
     */
    protected $appends = ['full_name'];

    /**
     * Get the full name based on the first name and last name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Get the time password was last changed from the additional_data attribute.
     *
     * @return Carbon|null
     */
    public function getPasswordLastChangedAttribute()
    {
        return isset($this->additional_data['password_last_changed']) ? Carbon::parse($this->additional_data['password_last_changed']) : null;
    }

    /**
     * Set the password last changed field for the user in the additional_data attribute.
     *
     * @param  Carbon|null  $value
     */
    public function setPasswordLastChangedAttribute(Carbon $value = null)
    {
        $data = $this->additional_data ?? [];
        $data['password_last_changed'] = $value;
        $this->additional_data = $data;
    }
}
