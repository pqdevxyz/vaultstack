<?php

namespace App\Models;

use App\DataTransferObjects\VaultItemDTO;
use App\Errors\VaultItemMessages;
use App\Exceptions\ModelNotSavedException;
use App\Helpers\PurpleModel\PurpleModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;

/**
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $description
 * @property int|null $vault_groups_id
 * @property array $additional_data
 */
class VaultItem extends Model
{
    use SoftDeletes;
    use HasFactory;
    use PurpleModel;
    use Searchable;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'additional_data' => 'array',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return BelongsTo
     */
    public function group(): BelongsTo
    {
        return $this->belongsTo(VaultGroup::class, 'vault_groups_id');
    }

    /**
     * Get the index name for the model.
     * @return string
     */
    public function searchableAs(): string
    {
        return Str::lower(config('app.env') . '.' . config('app.name') . ':vault-item');
    }

    /**
     * Get a message (error, warning, info or success)
     * @param string $type
     * @param string $slug
     * @return string
     */
    public static function getMessage(string $type, string $slug): string
    {
        return (new VaultItemMessages())->getMessage($type, $slug);
    }

    /**
     * @param VaultItemDTO $dto
     * @return self
     * @throws ModelNotSavedException
     */
    public static function createFromDTO(VaultItemDTO $dto): self
    {
        $item = new self();

        $item->name = $dto->name;
        $item->url = $dto->url;
        $item->description = $dto->description;
        $item->vault_groups_id = $dto->group;
        $item->user_id = $dto->userId;
        $item->created_by = $dto->createdBy;
        $item->updated_by = $dto->updatedBy;

        if ($item->save()) {
            return $item;
        }

        throw new ModelNotSavedException(self::getMessage('error', 'not-saved'));
    }

    /**
     * @param  VaultItem  $item
     * @param  VaultItemDTO  $dto
     * @return self
     * @throws ModelNotSavedException
     */
    public static function updateFromDTO(VaultItem $item, VaultItemDTO $dto): self
    {
        $item->name = $dto->name ?? $item->name;
        $item->url = $dto->url ?? $item->url;
        $item->description = $dto->description ?? $item->description;
        $item->vault_groups_id = $dto->group ?? $item->vault_groups_id;
        $item->updated_by = $dto->updatedBy ?? $item->updated_by;

        if (!$item->isDirty()) {
            return $item;
        }

        if ($item->save()) {
            return $item;
        }

        throw new ModelNotSavedException(VaultItem::getMessage('error', 'not-saved'));
    }
}
