<?php

namespace App\Models;

use App\DataTransferObjects\VaultGroupDTO;
use App\Enum\VaultGroupColorsEnum;
use App\Errors\VaultGroupMessages;
use App\Exceptions\ModelNotSavedException;
use App\Helpers\PurpleModel\PurpleModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string $name
 * @property VaultGroupColorsEnum|null $color
 * @property array $additional_data
 */
class VaultGroup extends Model
{
    use HasFactory;
    use PurpleModel;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'additional_data' => 'array',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(VaultItem::class, 'vault_groups_id');
    }

    /**
     * Get a message (error, warning, info or success)
     * @param  string  $type
     * @param  string  $slug
     * @return string
     */
    public static function getMessage(string $type, string $slug): string
    {
        return (new VaultGroupMessages())->getMessage($type, $slug);
    }

    /**
     * @param  VaultGroupDTO  $dto
     * @return self
     * @throws ModelNotSavedException
     */
    public static function createFromDTO(VaultGroupDTO $dto): self
    {
        $group = new self();

        $group->name = $dto->name;
        $group->color = $dto->color;
        $group->user_id = $dto->userId;
        $group->created_by = $dto->createdBy;
        $group->updated_by = $dto->updatedBy;

        if ($group->save()) {
            return $group;
        }

        throw new ModelNotSavedException(self::getMessage('error', 'not-saved'));
    }

    /**
     * @param  VaultGroup  $group
     * @param  VaultGroupDTO  $dto
     * @return self
     * @throws ModelNotSavedException
     */
    public static function updateFromDTO(VaultGroup $group, VaultGroupDTO $dto): self
    {
        $group->name = $dto->name ?? $group->name;
        $group->color = $dto->color ?? $group->color;
        $group->updated_by = $dto->updatedBy ?? $group->updated_by;

        if (!$group->isDirty()) {
            return $group;
        }

        if ($group->save()) {
            return $group;
        }

        throw new ModelNotSavedException(VaultGroup::getMessage('error', 'not-saved'));
    }
}
