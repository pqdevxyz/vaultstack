<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Enum\NamedRoutesEnum as NR;

class ResetYourPassword extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * @var string
     */
    public string $token;

    /**
     * Create a new message instance.
     *
     * @param  string  $token
     */
    public function __construct(string $token)
    {
        $this->token = route(NR::AUTH_SET_PASSWORD) . '?token=' . $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.auth.reset-your-password', ['token' => $this->token]);
    }
}
