<?php

namespace App\Exceptions;

class ModelNotSavedException extends PQException
{
    /** @var string */
    public string $pqMessage = 'Model Not Saved';

    /** @var string */
    public string $pqSlug = 'not-saved';

    /** @var string */
    public string $pqType = 'error';
}
