<?php

namespace App\Exceptions;

use Exception;

class PQException extends Exception
{
    use SetData;

    /** @var string */
    public string $pqClass;

    /** @var string */
    public string $pqMessage;

    /** @var string */
    public string $pqSlug;

    /** @var string */
    public string $pqType;
}
