<?php

namespace App\Exceptions;

class ModelNotFoundException extends PQException
{
    /** @var string */
    public string $pqMessage = 'Model Not Found';

    /** @var string */
    public string $pqSlug = 'not-found';

    /** @var string */
    public string $pqType = 'error';
}
