<?php

namespace App\Exceptions;

trait SetData
{
    /**
     * @param string|null $message
     * @param string|null $slug
     * @param string|null $type
     * @return $this
     */
    public function setData(string $message = null, string $slug = null, string $type = null, string $class = null): static
    {
        if (!is_null($message)) {
            $this->pqMessage = $message;
        }

        if (!is_null($slug)) {
            $this->pqSlug = $slug;
        }

        if (!is_null($type)) {
            $this->pqType = $type;
        }

        if (!is_null($class)) {
            $this->pqClass = $class;
        }

        return $this;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage(string $message): static
    {
        $this->pqMessage = $message;
        return $this;
    }

    /**
     * @param string $slug
     * @return $this
     */
    public function setSlug(string $slug): static
    {
        $this->pqSlug = $slug;
        return $this;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): static
    {
        $this->pqType = $type;
        return $this;
    }

    /**
     * @param string $class
     * @return $this
     */
    public function setClass(string $class): static
    {
        $this->pqClass = $class;
        return $this;
    }
}
