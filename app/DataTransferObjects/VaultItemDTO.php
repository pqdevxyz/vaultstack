<?php

namespace App\DataTransferObjects;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class VaultItemDTO extends DataTransferObject
{
    use CommonProperties;

    public string|null $url;
    public string|null $name;
    public string|null $description;
    public int|null $group;

    /**
     * @param  array  $data
     * @return self
     */
    public static function createFromArray(array $data): self
    {
        $dto = new self();

        $dto->url = $data['url'] ?? null;
        $dto->name = $data['name'] ?? null;
        $dto->description = $data['description'] ?? null;
        $dto->group = $data['group'] ?? null;
        $dto->userId = $data['userId'] ?? 0;
        $dto->createdBy = $data['createdBy'] ?? 0;
        $dto->updatedBy = $data['updatedBy'] ?? 0;

        return $dto;
    }

    /**
     * @param  Request  $request
     * @return self
     */
    public static function createFromRequest(Request $request): self
    {
        $dto = new self();

        $dto->name = $request->input('name') ?? null;
        $dto->description = $request->input('description') ?? null;
        $dto->url = $request->input('url') ?? null;
        $dto->group = $request->input('group') ?? null;
        $dto->userId = $request->user()->id ?? 0;
        $dto->createdBy = $request->user()->id ?? 0;
        $dto->updatedBy = $request->user()->id ?? 0;

        return $dto;
    }

    /**
     * @param  VaultItemDTO  $dto
     * @param  string|null  $name
     * @return self
     */
    public static function setName(self $dto, string|null $name): self
    {
        $dto = $dto->clone();
        $dto->name = $name;
        return $dto;
    }

    /**
     * @param  VaultItemDTO  $dto
     * @param  string|null  $description
     * @return self
     */
    public static function setDescription(self $dto, string|null $description): self
    {
        $dto = $dto->clone();
        $dto->description = $description;
        return $dto;
    }

    /**
     * @param  VaultItemDTO  $dto
     * @param  string|null  $url
     * @return self
     */
    public static function setURL(self $dto, string|null $url): self
    {
        $dto = $dto->clone();
        $dto->url = $url;
        return $dto;
    }

    /**
     * @param  VaultItemDTO  $dto
     * @param  int|null  $group
     * @return self
     */
    public static function setGroup(self $dto, int|null $group): self
    {
        $dto = $dto->clone();
        $dto->group = $group;
        return $dto;
    }
}
