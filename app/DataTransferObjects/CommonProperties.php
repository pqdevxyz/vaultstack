<?php

namespace App\DataTransferObjects;

trait CommonProperties
{
    public int $userId = 0;
    public int $createdBy = 0;
    public int $updatedBy = 0;

    /**
     * @param self $dto
     * @param int $userId
     * @return self
     */
    public static function setUserId(self $dto, int $userId): self
    {
        $dto = $dto->clone();
        $dto->userId = $userId;
        return $dto;
    }

    /**
     * @param self $dto
     * @param int $createdBy
     * @return self
     */
    public static function setCreatedBy(self $dto, int $createdBy): self
    {
        $dto = $dto->clone();
        $dto->createdBy = $createdBy;
        return $dto;
    }

    /**
     * @param self $dto
     * @param int $updatedBy
     * @return self
     */
    public static function setUpdatedBy(self $dto, int $updatedBy): self
    {
        $dto = $dto->clone();
        $dto->updatedBy = $updatedBy;
        return $dto;
    }
}
