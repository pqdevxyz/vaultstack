<?php

namespace App\DataTransferObjects;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;
use App\Enum\VaultGroupColorsEnum;

class VaultGroupDTO extends DataTransferObject
{
    use CommonProperties;

    public string|null $name;
    public VaultGroupColorsEnum|null $color;

    /**
     * @param  array  $data
     * @return self
     */
    public static function createFromArray(array $data): self
    {
        $dto = new self();

        $dto->name = $data['name'] ?? null;
        $dto->color = isset($data['color']) ? VaultGroupColorsEnum::from($data['color']) : null;
        $dto->userId = $data['userId'] ?? 0;
        $dto->createdBy = $data['createdBy'] ?? 0;
        $dto->updatedBy = $data['updatedBy'] ?? 0;

        return $dto;
    }

    /**
     * @param  Request  $request
     * @return self
     */
    public static function createFromRequest(Request $request): self
    {
        $dto = new self();

        $dto->name = $request->input('name') ?? null;
        $dto->color = $request->has('color') ? VaultGroupColorsEnum::from($request->input('color')) : null;
        $dto->userId = $request->user()->id ?? 0;
        $dto->createdBy = $request->user()->id ?? 0;
        $dto->updatedBy = $request->user()->id ?? 0;

        return $dto;
    }

    /**
     * @param  VaultGroupDTO  $dto
     * @param  string|null  $name
     * @return self
     */
    public static function setName(self $dto, string|null $name): self
    {
        $dto = $dto->clone();
        $dto->name = $name;
        return $dto;
    }

    /**
     * @param  VaultGroupDTO  $dto
     * @param  VaultGroupColorsEnum|string|null  $color
     * @return self
     */
    public static function setColor(self $dto, VaultGroupColorsEnum|string|null $color): self
    {
        $dto = $dto->clone();
        $dto->color = isset($color) ? VaultGroupColorsEnum::from($color) : null;
        return $dto;
    }
}
