<?php

namespace App\Actions;

use App\DataTransferObjects\VaultItemDTO;
use App\Exceptions\ModelNotFoundException;
use App\Models\VaultItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EditVaultItemAction
{
    public function __invoke(Request $request, int $id): VaultItem|bool
    {
        /** @var VaultItem|null $item */
        $item = VaultItem::where('user_id', Auth::id())->find($id);

        if (is_null($item)) {
            throw (new ModelNotFoundException('Model not found', 404))->setData('Not Found', 'not-found', 'error', VaultItem::class);
        }

        $dto = VaultItemDTO::createFromRequest($request);
        $dto = $dto::setUpdatedBy($dto, Auth::id());

        return VaultItem::updateFromDTO($item, $dto);
    }
}
