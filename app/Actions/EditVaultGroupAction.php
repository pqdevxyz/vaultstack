<?php

namespace App\Actions;

use App\DataTransferObjects\VaultGroupDTO;
use App\Exceptions\ModelNotFoundException;
use App\Exceptions\PQException;
use App\Models\VaultGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EditVaultGroupAction
{
    public function __invoke(Request $request, int $id): VaultGroup|bool
    {
        /** @var VaultGroup|null $group */
        $group = VaultGroup::where('user_id', Auth::id())->find($id);

        if (is_null($group)) {
            throw (new ModelNotFoundException('Model not found', 404))->setData('Not Found', 'not-found', 'error', VaultGroup::class);
        }

        $dto = VaultGroupDTO::createFromRequest($request);
        $dto = $dto::setUpdatedBy($dto, Auth::id());

        return VaultGroup::updateFromDTO($group, $dto);
    }
}
