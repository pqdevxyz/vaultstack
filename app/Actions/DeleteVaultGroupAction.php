<?php

namespace App\Actions;

use App\Exceptions\ModelNotFoundException;
use App\Models\VaultGroup;
use Illuminate\Support\Facades\Auth;

class DeleteVaultGroupAction
{
    public function __invoke(int $id): bool
    {
        /** @var VaultGroup|null $group */
        $group = VaultGroup::where('user_id', Auth::id())->find($id);

        if (is_null($group)) {
            throw (new ModelNotFoundException('Model not found', 404))->setData('Not Found', 'not-found', 'error', VaultGroup::class);
        }

        return $group->delete();
    }
}
