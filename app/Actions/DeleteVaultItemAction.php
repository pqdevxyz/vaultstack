<?php

namespace App\Actions;

use App\Exceptions\ModelNotFoundException;
use App\Models\VaultItem;
use Illuminate\Support\Facades\Auth;

class DeleteVaultItemAction
{
    public function __invoke(int $id): bool
    {
        /** @var VaultItem|null $item */
        $item = VaultItem::where('user_id', Auth::id())->find($id);

        if (is_null($item)) {
            throw (new ModelNotFoundException('Model not found', 404))->setData('Not Found', 'not-found', 'error', VaultItem::class);
        }

        return $item->delete();
    }
}
