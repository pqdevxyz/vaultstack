<?php

namespace App\Actions;

use App\DataTransferObjects\VaultItemDTO;
use App\Models\VaultItem;
use Illuminate\Http\Request;

class CreateVaultItemAction
{
    public function __invoke(Request $request): VaultItem|bool
    {
        $dto = VaultItemDTO::createFromRequest($request);

        return VaultItem::createFromDTO($dto);
    }
}
