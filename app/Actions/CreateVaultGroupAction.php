<?php

namespace App\Actions;

use App\DataTransferObjects\VaultGroupDTO;
use App\Exceptions\PQException;
use App\Models\VaultGroup;
use Illuminate\Http\Request;

class CreateVaultGroupAction
{
    public function __invoke(Request $request): VaultGroup|bool
    {
        $dto = VaultGroupDTO::createFromRequest($request);

        try {
            return VaultGroup::createFromDTO($dto);
        } catch (PQException $exception) {
            return false;
        }
    }
}
