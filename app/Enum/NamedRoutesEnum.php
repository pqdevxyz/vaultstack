<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

class NamedRoutesEnum extends Enum
{
    public const HOME = 'home';

    public const AUTH_FORGOT_PASSWORD = 'auth.forgot-password';
    public const AUTH_SET_PASSWORD = 'auth.set-password';
    public const AUTH_LOGIN = 'auth.login';
    public const AUTH_LOGOUT = 'auth.logout';

    public const VAULT_OVERVIEW = 'vault.overview';

    public const ITEM_REMOVE = 'item.remove';
    public const ITEM_VIEW = 'item.view';
    public const ITEM_EDIT = 'item.edit';
    public const ITEM_CREATE = 'item.create';

    public const GROUP_REMOVE = 'group.remove';
    public const GROUP_EDIT = 'group.edit';
    public const GROUP_CREATE = 'group.create';
}
