<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

class VaultGroupColorsEnum extends Enum
{
    public const SLATE = 'slate';
    public const GRAY = 'gray';
    public const ZINC = 'zinc';
    public const NEUTRAL = 'neutral';
    public const STONE = 'stone';
    public const RED = 'red';
    public const ORANGE = 'orange';
    public const AMBER = 'amber';
    public const YELLOW = 'yellow';
    public const LIME = 'lime';
    public const GREEN = 'green';
    public const EMERALD = 'emerald';
    public const TEAL = 'teal';
    public const CYAN = 'cyan';
    public const SKY = 'sky';
    public const BLUE = 'blue';
    public const INDIGO = 'indigo';
    public const VIOLET = 'violet';
    public const PURPLE = 'purple';
    public const FUCHSIA = 'fuchsia';
    public const PINK = 'pink';
    public const ROSE = 'rose';
}
