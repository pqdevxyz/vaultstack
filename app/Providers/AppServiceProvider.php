<?php

namespace App\Providers;

use App\Enum\NamedRoutesEnum;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\ParallelTesting;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $httpsEnvironments = ['local', 'ci', 'cypress', 'test', 'staging', 'production'];
        if (in_array(App::environment(), $httpsEnvironments)) {
            URL::forceScheme('https');
        }

        View::share('NR', new NamedRoutesEnum('home'));

        ParallelTesting::tearDownProcess(function () {
            Artisan::call('db:seed');
        });
    }
}
