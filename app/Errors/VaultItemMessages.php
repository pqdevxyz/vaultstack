<?php

namespace App\Errors;

class VaultItemMessages
{
    use Messages;

    /**
     * Error Messages for Vault Item model
     * @var array|string[]
     */
    public array $errorMessages = [
        'not-found' => 'Vault Item could not be found with the Id you provided.',
        'none-found' => 'No Vault Items could not be found.',
        'not-saved' => 'Could not save Vault Item',
        'not-deleted' => 'Could not delete Vault Item',
        'invalid-argument' => 'Please check the data and try again',
        'invalid-dto-property' => 'Please check the data and try again',
    ];

    /**
     * Warning Messages for Vault Item model
     * @var array|string[]
     */
    public array $warningMessages = [
        'cannot-create-vault-item-via-link' => 'Trying to create a new Vault Item via a link',
        'cannot-create-vault-item-via-form' => 'Trying to create a new Vault Item via form data',
        'cannot-update-vault-item' => 'Failed to update Vault Item',
    ];

    /**
     * Success Messages for Vault Item model
     * @var array|string[]
     */
    public array $successMessages = [
        'created' => 'Vault Item created successfully.',
        'deleted' => 'Vault Item deleted successfully.',
        'updated' => 'Vault Item updated successfully.',
    ];

    /**
     * Info Messages for Vault Item model
     * @var array|string[]
     */
    public array $infoMessages = [
        'test' => 'Unknown Info',
    ];
}
