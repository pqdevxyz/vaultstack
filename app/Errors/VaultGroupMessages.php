<?php

namespace App\Errors;

class VaultGroupMessages
{
    use Messages;

    /**
     * Error Messages for Vault Group model
     * @var array|string[]
     */
    public array $errorMessages = [
        'not-found' => 'Vault Group could not be found with the Id you provided.',
        'none-found' => 'No Vault Groups could not be found.',
        'not-saved' => 'Could not save Vault Group',
        'not-deleted' => 'Could not delete Vault Group',
        'invalid-argument' => 'Please check the data and try again',
        'invalid-dto-property' => 'Please check the data and try again',
    ];

    /**
     * Warning Messages for Vault Group model
     * @var array|string[]
     */
    public array $warningMessages = [
        'test' => 'Unknown Warning',
    ];

    /**
     * Success Messages for Vault Group model
     * @var array|string[]
     */
    public array $successMessages = [
        'created' => 'Vault Group created successfully.',
        'deleted' => 'Vault Group deleted successfully.',
        'updated' => 'Vault Group updated successfully.',
    ];

    /**
     * Info Messages for Vault Group model
     * @var array|string[]
     */
    public array $infoMessages = [
        'test' => 'Unknown Info',
    ];
}
