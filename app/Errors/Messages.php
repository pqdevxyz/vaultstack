<?php

namespace App\Errors;

trait Messages
{
    /**
     * @param string $type
     * @param string $slug
     * @return string
     */
    public function getMessage(string $type, string $slug): string
    {
        return match ($type) {
            'info' => array_key_exists($slug, $this->infoMessages) ? $this->infoMessages[$slug] : 'Unknown Info',
            'success' => array_key_exists($slug, $this->successMessages) ? $this->successMessages[$slug] : 'Unknown Success',
            'warning' => array_key_exists($slug, $this->warningMessages) ? $this->warningMessages[$slug] : 'Unknown Warning',
            default => array_key_exists($slug, $this->errorMessages) ? $this->errorMessages[$slug] : 'Unknown Error',
        };
    }
}
