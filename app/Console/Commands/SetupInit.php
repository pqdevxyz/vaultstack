<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SetupInit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vs:init {--U|onlyUser}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup the VaultStack project';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('clear-compiled');
        $this->call('migrate:fresh');
        if ($this->option('onlyUser')) {
            $this->call('db:seed', ['class' => 'UserSeeder']);
        } else {
            $this->call('db:seed');
        }
        $this->call('package:discover');
    }
}
