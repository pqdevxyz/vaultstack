<?php

namespace App\Helpers\PurpleModel;

trait PurpleModel
{
    /**
     * Get the created by user from the additional_data attribute.
     *
     * @return int|null
     */
    protected function getCreatedByAttribute(): int|null
    {
        return isset($this->additional_data['created_by']) && $this->additional_data['created_by'] != 0 ? $this->additional_data['created_by'] : null;
    }

    /**
     * Set the created by field for the user in the additional_data attribute.
     *
     * @param  int  $value
     */
    protected function setCreatedByAttribute(int $value)
    {
        $data = $this->additional_data ?? [];
        $data['created_by'] = $value;
        $this->additional_data = $data;
    }

    /**
     * Get the updated by user from the additional_data attribute.
     *
     * @return int|null
     */
    protected function getUpdatedByAttribute(): int|null
    {
        return isset($this->additional_data['updated_by']) && $this->additional_data['updated_by'] != 0 ? $this->additional_data['updated_by'] : null;
    }

    /**
     * Set the updated by field for the user in the additional_data attribute.
     *
     * @param  int  $value
     */
    protected function setUpdatedByAttribute(int $value)
    {
        $data = $this->additional_data ?? [];
        $data['updated_by'] = $value;
        $this->additional_data = $data;
    }
}
