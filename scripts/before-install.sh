#!/usr/bin/env bash
if [ "$DEPLOYMENT_GROUP_NAME" == "test" ]; then
    cd /var/www/vaultstack/test
elif [ "$DEPLOYMENT_GROUP_NAME" == "staging" ]; then
    cd /var/www/vaultstack/staging
elif [ "$DEPLOYMENT_GROUP_NAME" == "production" ]; then
    cd /var/www/vaultstack/production
fi

if [[ -f "artisan" ]]; then
    echo "=======================" >> ./scripts/log.txt
    echo "START: before-install.sh" >> ./scripts/log.txt

    #Terminate rouge horizon processes
    php artisan horizon:terminate >> ./scripts/log.txt
    #Put app in maintenance mode
    php artisan down --render="errors::503" >> ./scripts/log.txt

    #Delete the bootstrap/cache/config.php file
    rm ./bootstrap/cache/config.php

    echo "END: before-install.sh" >> ./scripts/log.txt
    echo "=======================" >> ./scripts/log.txt
fi
