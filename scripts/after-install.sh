#!/usr/bin/env bash

if [ "$DEPLOYMENT_GROUP_NAME" == "test" ]; then
    cd /var/www/vaultstack/test
elif [ "$DEPLOYMENT_GROUP_NAME" == "staging" ]; then
    cd /var/www/vaultstack/staging
elif [ "$DEPLOYMENT_GROUP_NAME" == "production" ]; then
    cd /var/www/vaultstack/production
fi

echo "=======================" >> ./scripts/log.txt
echo "START: after-install.sh" >> ./scripts/log.txt

#Update file / folder permissions
sudo chown root:root /var/www
sudo chmod 755 /var/www/
sudo chown -R www-data:www-data /var/www/vaultstack
sudo chmod -R 775 /var/www/vaultstack

#Composer install
composer install --no-dev --optimize-autoloader >> ./scripts/log.txt

if [[ -f "artisan" ]]; then
    #Clear config
    php artisan cache:clear >> ./scripts/log.txt
    php artisan view:clear >> ./scripts/log.txt
    php artisan config:clear >> ./scripts/log.txt
    php artisan route:clear >> ./scripts/log.txt

    php artisan view:cache >> ./scripts/log.txt
    php artisan config:cache >> ./scripts/log.txt
    php artisan route:cache >> ./scripts/log.txt

    #Migrate
    php artisan migrate --force >> ./scripts/log.txt
fi

#Update file / folder permissions
sudo chown root:root /var/www
sudo chmod 755 /var/www/
sudo chown -R www-data:www-data /var/www/vaultstack
sudo chmod -R 775 /var/www/vaultstack

#Reboot octane
sudo php artisan octane:reload >> ./scripts/log.txt

echo "END: after-install.sh" >> ./scripts/log.txt
echo "=======================" >> ./scripts/log.txt
