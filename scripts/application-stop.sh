#!/usr/bin/env bash
if [ "$DEPLOYMENT_GROUP_NAME" == "test" ]; then
    SUPERVISOR_HORIZON="vaultstack-test-horizon"
    cd /var/www/vaultstack/test
elif [ "$DEPLOYMENT_GROUP_NAME" == "staging" ]; then
    SUPERVISOR_HORIZON="vaultstack-staging-horizon"
    cd /var/www/vaultstack/staging
elif [ "$DEPLOYMENT_GROUP_NAME" == "production" ]; then
    SUPERVISOR_HORIZON="vaultstack-production-horizon"
    cd /var/www/vaultstack/production
fi

echo "=======================" > ./scripts/log.txt
echo "START: application-stop.sh" >> ./scripts/log.txt
echo $(date) >> ./scripts/log.txt

#Stop Supervisor
sudo supervisorctl stop $SUPERVISOR_HORIZON >> ./scripts/log.txt

echo "END: application-stop.sh" >> ./scripts/log.txt
echo "=======================" >> ./scripts/log.txt
