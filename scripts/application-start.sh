#!/usr/bin/env bash
if [ "$DEPLOYMENT_GROUP_NAME" == "test" ]; then
    SUPERVISOR_HORIZON="vaultstack-test-horizon"
    SUPERVISOR_OCTANE="vaultstack-test-octane"
    cd /var/www/vaultstack/test
elif [ "$DEPLOYMENT_GROUP_NAME" == "staging" ]; then
    SUPERVISOR_HORIZON="vaultstack-staging-horizon"
    SUPERVISOR_OCTANE="vaultstack-staging-octane"
    cd /var/www/vaultstack/staging
elif [ "$DEPLOYMENT_GROUP_NAME" == "production" ]; then
    SUPERVISOR_HORIZON="vaultstack-production-horizon"
    SUPERVISOR_OCTANE="vaultstack-production-octane"
    cd /var/www/vaultstack/production
fi

echo "=======================" >> ./scripts/log.txt
echo "START: application-start.sh" >> ./scripts/log.txt

if [[ -f "artisan" ]]; then
    #Pull app out of maintenance mode
    php artisan up >> ./scripts/log.txt
fi

#Start supervisor
sudo supervisorctl start $SUPERVISOR_HORIZON >> ./scripts/log.txt
sudo supervisorctl restart $SUPERVISOR_OCTANE >> ./scripts/log.txt

echo "END: application-start.sh" >> ./scripts/log.txt
echo "=======================" >> ./scripts/log.txt
