#!/usr/bin/env bash

#Install yarn packages
yarn install

#Run yrd
yarn run ${YARN_MODE}

#Copy appspec for AWS CodeDeploy
cp appspec.yml.${ENVIRONMENT} appspec.yml

#Run clean repo script
./scripts/clean-repo.sh

#Get env file
aws s3 cp s3://environment.vaultstack.pqdev/${ENVIRONMENT}/.env .env
