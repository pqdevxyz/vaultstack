const colors = require('./resources/sass/colours')
const forms = require('@tailwindcss/forms')
const typography = require('@tailwindcss/typography')
const autoprefixer = require('autoprefixer')

module.exports = {
    content: [
        './resources/views/**/*.php'
    ],
    theme: {
        colors: {
            primary: colors.primary,
            neutral: colors.neutral,
            error: colors.error,
            warning: colors.warning,
            success: colors.success,
            info: colors.info,
            black: colors.black,
            white: colors.white,
            groups: colors.groups,
        },
        extend: {
            fontFamily: {
                sans: ['Raleway', 'ui-sans-serif', 'system-ui', '-apple-system', 'BlinkMacSystemFont', 'Segoe UI', 'Helvetica Neue', 'Arial', 'Noto Sans', 'sans-serif', 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji'],
                quicksand: ['Quicksand', 'Raleway', 'ui-sans-serif', 'system-ui', '-apple-system', 'BlinkMacSystemFont', 'Segoe UI', 'Helvetica Neue', 'Arial', 'Noto Sans', 'sans-serif', 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji'],
            }
        },
    },
    plugins: [
        forms,
        typography,
        autoprefixer,
    ],
}
