# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project using to [Calendar Versioning](https://calver.org/overview.html) with the following format `YY.MM.PATCH`

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

<!-- Remember to update sonar-project.properties with the correct version number -->
## 22.06.1
### Security
- Updated dependencies

## 22.05.1
### Added
- Added infection testing package
### Changed
- Changes to docker scripts
- Upgraded Font Awesome
- Fixed tests & made them work in parallel
### Removed
- Enlightn package

## 22.02.3
### Added
- Laravel octane for better performance

## 22.02.2
### Changed
- Update to Laravel 9

## 22.02.1
### Changed
- Update to PHP 8.1

## 22.01.3
### Added
- Redis DBs to example env file

## 22.01.2
### Fixed
- Fixed ohdear health check

## 22.01.1
- Initial Release
